({
	handleDbOpertaion : function(component, event, helper) 
	{
		var objResponse = component.get("v.objResponse");
		
    	component.set("v.showSpinner",true);
    	
		var action = component.get("c.createProductUsageRecords");
        action.setParams({ objResponseForSave : JSON.stringify(objResponse), 
        					idRecord : component.get("v.recordId") });
        action.setCallback(this, function(response) 
        {
        	component.set("v.showSpinner",false);
        	var state = response.getState();
           
            if (state === "SUCCESS") {
            	component.set("v.objResponse",response.getReturnValue() ); 
            	if(component.get("v.objResponse").boolIsError==true)
                {
            		var objResponse = component.get("v.objResponse");
            		var boolIsIndividualError = false;
            		for(var i in objResponse.lstOrderProductWrapper)
            		{
            			if(objResponse.lstOrderProductWrapper[i].boolIsErrorInOldProduct 
            			 || objResponse.lstOrderProductWrapper[i].boolIsErrorInReasonForStopUsing)
            			{
            				boolIsIndividualError = true;
            			}            			
            		}
            		
            		if(!boolIsIndividualError)
            		{
	                	var errmsg= new Array(); 
	            		var errmsg = component.get("v.objResponse").strErrMsg.split('::');
	            		for(var intVar=0; intVar<errmsg.length ; intVar ++)
	            		{
	            			var toastEvent = $A.get("e.force:showToast");
							toastEvent.setParams({
							     title: 'Error',
							     type: 'error',
							     mode:'sticky',
							     message: errmsg[intVar]
							});
							toastEvent.fire();
	            		}
            		}
            		component.set("v.objResponse", objResponse);
                }
                else
                {
                	if(component.get("v.objResponse").boolShowProdUsageModal == false)
                	{
	                	var toastEvent = $A.get("e.force:showToast");
						toastEvent.setParams({
						     title: 'Success',
						     type: 'success',
						     mode:'pester',
						     message: component.get("v.objResponse").strSucMsg
						});
						toastEvent.fire();
						
						if(component.get("v.objResponse").strobjAPI == 'Case')
						{
							var navEvt = $A.get("e.force:navigateToSObject");
						    navEvt.setParams({
						      "recordId": component.get("v.objResponse").objOrder.Id,
						      "slideDevName": "Details"
						    });
						    navEvt.fire();
						}
						else
						{
							var objResponse = component.get("v.objResponse");
							objResponse.boolShowButton = false;
							component.set("v.objResponse",objResponse);
							$A.get('e.force:refreshView').fire();
						}	
                	}
                }               
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        
        });
        $A.enqueueAction(action);
	}
})