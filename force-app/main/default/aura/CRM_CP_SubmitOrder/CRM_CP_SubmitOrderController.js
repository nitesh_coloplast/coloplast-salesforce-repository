({
    doInit : function(component, event, helper) {
    
        var action = component.get("c.getButtonVisibility");
        action.setParams({ idRecord : component.get("v.recordId") });
       
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            	component.set("v.showSpinner",false);
            	component.set("v.objResponse",response.getReturnValue() );   
            	component.set("v.lstProductUsage", response.getReturnValue().lstProductUsagePicklist);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
     submitOrder : function(component, event, helper) {
     
        component.set("v.showSpinner",true);
        var action = component.get("c.convertToOrder");
        component.set("v.showSpinner",true);
        action.setParams({ idRecord : component.get("v.recordId") });
        action.setCallback(this, function(response) {
        
        	component.set("v.showSpinner",false);
           component.set("v.showSpinner",false);
           var state = response.getState();
            if (state === "SUCCESS") {
            	component.set("v.objResponse",response.getReturnValue() );  
            	component.set("v.lstProductUsage", response.getReturnValue().lstProductUsagePicklist);
            	if(component.get("v.objResponse").boolIsError==true)
                {
                	var errmsg= new Array(); 
            		var errmsg = component.get("v.objResponse").strErrMsg.split('::');
            		for(var intVar=0; intVar<errmsg.length ; intVar ++)
            		{
            			var toastEvent = $A.get("e.force:showToast");
						toastEvent.setParams({
						     title: 'Error',
						     type: 'error',
						     mode:'sticky',
						     message: errmsg[intVar]
						});
						toastEvent.fire();
            		}
                }
                else
                {
                	if(component.get("v.objResponse").boolShowProdUsageModal == false)
                	{
	                	var toastEvent = $A.get("e.force:showToast");
						toastEvent.setParams({
						     title: 'Success',
						     type: 'success',
						     mode:'pester',
						     message: component.get("v.objResponse").strSucMsg
						});
						toastEvent.fire();
						if(component.get("v.objResponse").strobjAPI == 'Case')
						{
							var navEvt = $A.get("e.force:navigateToSObject");
						    navEvt.setParams({
						      "recordId": component.get("v.objResponse").objOrder.Id,
						      "slideDevName": "Details"
						    });
						    navEvt.fire();
						}
						else
						{
							var objResponse = component.get("v.objResponse");
							objResponse.boolShowButton = false;
							component.set("v.objResponse",objResponse);
							$A.get('e.force:refreshView').fire();
						}						
                	}
                }               
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        
        });
        $A.enqueueAction(action);
    },
    /*
	 * CloaseModal : This JS method clode modal opened for showing Products 
	 * Created Date : 07 - April - 2018
	 */
    closeModal : function(component, event, helper)
    {
    	var objResponse = component.get("v.objResponse");
    	objResponse.boolShowProdUsageModal = false;
    	component.set("v.objResponse", objResponse);
    },
    
    /*
	 * handleSave : This JS method cloe modal opened for showing Products 
	 * Created Date : 07 - April - 2018
	 */
    handleSave : function (component, event, helper)
    {
    	var objResponse = component.get("v.objResponse");
    	objResponse.boolIsError=false;
    	objResponse.strErrMsg = null;
    	objResponse.lstErrRec  = new Array();
    	    	
    	component.set("v.objResponse",objResponse);
    	
    	helper.handleDbOpertaion(component, event, helper);    	    	
    	
    	component.set("v.objResponse", objResponse);
    }
    
    
})