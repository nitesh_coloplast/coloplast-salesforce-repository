({
    openPopup : function(component, event, helper){
        component.set("v.showPopup",true);
    },
    
    closePopup : function(component, event, helper){
        component.set("v.showPopup",false);
    },
    
    changeCaseOwner : function(component, event, helper){
        
        if(helper.validateForm(component,event)){
        
            var action = component.get("c.changeOwner");   
            action.setParams({ 
                                caseId : component.get("v.recordId"),
                                userId : component.get("v.userLookupId"),
                                email : component.get("v.userEmail"),
                                isSendEmailTrue : component.get("v.isSendEmailTrue")
                            });
            
            // Create a callback that is executed after 
            // the server-side action returns
            action.setCallback(this, function(response) {
                var state = response.getState();
                
                if (state === "SUCCESS") {
                    component.set("v.showPopup",false);
                    $A.get("e.force:refreshView").fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type":"success",
                        "message": component.get("v.userName") + " " + $A.get("$Label.c.CRM_CP_now_owns_the_record_for") + " " + response.getReturnValue().CaseNumber + "."
                    });
                    toastEvent.fire();
                }
                else if (state === "INCOMPLETE") {
                    // do something
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            // optionally set storable, abortable, background flag here
            
            // A client-side action could cause multiple events, 
            // which could trigger other events and 
            // other server-side action calls.
            // $A.enqueueAction adds the server-side action to the queue.
            $A.enqueueAction(action);
        }
    },
    
    searchusers : function(component, event, helper){
        
        component.set("v.userLookupId",'');
        
        component.set("v.searchUsers",true);
        var src = event.target || event.srcElement;
        var searchText = event.target.value;
        
        console.log(searchText);
        
        if(searchText.length > 2){
            var action = component.get('c.searchUsers');
            action.setParams({ searchText : searchText});           
            var cmpTarget = component.find('userlookup');
            
            action.setCallback(this, $A.getCallback(function (response) {
                
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log(JSON.stringify(response.getReturnValue()));
                    component.set("v.users", response.getReturnValue());
                    $A.util.addClass(cmpTarget, 'slds-is-open');
                } else if (state === "ERROR") {
                    var resultsToast1 = $A.get("e.force:showToast");
                    resultsToast1.setParams({
                        "title": "ERROR",
                        "type":"error",
                        "message": $A.get("$Label.c.CRM_CP_Something_went_wrong_please_check_with_your_system_administrator")
                    });
                    $A.util.removeClass(cmpTarget, 'slds-is-open');
                    resultsToast1.fire();
                }
                    else{
                        $A.util.removeClass(cmpTarget, 'slds-is-open');
                    }
            }));
            $A.enqueueAction(action);
        }
    },
    
    selectUser : function(component, event, helper){
        
        component.set("v.userLookupId",null);
        component.set("v.userName",null);
        component.set("v.userEmail",null);
        
        var src = event.target || event.srcElement;
        var selectedClient = event.target.id;
        var users = component.get("v.users");
        
        for(var i = 0 ; i < users.length ; i++){
            if(users[i].Id === selectedClient){
                console.log('Test ' + users[i].Name);
                component.set("v.userLookupId",users[i].Id);
                component.set("v.userName",users[i].Name);
                component.set("v.userEmail",users[i].Email);
                component.set("v.searchUsers",false);
                //helper.validateUserLookup(component,event);
                break;
            }
        }
        var cmpTarget = component.find('userlookup');
        $A.util.removeClass(cmpTarget, 'slds-is-open');
        component.set("v.users", []);
    } 
})