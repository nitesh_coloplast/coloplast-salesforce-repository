({
    validateForm : function(component, event){
        
        var errorcount = 0;

        if(typeof component.get('v.userLookupId') == 'undefined' 
          || component.get('v.userLookupId') == null
          || component.get('v.userLookupId') == ''){
            errorcount++;
            
            document.getElementById('userFormId').classList.add("slds-has-error");
            document.getElementById('userName').classList.add("slds-has-error");
            document.getElementById('userName').classList.remove("slds-combobox__input");
           	document.getElementById('error-messageUser').style.display = 'block';
  
        } 
        else{
            document.getElementById('userFormId').classList.remove("slds-has-error");
            document.getElementById('userName').classList.remove("slds-has-error");
            document.getElementById('userName').classList.add("slds-combobox__input");
           	document.getElementById('error-messageUser').style.display = 'none';
        }
        
        if(errorcount > 0){
        	return false;
        }
        else{
            return true;
        }
    }
})