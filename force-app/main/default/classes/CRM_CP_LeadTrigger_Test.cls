/**
* @author       : Ninad Patil
* @date         : 06 Aug 2018
* @description  : This class is Test class for Sample Request to Case Order conversion utilities.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Ninad Patil -
*/
@isTest
private class CRM_CP_LeadTrigger_Test {

    /**
    * @author       : Ninad Patil
    * @date         : 06 AUG 2018
    * @description  : This method creates data in the system before test methods are executed
    */
    @testSetup static void setup()
    {
        Product2 catheterProduct = new Product2();
        catheterProduct.CRM_CP_Owner_Company_Number__c = '230';
        catheterProduct.Name = 'IC Unknown';
        catheterProduct.CRM_CP_External_ID__c = '230_UN_21';
        catheterProduct.CRM_CP_Product_Type__c = 'Intermittent Catheter';
        catheterProduct.CRM_CP_Active_for_Product_Usage__c = TRUE;
        insert catheterProduct;

        List<Lead> lstLeadToInsert = new List<Lead>();
        Lead objLead1 = CRM_CP_TestDataUtility.createLead('Test lead FNAME1',
                    'Test lead LNAME1',
                    'Patient' ,
                    '9867775632',
                    'fnamelname1@company.com');
        objLead1.CRM_CP_Eloqua_Integration__c = FALSE;
        objLead1.CRM_CP_Owner_Company_Number__c = '230';
        lstLeadToInsert.add(objLead1);
        Lead objLead2 = CRM_CP_TestDataUtility.createLead('Test lead FNAME2',
                    'Test lead LNAME2',
                    'Patient' ,
                    '9867775632',
                    'fnamelname2@company.com');
        objLead2.CRM_CP_Eloqua_Integration__c = TRUE;
        objLead2.CRM_CP_Owner_Company_Number__c = '230';
        objLead2.CRM_CP_ELQ_Colostomy__c = 'Y';
        objLead2.CRM_CP_ELQ_Ileostomy__c = 'Y';
        objLead2.CRM_CP_ELQ_Jejunostomy__c = 'Y';
        objLead2.CRM_CP_ELQ_Nephrostomy__c = 'Y';
        objLead2.CRM_CP_ELQ_Urostomy__c = 'Y';
        objLead2.CRM_CP_ELQ_Continent_Urostomy__c = 'Y';
        objLead2.CRM_CP_ELQ_Temporary_Stoma__c = 'Y';
        objLead2.CRM_CP_ELQ_Stoma_Opening_Position__c = 'Above Skin Surface';
        objLead2.CRM_CP_ELQ_Wheelchair__c = 'Y';
        objLead2.CRM_CP_ELQ_Surgery_Date__c = SYSTEM.TODAY() - 20;
        objLead2.CRM_CP_ELQ_Catheter_Usage_Date__c = SYSTEM.TODAY() - 21;
        lstLeadToInsert.add(objLead2);
        Lead objLead3 = CRM_CP_TestDataUtility.createLead('Test lead FNAME3',
                    'Test lead LNAME3',
                    'Patient' ,
                    '9867775632',
                    'fnamelname3@company.com');
        objLead3.CRM_CP_Eloqua_Integration__c = TRUE;
        objLead3.CRM_CP_Owner_Company_Number__c = '230';
        objLead3.CRM_CP_ELQ_Colostomy__c = 'Y';
        objLead3.CRM_CP_ELQ_Jejunostomy__c = 'Y';
        objLead3.CRM_CP_ELQ_Nephrostomy__c = 'Y';
        objLead3.CRM_CP_ELQ_Temporary_Stoma__c = 'N';
        objLead3.CRM_CP_ELQ_Stoma_Opening_Position__c = 'Above Skin Surface';
        objLead3.CRM_CP_ELQ_Wheelchair__c = 'Y';
        objLead3.CRM_CP_ELQ_Surgery_Date__c = SYSTEM.TODAY() - 22;
        objLead3.CRM_CP_ELQ_Catheter_Usage_Date__c = SYSTEM.TODAY() - 25;
        lstLeadToInsert.add(objLead3);
        insert lstLeadToInsert;

        objLead3 = [Select Id, Name from Lead where Email = 'fnamelname3@company.com'];
        CRM_CP_Health_Basics__c objHB = new CRM_CP_Health_Basics__c();
        objHB.CRM_CP_Lead__c = objLead3.Id;
        objHB.recordTypeId = Schema.SObjectType.CRM_CP_Health_Basics__c.getRecordTypeInfosByDeveloperName().get('CRM_CP_Procedure_Outcome').getRecordTypeId();
        objHB.CRM_CP_Outcome__c = 'Colostomy';
        objHB.CRM_CP_Stoma_Status__c = 'Temporary';
        insert objHB;



    }
    /**
    * @author       : Ninad Patil
    * @date         : 06 AUG 2018
    * @description  : This method test conversion of case under Consumer Account.
    */
    @isTest static void testDefaultPopulationOflanguages()
    {
        List<CRM_CP_Spoken_Written_Language_Config__c> lstlanguageConfig = CRM_CP_TestDataUtility.getSpokenAndwrittenLanguageCustomConfig();
        if(lstlanguageConfig != null
            && !lstlanguageConfig.isEmpty()) insert lstlanguageConfig;

       Test.startTest();

       Lead objLead = CRM_CP_TestDataUtility.createLead('strFirstName',
                    'strLastName',
                    'Patient' ,
                    '9867775632',
                    'strEmail@company.com');

       insert objLead;

       objLead = [Select id, name, CRM_CP_Preferred_Written_Language__c, CRM_CP_PreferredSpoken_Lang__c
        FROM Lead where Id = :objLead.Id ];

       system.assert(objLead.CRM_CP_Preferred_Written_Language__c != null);

       objLead.CRM_CP_Owner_Company_Number__c = '760';
       objLead.CRM_CP_PreferredSpoken_Lang__c = 'French';
       update objLead;

       objLead.CRM_CP_PreferredSpoken_Lang__c = 'English';
       update objLead;

       Test.stopTest();

    }


    /**
    * @author       : Mrua Shastri
    * @date         : 23-Oct-2018
    * @description  : This method tests Eloqua Sync Logic on Creation of Lead.
    */
    @isTest static void testELQSyncLogicOnCreate()
    {
        List<Lead> lstQueriedLeads = new List<Lead>();

       Test.startTest();

       Lead objLead = CRM_CP_TestDataUtility.createLead('Test lead FNAME4',
                    'Test lead LNAME4',
                    'Patient' ,
                    '9867775632',
                    'fnamelname4@company.com');
        objLead.CRM_CP_Eloqua_Integration__c = TRUE;
        objLead.CRM_CP_Owner_Company_Number__c = '230';
        objLead.CRM_CP_ELQ_Colostomy__c = 'Y';
        objLead.CRM_CP_ELQ_Ileostomy__c = 'Y';
        objLead.CRM_CP_ELQ_Jejunostomy__c = 'Y';
        objLead.CRM_CP_ELQ_Nephrostomy__c = 'Y';
        objLead.CRM_CP_ELQ_Urostomy__c = 'Y';
        objLead.CRM_CP_ELQ_Continent_Urostomy__c = 'Y';
        objLead.CRM_CP_ELQ_Temporary_Stoma__c = 'Y';
        objLead.CRM_CP_ELQ_Stoma_Opening_Position__c = 'Above Skin Surface';
        objLead.CRM_CP_ELQ_Wheelchair__c = 'Y';
        objLead.CRM_CP_ELQ_Surgery_Date__c = SYSTEM.TODAY() - 20;
        objLead.CRM_CP_ELQ_Catheter_Usage_Date__c = SYSTEM.TODAY() - 21;
        insert objLead;
        lstQueriedLeads = [Select Id, Name,CRM_CP_Eloqua_Integration__c, CRM_CP_ELQ_Colostomy__c,CRM_CP_ELQ_Ileostomy__c,CRM_CP_ELQ_Jejunostomy__c,CRM_CP_ELQ_Nephrostomy__c,CRM_CP_ELQ_Urostomy__c,CRM_CP_ELQ_Continent_Urostomy__c,CRM_CP_ELQ_Temporary_Stoma__c,CRM_CP_ELQ_Stoma_Opening_Position__c, CRM_CP_ELQ_Wheelchair__c, CRM_CP_ELQ_Surgery_Date__c, CRM_CP_ELQ_Catheter_Usage_Date__c    From Lead];

        for(Lead leadToUpdate : lstQueriedLeads){
            if(leadToUpdate.CRM_CP_Eloqua_Integration__c){
                leadToUpdate.CRM_CP_ELQ_Colostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Ileostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Jejunostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Nephrostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Urostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Continent_Urostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Temporary_Stoma__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Stoma_Opening_Position__c = 'Below Skin Surface';
                leadToUpdate.CRM_CP_ELQ_Wheelchair__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Surgery_Date__c = SYSTEM.TODAY() - 23;
                leadToUpdate.CRM_CP_ELQ_Catheter_Usage_Date__c = SYSTEM.TODAY() - 22;
            }else{
                leadToUpdate.CRM_CP_ELQ_Colostomy__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Ileostomy__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Jejunostomy__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Nephrostomy__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Urostomy__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Continent_Urostomy__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Temporary_Stoma__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Stoma_Opening_Position__c = 'Above Skin Surface';
                leadToUpdate.CRM_CP_ELQ_Wheelchair__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Surgery_Date__c = SYSTEM.TODAY() - 20;
                leadToUpdate.CRM_CP_ELQ_Catheter_Usage_Date__c = SYSTEM.TODAY() - 26;
            }

       }



       update lstQueriedLeads;

       Test.stopTest();

    }

        /**
    * @author       : Anisa
    * @date         : 23-Oct-2018
    * @description  : This method tests Eloqua Sync Logic on update of surgery date on lead.
    */
    @isTest static void testELQSyncSurgeryDate()
    {
        List<Lead> lstQueriedLeads = new List<Lead>();

       Test.startTest();

        lstQueriedLeads = [Select Id, Name,CRM_CP_Eloqua_Integration__c, CRM_CP_ELQ_Colostomy__c,
        						  CRM_CP_ELQ_Ileostomy__c,CRM_CP_ELQ_Jejunostomy__c,CRM_CP_ELQ_Nephrostomy__c,
        						  CRM_CP_ELQ_Urostomy__c,CRM_CP_ELQ_Continent_Urostomy__c,
        						  CRM_CP_ELQ_Temporary_Stoma__c,CRM_CP_ELQ_Stoma_Opening_Position__c,
        						  CRM_CP_ELQ_Wheelchair__c, CRM_CP_ELQ_Surgery_Date__c,
        						  CRM_CP_ELQ_Catheter_Usage_Date__c
        				   From Lead limit 10];

        for(Lead leadToUpdate : lstQueriedLeads){
            if(leadToUpdate.CRM_CP_Eloqua_Integration__c){
                leadToUpdate.CRM_CP_ELQ_Colostomy__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Ileostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Jejunostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Nephrostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Urostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Continent_Urostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Temporary_Stoma__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Stoma_Opening_Position__c = 'Below Skin Surface';
                leadToUpdate.CRM_CP_ELQ_Wheelchair__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Surgery_Date__c = SYSTEM.TODAY() - 23;
                leadToUpdate.CRM_CP_ELQ_Catheter_Usage_Date__c = SYSTEM.TODAY() - 24;
            }

        }
        update lstQueriedLeads;


       Test.stopTest();

    }

        /**
    * @author       : Mrua Shastri
    * @date         : 23-Oct-2018
    * @description  : This method tests Eloqua Sync Logic on Creation of Account.
    */
    @isTest static void testELQSyncLogicOnUpdate()
    {
        List<Lead> lstQueriedLeads = new List<Lead>();

       Test.startTest();

        lstQueriedLeads = [Select Id, Name,CRM_CP_Eloqua_Integration__c, CRM_CP_ELQ_Colostomy__c,CRM_CP_ELQ_Ileostomy__c,CRM_CP_ELQ_Jejunostomy__c,CRM_CP_ELQ_Nephrostomy__c,CRM_CP_ELQ_Urostomy__c,CRM_CP_ELQ_Continent_Urostomy__c,CRM_CP_ELQ_Temporary_Stoma__c,CRM_CP_ELQ_Stoma_Opening_Position__c, CRM_CP_ELQ_Wheelchair__c, CRM_CP_ELQ_Surgery_Date__c, CRM_CP_ELQ_Catheter_Usage_Date__c    From Lead];

        for(Lead leadToUpdate : lstQueriedLeads){
            if(leadToUpdate.CRM_CP_Eloqua_Integration__c){
                leadToUpdate.CRM_CP_ELQ_Colostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Ileostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Jejunostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Nephrostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Urostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Continent_Urostomy__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Temporary_Stoma__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Stoma_Opening_Position__c = 'Below Skin Surface';
                leadToUpdate.CRM_CP_ELQ_Wheelchair__c = 'N';
                leadToUpdate.CRM_CP_ELQ_Surgery_Date__c = SYSTEM.TODAY() - 23;
                leadToUpdate.CRM_CP_ELQ_Catheter_Usage_Date__c = SYSTEM.TODAY() - 24;
            }else{
                leadToUpdate.CRM_CP_ELQ_Colostomy__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Ileostomy__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Jejunostomy__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Nephrostomy__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Urostomy__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Continent_Urostomy__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Temporary_Stoma__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Stoma_Opening_Position__c = 'Above Skin Surface';
                leadToUpdate.CRM_CP_ELQ_Wheelchair__c = 'Y';
                leadToUpdate.CRM_CP_ELQ_Surgery_Date__c = SYSTEM.TODAY() - 20;
                leadToUpdate.CRM_CP_ELQ_Catheter_Usage_Date__c = SYSTEM.TODAY() - 26;
            }

        }
        update lstQueriedLeads;



       Test.stopTest();

    }

}