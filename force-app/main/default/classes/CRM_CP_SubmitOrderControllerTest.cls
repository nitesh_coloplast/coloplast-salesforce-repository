/**
* @author   	: Ninad Patil
* @date 		: 06 Aug 2018
* @description 	: This class is Test class for Sample Request to Case Order conversion utilities.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Ninad Patil - 
*/
@isTest
private class CRM_CP_SubmitOrderControllerTest {
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method fetches logged in User's OCN. ELse returns 190 as default
	*/
	static private string getOCN()
    {
        string strOCN = '';
        
        List<user> lstUser = [Select id, 
                                     name,
                                     CRM_CP_Subsidiary_Number__C
                                     From User 
                                     Where Id = :UserInfo.getuserId()];
        if(lstUser != null && !lstuser.isEmpty())
        {
            if(lstUser[0].CRM_CP_Subsidiary_Number__C != null) strOCN = lstUser[0].CRM_CP_Subsidiary_Number__C;
            else strOCN = '190';
        }
        
        return strOCN;
    }
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method creates data in the system before test methods are executed
	*/
    @testSetup static void setup() 
    {
        string strOCN = getOCN();
        
        //Create Products
		List<Product2> lstProd = new List<Product2>();
		lstProd.add(CRM_CP_TestDataUtility.createProduct('Test', '1'));
		lstProd.add(CRM_CP_TestDataUtility.createProduct('Test1', '2'));
		lstProd.add(CRM_CP_TestDataUtility.createProduct('Test2', '1'));
		lstProd.add(CRM_CP_TestDataUtility.createProduct('Test3', '1'));
		for(Product2 objProd :lstProd)
		{
			objProd.CRM_CP_Owner_Company_Number__c = strOCN;
		}
		lstProd[3].CRM_CP_Active_for_Product_Usage__c = false;
		insert lstProd;
        
        //Create Pricebooks
        Pricebook2 objPBSR = CRM_CP_TestDataUtility.createPriceBookSampleOrder();
        objPBSR.CRM_CP_Owner_Company_Number__c = strOCN;
        insert objPBSR;
        
        //Create Pricebook Enty
        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> lstPB = new List<PricebookEntry>();
        lstPB.add(new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = lstProd[0].Id,
            UnitPrice = 10000, IsActive = true,CRM_CP_External_ID__c = '123'));
        lstPB.add(new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = lstProd[1].Id,
            UnitPrice = 10000, IsActive = true,CRM_CP_External_ID__c = '1231'));
        lstPB.add(new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = lstProd[3].Id,
            UnitPrice = 10000, IsActive = true,CRM_CP_External_ID__c = '1232'));
        insert lstPB;
         
        List<PricebookEntry> lstPBE = new List<PricebookEntry>();
        lstPBE.add(CRM_CP_TestDataUtility.createPriceBook(lstProd[0], objPBSR));
        lstPBE.add(CRM_CP_TestDataUtility.createPriceBook(lstProd[1], objPBSR));
        lstPBE.add(CRM_CP_TestDataUtility.createPriceBook(lstProd[3], objPBSR));
        insert lstPBE;
        
        //Create Person Account
        Account objAcc = CRM_CP_TestDataUtility.createAccount('ConsumerLastName');
        objAcc.CRM_CP_Owner_Company_Number__c = strOCN;
        insert objAcc;
        
        Contact objCon = new Contact();
        List<Contact> lstCon = [Select id, name from Contact Where accountID = : objAcc.Id];
        if(lstCon != null && !lstCon.isEMpty()) objCon = lstCon[0];
        
        Campaign objCamp = CRM_CP_TestDataUtility.createConsumerCampaign('Test_Campaign');
        insert objCamp;
        
        //Create Case sample request
        Case objCase  = CRM_CP_TestDataUtility.createCaseSampleRequest(objAcc.ID, objCon.Id);
        objCase.CRM_CP_Source_Campaign__c = objCamp.Id;
        objCase.CRM_CP_Owner_Company_Number__c = strOCN;
        insert objCase;
        
        //Create Order
        Order objOrder = CRM_CP_TestDataUtility.createOrder(objAcc);
        objOrder.CRM_CP_Subsidiary_Number__c = strOCN;
        objOrder.pricebook2Id = objPBSR.Id;
        objOrder.CRM_CP_Contact__c = objCon.Id;
        objOrder.CRM_CP_Campaign_Referen__c = objCamp.Id;
        insert objOrder;
    }
    /** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method test conversion of case under Consumer Account.
	*/
    @isTest static void testConvertCase() 
    {
    	string strOCN = getOCN();
        List<product2> lstProd = [Select Id, name from Product2 where name like '%Test%'];
        
        List<Account> lstAcc = [Select id, name, (select id from Cases) from Account where name like '%ConsumerLastName' ];
        
        //Create Product Requests
        List<CRM_CP_Product_Request__c> lstProdReq = new List<CRM_CP_Product_Request__c>();
        for(integer intVar = 0; intVar<3; intVar ++)
        {
            lstProdReq.add(CRM_CP_TestDataUtility.createProdrequest(lstProd[0],lstAcc[0].cases[0], true));
        }
        for(CRM_CP_Product_Request__c objProdReq : lstProdReq) objProdReq.CRM_CP_Owner_Company_Number__c = strOCN;
            
        if(lstProdReq != null && !lstProdReq.isEmpty()) insert lstProdReq;
        
        Test.startTest();
        
        CRM_CP_SubmitOrderResponse objRes = CRM_CP_SubmitOrderController.convertToOrder(lstAcc[0].cases[0].Id);
        system.assertEquals(objRes.boolIsError, false);
        
        Test.stopTest();
            
    }
 	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method simulates Order to Product Usage creation.
	*/
    @isTest static void testforOrderSubmission() 
    {
        List<product2> lstProd = [Select Id, name from Product2 where name = 'Test'];
        
        List<Id> lstProdIds = new List<Id>();
        
        for(product2 objProd : lstProd) lstProdIds.add(objProd.Id);
        
        List<Account> lstAcc = [Select id, name, (select id from orders) 
        	from Account where name like '%ConsumerLastName' 
        	];
        
        List<PricebookEntry> lstPBE = [select id, product2.Name 
        	from PricebookEntry WHere Product2Id IN : lstProdIds
        		AND Pricebook2Id != :Test.getStandardPricebookId()];  
        //Add order line item
        OrderItem objOrderItem = CRM_CP_TestDataUtility.createOrderItem(lstAcc[0].orders[0], lstPBE[0], lstProd[0]);
        insert objOrderItem;
        
        Test.startTest();
        
        CRM_CP_SubmitOrderResponse objRes = CRM_CP_SubmitOrderController.convertToOrder(lstAcc[0].orders[0].id);
        system.assertEquals(objRes.boolIsError, false);
		
		Order orderins = new Order();
        orderins.Id = lstAcc[0].orders[0].id;
        orderins.Status = 'Draft';
        update orderins;
        delete orderins;
        undelete orderins;
        Test.stopTest();            
    }
    
   
    /** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method checks Order buttno visibility on the screen.
	*/
    @isTest static void testforOrdersButtonVisibility() 
    {
        List<product2> lstProd = [Select Id, name from Product2 where name = 'Test'];
        
        List<Id> lstProdIds = new List<Id>();
        
        for(product2 objProd : lstProd) lstProdIds.add(objProd.Id);
        
        List<Account> lstAcc = [Select id, name, (select id from orders) 
        	from Account where name like '%ConsumerLastName' 
        	];
        
        List<PricebookEntry> lstPBE = [select id, product2.Name 
        	from PricebookEntry WHere Product2Id IN : lstProdIds
        		AND Pricebook2Id != :Test.getStandardPricebookId()];  
        //Add order line item
        OrderItem objOrderItem = CRM_CP_TestDataUtility.createOrderItem(lstAcc[0].orders[0], lstPBE[0], lstProd[0]);
        insert objOrderItem;
        
        Test.startTest();
        
        CRM_CP_SubmitOrderResponse objRes = CRM_CP_SubmitOrderController.getButtonVisibility(lstAcc[0].orders[0].id);
        system.assertEquals(objRes.boolShowButton,true);
        
        Test.stopTest();            
    }
    
    /** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method checks Order buttno visibility on the screen.
	*/
    @isTest static void testforCaseButtonVisibility() 
    {
        List<product2> lstProd = [Select Id, name from Product2 where name = 'Test'];
        
        List<Id> lstProdIds = new List<Id>();
        
        for(product2 objProd : lstProd) lstProdIds.add(objProd.Id);
        
        List<Account> lstAcc = [Select id, name, (select id from cases) 
        	from Account where name like '%ConsumerLastName' 
        	];
        
        Test.startTest();
        
        CRM_CP_SubmitOrderResponse objRes = CRM_CP_SubmitOrderController.getButtonVisibility(lstAcc[0].cases[0].id);
        system.assertEquals(objRes.boolShowButton,true);
        
        Test.stopTest();            
    }
    
    /** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method simulates Sample Case submission to Order When Product is not haveing Procebook.
	*/
	@isTest static void testforCurrentProductUsage_Case() 
    {
    	string strOCN = getOCN();
        List<product2> lstProd = [Select Id, name,CRM_CP_Manufacturer__c from Product2 where Name = 'Test'];
        
        List<Account> lstAcc = [Select id, name, (select id from Cases) from Account where name like '%ConsumerLastName' ];
        
        //Create Product Usages
        CRM_CP_Product_Usage__c objProdUsage = CRM_CP_TestDataUtility.createProdUsage(lstAcc[0], lstProd[0]);
        objProdUsage.CRM_CP_Owner_Company_Number__c = strOCN;
        insert objProdUsage;
        
        //Create Product Requests
        List<CRM_CP_Product_Request__c> lstProdReq = new List<CRM_CP_Product_Request__c>();
        for(integer intVar = 0; intVar<3; intVar ++)
        {
            lstProdReq.add(CRM_CP_TestDataUtility.createProdrequest(lstProd[0],lstAcc[0].cases[0], true));
        }
        for(CRM_CP_Product_Request__c objProdReq : lstProdReq) objProdReq.CRM_CP_Owner_Company_Number__c = strOCN;
            
        if(lstProdReq != null && !lstProdReq.isEmpty()) insert lstProdReq;
        
        Test.startTest();
        
        CRM_CP_SubmitOrderResponse objRes = CRM_CP_SubmitOrderController.convertToOrder(lstAcc[0].cases[0].Id);
        system.assertEquals(objRes.boolIsError, false);
        objRes = CRM_CP_SubmitOrderController.createProductUsageRecords(JSON.serialize(objRes), lstAcc[0].cases[0].Id);
        
        Test.stopTest();
            
    }
    /** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method tests sample request case to Order for products with active prouct usages under account.
	*/
    @isTest static void testforCurrentProductUsage_Order() 
    {
    	string strOCN = getOCN();
        List<product2> lstProd = [Select Id, name,CRM_CP_Manufacturer__c from Product2 where name = 'Test'];
        
        List<Id> lstProdIds = new List<Id>();
        
        for(product2 objProd : lstProd) lstProdIds.add(objProd.Id);
        
        List<Account> lstAcc = [Select id, name, (select id from orders) 
        	from Account where name like '%ConsumerLastName' 
        	];
        
        //Create Product Usages
        CRM_CP_Product_Usage__c objProdUsage = CRM_CP_TestDataUtility.createProdUsage(lstAcc[0], lstProd[0]);
        objProdUsage.CRM_CP_Owner_Company_Number__c = strOCN;
        insert objProdUsage;
        
        List<PricebookEntry> lstPBE = [select id, product2.Name 
        	from PricebookEntry WHere Product2Id IN : lstProdIds
        		AND Pricebook2Id != :Test.getStandardPricebookId()];  
        //Add order line item
        OrderItem objOrderItem = CRM_CP_TestDataUtility.createOrderItem(lstAcc[0].orders[0], lstPBE[0], lstProd[0]);
        insert objOrderItem;
        
        Test.startTest();
        
        CRM_CP_SubmitOrderResponse objRes = CRM_CP_SubmitOrderController.convertToOrder(lstAcc[0].orders[0].id);
        system.assertEquals(objRes.boolIsError, false);
        objRes = CRM_CP_SubmitOrderController.createProductUsageRecords(JSON.serialize(objRes), lstAcc[0].orders[0].Id);
        //calling test exception handler method
        CRM_CP_Case_OrderConversionUtil.handleExceptions(objRes,null, null , 'Testclass' , 'TestMethod');
        Test.stopTest();
            
    }
}