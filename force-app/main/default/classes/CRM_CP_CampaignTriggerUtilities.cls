/*
* CRM_CP_SubmitOrderResponse : This class 
* 1. fetches data for Campaign Member Status from Custom Metadata.
* 2. It creates Campaign Member Status according to the data in the custom metadata.
* 3. Since Send and Responeded Statues already exists with sort order 1 and 2, we need to make sure other
*    statuses to be inserted has not same sort order, to handle, we 1st update the default Send and Responded Statues
*    and then insert the new statuses.
* 4. Delete the default Statuses 
* Created Date : 06-April-2018
* 
*/

public class CRM_CP_CampaignTriggerUtilities{
    
    
    public static void insertCampaignMemberStatus(Map<Id,sObject> mapOfCampaign){
        
        List<CampaignMemberStatus> listCampaignStatusToInsert = new List<CampaignMemberStatus>();
        List<CampaignMemberStatus> listCampaignStatusToUpdate = new List<CampaignMemberStatus>();
        List<CampaignMemberStatus> listCampaignStatusToDelete = new List<CampaignMemberStatus>();
        
        Map <String,List <CRM_CP_CampaignMemberStatus__mdt>> mapStatusByRecordType = new Map <String,List <CRM_CP_CampaignMemberStatus__mdt>>();
        Map<Id,List<String>> mapCampaignStatusExist = new Map<Id,List<String>>();
        Map<String,Integer> mapHighestOrderForCampaign = new Map<String,Integer>();
        
        List <Campaign> listCampaign = [Select Id, RecordTypeId,(Select Id,SortOrder, Label FROM CampaignMemberStatuses), CRM_CP_Eloqua_Integration__c, CRM_CP_Coloplast_Programme__c, RecordType.Name FROM Campaign WHERE ID IN : mapOfCampaign.KeySet()];
        List<CampaignMemberStatus> listCampaignStatus = new List<CampaignMemberStatus>();
        
        List<CRM_CP_CampaignMemberStatus__mdt> listCampaignMemberStatusMetadata = [Select  CRM_CP_HasResponded__c,CRM_CP_Eloqua_Integration__c,CRM_CP_SortOrder__c,CRM_CP_Label__c,CRM_CP_Is_Default__c,CRM_CP_Record_Type__c from CRM_CP_CampaignMemberStatus__mdt];
        if(!listCampaignMemberStatusMetadata.IsEmpty()){
            Integer hightestSortOrder = 0;
            for(CRM_CP_CampaignMemberStatus__mdt cmsMetadata : listCampaignMemberStatusMetadata){
                if(mapStatusByRecordType.containsKey(cmsMetadata.CRM_CP_Record_Type__c))
                    mapStatusByRecordType.get(cmsMetadata.CRM_CP_Record_Type__c).add(cmsMetadata);
                else
                    mapStatusByRecordType.put(cmsMetadata.CRM_CP_Record_Type__c, new List <CRM_CP_CampaignMemberStatus__mdt>{cmsMetadata});
                if(hightestSortOrder < cmsMetadata.CRM_CP_SortOrder__c){
                    hightestSortOrder = Integer.ValueOf(cmsMetadata.CRM_CP_SortOrder__c);
                }
                mapHighestOrderForCampaign.put(cmsMetadata.CRM_CP_Record_Type__c,hightestSortOrder);
            }
            
            for(Campaign cmpg : listCampaign){
                for(CampaignMemberStatus cmStatus : cmpg.CampaignMemberStatuses){
                    if(mapCampaignStatusExist.containsKey(cmpg.Id)){
                        mapCampaignStatusExist.get(cmpg.Id).add(cmStatus.Label);
                    }
                    else
                        mapCampaignStatusExist.put(cmpg.Id,new List<String>{cmStatus.Label});
                }
            }    
            
            /*
            Date: 17/05/2018
            Developer: Joseph Abi-Khalil 
            This method is used for Eloqua integration. It's aim is that everytime we have "Eloqua Integration" set to true and "Coloplast Programme" = Coloplast care 
               the possible statuses for the campaign members are "Opted Out and "Enrolled"*/
            
            for(Campaign camp: listCampaign){
               // Capture existing statuses
                Integer highestOrder = mapHighestOrderForCampaign.get(camp.RecordType.Name); 
                for(CampaignMemberStatus cms : camp.CampaignMemberStatuses){
                    cms.SortOrder = cms.SortOrder + highestOrder;
                    listCampaignStatusToUpdate.add(cms);
                }
      
                if(mapStatusByRecordType.containsKey(camp.RecordType.Name)){
                    List <CRM_CP_CampaignMemberStatus__mdt> listMetadata = mapStatusByRecordType.get(camp.RecordType.Name);
                    for(CRM_CP_CampaignMemberStatus__mdt campData: listMetadata){
                        if(!mapCampaignStatusExist.IsEmpty() && !mapCampaignStatusExist.get(camp.Id).contains(campData.CRM_CP_Label__c)){
                            CampaignMemberStatus campaignStatus = new CampaignMemberStatus();
                            if(camp.CRM_CP_Eloqua_Integration__c && camp.CRM_CP_Coloplast_Programme__c == 'Coloplast Care'){
                                if(campData.CRM_CP_Eloqua_Integration__c){
                                    campaignStatus = new CampaignMemberStatus(CampaignId = camp.Id,HasResponded = campData.CRM_CP_HasResponded__c,Label = campData.CRM_CP_Label__c,
                                                                                           SortOrder=(Integer)campData.CRM_CP_SortOrder__c,IsDefault=campData.CRM_CP_Is_Default__c);
                                }
                            }
                            else
                            {
                                if(!campData.CRM_CP_Eloqua_Integration__c)
                                campaignStatus = new CampaignMemberStatus(CampaignId = camp.Id,HasResponded = campData.CRM_CP_HasResponded__c,Label = campData.CRM_CP_Label__c,
                                                                                           SortOrder=(Integer)campData.CRM_CP_SortOrder__c,IsDefault=campData.CRM_CP_Is_Default__c);
                            }
                            if(campaignStatus.CampaignId != null)
                            listCampaignStatusToInsert.add(campaignStatus);
                        }
                    }
                }

            }
            if(!listCampaignStatusToUpdate.IsEmpty())
                update listCampaignStatusToUpdate;
            if(!listCampaignStatusToInsert.IsEmpty())
                insert listCampaignStatusToInsert;
            if(!listCampaignStatusToUpdate.IsEmpty())
                delete listCampaignStatusToUpdate;
        }
        
    } 
   
    public static void updateOverallTargetAudienceAfter(Map<Id,Sobject> oldItems, Map<Id,Sobject> newItems){
       List<Campaign> campaignsToUpdate = new List<Campaign>();
       Campaign OldCmp = new Campaign();
       List<Campaign> newCampaigns = [SELECT id,CRM_CP_Overall_Target_Audience__c ,CRM_CP_Overall_Target_Audience_Text__c FROM Campaign WHERE ID IN:newItems.keySet()];
       
       for(Campaign eachCampaign : newCampaigns){
               
               OldCmp = (Campaign)Trigger.oldMap.get(eachCampaign.Id);
               if(OldCmp.CRM_CP_Overall_Target_Audience__c != newItems.get((String)eachCampaign.get('Id')).get('CRM_CP_Overall_Target_Audience__c') ){
                 eachCampaign.CRM_CP_Overall_Target_Audience_Text__c = String.ValueOf(newItems.get((String)eachCampaign.get('Id')).get('CRM_CP_Overall_Target_Audience__c'));
                 campaignsToUpdate.add(eachCampaign);
               } 
       }
       update campaignsToUpdate;

    }
    
    public static void updateOverallTargetAudience(Map<Id,Sobject> oldItems, Map<Id,Sobject> newItems){
       List<Campaign> campaignsToUpdate = new List<Campaign>();
       Campaign newCmp = new Campaign();
       for(Sobject eachCampaign : newItems.values()){
               newCmp = (Campaign)eachCampaign;
               if(oldItems.get((String)eachCampaign.get('Id')).get('CRM_CP_Overall_Target_Audience__c') != newItems.get((String)eachCampaign.get('Id')).get('CRM_CP_Overall_Target_Audience__c') ){
                 newCmp.CRM_CP_Overall_Target_Audience_Text__c = String.ValueOf(newItems.get((String)eachCampaign.get('Id')).get('CRM_CP_Overall_Target_Audience__c'));
               } 
       }
    }
    
    public static void insertOverallTargetAudience(List<Sobject> newItems){
        Campaign newCmp = new Campaign();
        for(SObject eachCampaign : newItems){ 
            newCmp = (Campaign)eachCampaign ;
            newCmp.CRM_CP_Overall_Target_Audience_Text__c = newCmp.CRM_CP_Overall_Target_Audience__c;
        }
    }
    
}