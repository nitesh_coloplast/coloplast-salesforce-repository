@isTest(seeAllData=false)
public class CRM_CP_ConsentDetailsController_Test {
    
    @testSetup static void setup() {
        List<sObject> lstRecords = new List<sObject>();
        
        
        String AccountRecordTypeId, ContactRecordTypeId, LeadRecordTypeId;
        
        AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
        ContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Professional Contact').getRecordTypeId();
        LeadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Patient').getRecordTypeId();

                
        Account objAcc = new Account(RecordTypeId=AccountRecordTypeId, FirstName='TestAccountFname', LastName='TestAccountLName');
        lstRecords.add(objAcc);
        Contact objCon = new Contact(RecordTypeId=ContactRecordTypeId, FirstName='TestContactFname', LastName='TestContactLName',CRM_CP_Contact_Type__c='Educator');
        lstRecords.add(objCon);
        Lead objlead = new Lead(RecordTypeId=LeadRecordTypeId, FirstName='TestLeadFname', LastName='TestLeadName',Status='New');
        lstRecords.add(objlead);
        
        try{
            insert lstRecords; 
        }
        catch(Exception ex){
            system.debug('Test Fail :' +ex.getMessage());
        }
    }
    
    public static testMethod void testConsentScenariosForAccount() {
            
            Test.startTest();
                try{
                    List<Account> acc = [select Id from Account LIMIT 1];
                    if(!acc.isEmpty()){
                        CRM_CP_ConsentDetailsController.getIndividualRecordId(acc[0].Id);
                        delete acc;
                        CRM_CP_ConsentDetailsController.getIndividualRecordId(acc[0].Id);
                    }
                    
                    List<Contact> contact = [select Id from Contact LIMIT 1];
                    if(!contact.isEmpty()){
                        CRM_CP_ConsentDetailsController.getIndividualRecordId(contact[0].Id);
                        delete contact;
                        CRM_CP_ConsentDetailsController.getIndividualRecordId(contact[0].Id);
                    }
                    
                    List<Lead> lead = [select Id from Lead LIMIT 1];
                    if(!lead.isEmpty()){
                        CRM_CP_ConsentDetailsController.getIndividualRecordId(lead[0].Id);
                        delete lead;
                        CRM_CP_ConsentDetailsController.getIndividualRecordId(lead[0].Id);
                    }
                }
                catch(Exception ex){
                    system.debug('Test Fail :' +ex.getMessage());
                }
            Test.stopTest();    
     }
     
    
    
}