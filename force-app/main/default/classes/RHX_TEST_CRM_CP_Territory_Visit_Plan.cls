@isTest(SeeAllData=true)
public class RHX_TEST_CRM_CP_Territory_Visit_Plan {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM CRM_CP_Territory_Visit_Plan__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new CRM_CP_Territory_Visit_Plan__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}