/**
* @author       : Nitesh Kumar
* @date         : 08/28/2018
* @description  : This class is handling the translation of picklist values and storing in the same record as text values.
*                 This is a generic class and will be used for all the objects.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Nitesh Kumar - Creted all the method as part of US-1354
*/
public class CRM_CP_Translated_Search_Enabler {
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 08/28/2018
    * @description  : This method will translate the picklist values of the record and store them back to the object record
    * @param        : newItems - this will accept the records for which translation needs to happen.
    */
    public static void runTranslatedSearchEnabler(List<sObject> newItems){
        
        if(!newItems.isEmpty()){
            
            Map<String, Schema.SObjectField> mapFieldResult = Schema.SObjectType.CRM_CP_Translated_Search_Configuration__c.fields.getMap();
            List<String> lstFields = new List<String>();

            //Store the number of languages configured
            for(Schema.SObjectField field : mapFieldResult.Values()){
                //Store only Custom fields
                if(field.getDescribe().isCustom()){
                    lstFields.add(field.getDescribe().getName());
                }     
            }        
            
            //Fetch all the translations according to the picklist values
            Map<String, sObject> translationMap = CRM_CP_Translated_Search_Configuration__c.getAll();
            
            if(!lstFields.isEmpty()){
                
                //Store the Object API name
                String objectAPI = newItems[0].getSObjectType().getDescribe().getName();
                
                //Fetch all the fields configured for the current (runnin) object.
                List<CRM_CP_Fields_Available_for_Search__mdt> fieldsList = [ SELECT MasterLabel,
                                                                            CRM_CP_Field_To_Store_Translation__c , 
                                                                            CRM_CP_Fields__c 
                                                                            FROM CRM_CP_Fields_Available_for_Search__mdt 
                                                                            WHERE MasterLabel = :objectAPI LIMIT 1];
                
                
                if(!fieldsList.isEmpty()){
                    
                    List<String> fields = new List<String>();
                    
                    //Fields are stored in multiple lines so split the data on the basis of ';'
                    if(fieldsList[0].CRM_CP_Fields__c != null){
                        fields = fieldsList[0].CRM_CP_Fields__c.split(';');
                    }
                    
                    //Loop through the data and update the translations
                    for(sObject prod : newItems){
                        
                        String translatedValues = ''; 
                        
                        for(String field : fields){
                            if(prod.get(field) != null && translationMap.get((String)prod.get(field)) != null){
                                
                                //Store the Engligh value of picklist value.
                                translatedValues += (String) prod.get(field);
                                
                                //Now Store the other translation values of picklist value.
                                for(String configField : lstFields){
                                    if(translationMap.get((String)prod.get(field)).get(configField) != null){
                                        translatedValues += ' | ' + translationMap.get((String)prod.get(field)).get(configField);
                                    }
                                }
                                translatedValues += '\n';
                            }
                        }
                        prod.put(fieldsList[0].CRM_CP_Field_To_Store_Translation__c,translatedValues);
                    }
                }
            }
        }
    }
}