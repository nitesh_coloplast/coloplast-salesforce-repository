/*********************************************************************************
* @author         Namrata Bansode
* @description    class for ErrorloggingComponent
* @date	     	  2018-02-20
* @group          
**********************************************************************************/
public class CustomErrorLoggingController {
    
    public ErrorLogWrapper errorWrapper{get;set;}
    
    /**********************************************************************************
    * @author			Namrata Bansode
    * @date				2018-02-20
    * @description     	method call will be done to insert error in ErrorLog Object
    
    *******************************************************************************/
    
    public void  logErrorMessage(){
        List < CRM_CP_Error_Log__c > lstErrors = new List < CRM_CP_Error_Log__c >();
        if(errorWrapper != null){
            lstErrors = ExceptionHelper.logErrors(errorWrapper.exceptionName, 
                                                  errorWrapper.methodName, errorWrapper.className); 
            
        }
    }
}