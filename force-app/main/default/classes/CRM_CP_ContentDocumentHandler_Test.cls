/**
* @author       : Prity Kumari
* @date         : 02/19/019
* @description  : This class is Test class for ContentDocument and ContentDocumentLink Handler class functionality.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Prity Kumari - 02/19/019
*/

@isTest
public class CRM_CP_ContentDocumentHandler_Test {
    private static testMethod void testPopulateAttachmentLastModifiedDate(){
          Id AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
          Account objAcc = new Account(RecordTypeId=AccountRecordTypeId, 
                                       Name='TestAccountFname', 
                                       CRM_CP_Account_status__c  = 'Draft',
                                       Type = 'Clinic/Medical Centre',
                                       CRM_CP_Owner_Company_Number__c = '190');
          insert objAcc; 
          CRM_CP_ASAP__c objASAP = new CRM_CP_ASAP__c(CRM_CP_Account_Name__c = objAcc.Id, 
                                                      CRM_CP_Status__c = 'Active', 
                                                      CRM_CP_Fiscal_Year__c = '19/20',
                                                      CRM_CP_Sub_Business_Areas__c  = 'BM',
                                                      CRM_CP_Attachment_LastModifiedDate__c= null);
          insert objASAP;
          
          ContentVersion objcontentVersion = new ContentVersion(Title = 'Penguins',
                                                             PathOnClient = 'Penguins.jpg',
                                                             VersionData = Blob.valueOf('Test Content'),
                                                             IsMajorVersion = true);
                                                              
          insert objcontentVersion; 
          
          List<ContentDocument> lstDocuments = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
          
          //create ContentDocumentLink  record 
          ContentDocumentLink objCDL = New ContentDocumentLink();
          objCDL.LinkedEntityId = objASAP.id;
          objCDL.ContentDocumentId = lstDocuments[0].Id;
          objCDL.shareType = 'V';
         
          insert objCDL;
          
          objcontentVersion.CRM_CP_File_Type__c = 'Excel';
          update objcontentVersion;
       
                    
          delete lstDocuments;
          List<ContentDocument> lstUndeleteDoc = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument ALL ROWS];
          undelete lstUndeleteDoc;
          
      }
}