/*
 * CRM_CP_UserUpDateValidator : This class accepts User object and checks 
 	for data modifications based on CPBC user permissions
 *
 * Created Date : 30 - August - 2018
 * 
 Modification Log
 --------------------------------------------------------------------
 DeveloperName       Date           User Story		Description
 --------------------------------------------------------------------
 Ninad              30-Aug-2018     US1274          This class accepts 
 													User object and checks 
 													for data modifications 
 													based on CPBC user permissions                                                  
 */
public with sharing class CRM_CP_OpportunityTriggerHandler implements CRM_CP_TriggerInterface
{
	// Allows unit tests (or other code) to disable this trigger for the transaction
	public static Boolean TriggerDisabled = false;
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will check whether the account trigger is disaled or not. The configuration is stored in Trigger Setting custom metadata.
	*/
	public Boolean IsDisabled()
	{
		return CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserId()).CRM_CP_Triggers_Disabled__c;
    }
    
    /** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records before inserting the data to the database(NOT commit)..
	* @param        : newItems - this will accepts the new reocords of Case.
	*/
	public void BeforeInsert(List<SObject> newItems) {
		CRM_CP_PricebookAssignmentUtility.assignPricebook(newItems);
	}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records before updating the data to the database(NOT commit)..
	* @param        : newItems - this will accepts the new reocords of Case.
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records before deleting the data from the database(NOT commit)..
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	private void handleContactProtocolDuplicate(list<Case> lstCase){}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records before deleting the data from the database(NOT commit)..
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	public void BeforeDelete(Map<Id, SObject> oldItems) {}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records after deleting the data from the database(NOT commit).
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	public void AfterDelete(Map<Id, SObject> oldItems) {}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records after undeleting the data from the database(NOT commit).
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	public void AfterUndelete(Map<Id, SObject> oldItems) {}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records after undeleting the data from the database(NOT commit).
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	public void AfterInsert(Map<Id, SObject> oldItems) {}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records after undeleting the data from the database(NOT commit).
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	public void AfterUpdate(Map<Id, SObject> newItems,Map<Id, SObject> oldItems) {} 
	
	
}