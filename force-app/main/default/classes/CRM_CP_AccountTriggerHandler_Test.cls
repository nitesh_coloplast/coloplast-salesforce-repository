@isTest
public class CRM_CP_AccountTriggerHandler_Test {
    
    @testSetup static void setup() {
        
        List<Account> lstAccount = new List<Account>();
        List<CRM_CP_Health_Basics__c> listHealthBasics = new List<CRM_CP_Health_Basics__c>();
        
        Id AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
        Id HealthBasicsRecordTypeId = Schema.SObjectType.CRM_CP_Health_Basics__c.getRecordTypeInfosByName().get('Outcome').getRecordTypeId();
        Id patientInfoRectypeId = Schema.SObjectType.CRM_CP_Health_Basics__c.getRecordTypeInfosByName().get('Other Health Related Information').getRecordTypeId();
        
        for(integer i = 0; i < 4; i++){
            Account objAcc = new Account(RecordTypeId=AccountRecordTypeId, FirstName='TestAccountFname_'+i, LastName='TestAccountLName_'+i,CRM_CP_Eloqua_Integration__pc=True,
            CRM_CP_Contact_Status__pc='Active',CRM_CP_Update_Health_Basic__pc=false, CRM_CP_Owner_Company_Number__c = '190');
            lstAccount.add(objAcc);   
        }
        
        if(!lstAccount.isEmpty()){
            insert lstAccount;
        }

// ACC1 - Has HB children with all stoma types and 2 mobolity records. 

                 CRM_CP_Health_Basics__c objHealthBasics = new CRM_CP_Health_Basics__c(RecordTypeId = HealthBasicsRecordTypeId,CRM_CP_Outcome__c='Colostomy',CRM_CP_Account_Name_Legal_Name__c=lstAccount[0].Id,
                 CRM_CP_Date_of_Surgery__c = System.today(),CRM_CP_Stoma_Status__c='Permanent');
                 listHealthBasics.add(objHealthBasics);
                 
                 objHealthBasics = new CRM_CP_Health_Basics__c(RecordTypeId = HealthBasicsRecordTypeId,CRM_CP_Outcome__c='Ileostomy',CRM_CP_Account_Name_Legal_Name__c=lstAccount[0].Id,
                 CRM_CP_Date_of_Surgery__c = System.today(),CRM_CP_Stoma_Status__c='Permanent');
                 listHealthBasics.add(objHealthBasics);
                 
                 objHealthBasics = new CRM_CP_Health_Basics__c(RecordTypeId = HealthBasicsRecordTypeId,CRM_CP_Outcome__c='Continent Urostomy (Mitrofanoff)' ,CRM_CP_Account_Name_Legal_Name__c=lstAccount[0].Id,
                 CRM_CP_Date_of_Surgery__c = System.today(),CRM_CP_Stoma_Status__c='Permanent');
                 listHealthBasics.add(objHealthBasics);
                 
                 objHealthBasics = new CRM_CP_Health_Basics__c(RecordTypeId = HealthBasicsRecordTypeId,CRM_CP_Outcome__c='Jejunostomy' ,CRM_CP_Account_Name_Legal_Name__c=lstAccount[0].Id,
                 CRM_CP_Date_of_Surgery__c = System.today(),CRM_CP_Stoma_Status__c='Permanent');
                 listHealthBasics.add(objHealthBasics);
                 
                 objHealthBasics = new CRM_CP_Health_Basics__c(RecordTypeId = HealthBasicsRecordTypeId,CRM_CP_Outcome__c='Nephrostomy' ,CRM_CP_Account_Name_Legal_Name__c=lstAccount[0].Id,
                 CRM_CP_Date_of_Surgery__c = System.today(),CRM_CP_Stoma_Status__c='Permanent');                 
                 listHealthBasics.add(objHealthBasics);
                 
                 objHealthBasics = new CRM_CP_Health_Basics__c(RecordTypeId = HealthBasicsRecordTypeId,CRM_CP_Outcome__c='Urostomy' ,CRM_CP_Account_Name_Legal_Name__c=lstAccount[0].Id,
                 CRM_CP_Date_of_Surgery__c = System.today(),CRM_CP_Stoma_Status__c='Permanent');                 
                 listHealthBasics.add(objHealthBasics); 
                 
                 CRM_CP_Health_Basics__c objHealthBasics1 = new CRM_CP_Health_Basics__c(RecordTypeId = patientInfoRectypeId, CRM_CP_Mobility__c='Wheelchair', CRM_CP_Status__c='Current', CRM_CP_Account_Name_Legal_Name__c=lstAccount[0].Id);
                 listHealthBasics.add(objHealthBasics1);
                 
                 objHealthBasics1 = new CRM_CP_Health_Basics__c(RecordTypeId = patientInfoRectypeId, CRM_CP_Mobility__c='Wheelchair', CRM_CP_Status__c='Current', CRM_CP_Account_Name_Legal_Name__c=lstAccount[0].Id);
                 listHealthBasics.add(objHealthBasics1);
                 
//ACC2 - Has HB children with only 1 stoma types and 1 mobolity record.               
                 objHealthBasics = new CRM_CP_Health_Basics__c(RecordTypeId = HealthBasicsRecordTypeId,CRM_CP_Outcome__c='Colostomy' ,CRM_CP_Account_Name_Legal_Name__c=lstAccount[1].Id,
                 CRM_CP_Date_of_Surgery__c = System.today()-5,CRM_CP_Stoma_Status__c='Permanent');                 
                 listHealthBasics.add(objHealthBasics);
                 
                 objHealthBasics1 = new CRM_CP_Health_Basics__c(RecordTypeId = patientInfoRectypeId, CRM_CP_Mobility__c='Wheelchair', CRM_CP_Status__c='Current', CRM_CP_Account_Name_Legal_Name__c=lstAccount[1].Id);
                 listHealthBasics.add(objHealthBasics1);


// ACC3 - Has HB children with all stoma types and 1 mobolity record 

                 objHealthBasics = new CRM_CP_Health_Basics__c(RecordTypeId = HealthBasicsRecordTypeId,CRM_CP_Outcome__c='Colostomy',CRM_CP_Account_Name_Legal_Name__c=lstAccount[2].Id,
                 CRM_CP_Date_of_Surgery__c = System.today(),CRM_CP_Stoma_Status__c='Permanent');
                 listHealthBasics.add(objHealthBasics);
                 
                 objHealthBasics = new CRM_CP_Health_Basics__c(RecordTypeId = HealthBasicsRecordTypeId,CRM_CP_Outcome__c='Ileostomy',CRM_CP_Account_Name_Legal_Name__c=lstAccount[2].Id,
                 CRM_CP_Date_of_Surgery__c = System.today(),CRM_CP_Stoma_Status__c='Permanent');
                 listHealthBasics.add(objHealthBasics);
                 
                 objHealthBasics = new CRM_CP_Health_Basics__c(RecordTypeId = HealthBasicsRecordTypeId,CRM_CP_Outcome__c='Continent Urostomy (Mitrofanoff)' ,CRM_CP_Account_Name_Legal_Name__c=lstAccount[2].Id,
                 CRM_CP_Date_of_Surgery__c = System.today(),CRM_CP_Stoma_Status__c='Permanent');
                 listHealthBasics.add(objHealthBasics);
                 
                 objHealthBasics = new CRM_CP_Health_Basics__c(RecordTypeId = HealthBasicsRecordTypeId,CRM_CP_Outcome__c='Jejunostomy' ,CRM_CP_Account_Name_Legal_Name__c=lstAccount[2].Id,
                 CRM_CP_Date_of_Surgery__c = System.today(),CRM_CP_Stoma_Status__c='Permanent');
                 listHealthBasics.add(objHealthBasics);
                 
                 objHealthBasics = new CRM_CP_Health_Basics__c(RecordTypeId = HealthBasicsRecordTypeId,CRM_CP_Outcome__c='Nephrostomy' ,CRM_CP_Account_Name_Legal_Name__c=lstAccount[2].Id,
                 CRM_CP_Date_of_Surgery__c = System.today(),CRM_CP_Stoma_Status__c='Permanent');                 
                 listHealthBasics.add(objHealthBasics);
                 
                 objHealthBasics = new CRM_CP_Health_Basics__c(RecordTypeId = HealthBasicsRecordTypeId,CRM_CP_Outcome__c='Urostomy' ,CRM_CP_Account_Name_Legal_Name__c=lstAccount[2].Id,
                 CRM_CP_Date_of_Surgery__c = System.today(),CRM_CP_Stoma_Status__c='Permanent');                 
                 listHealthBasics.add(objHealthBasics); 
                 
                 objHealthBasics1 = new CRM_CP_Health_Basics__c(RecordTypeId = patientInfoRectypeId, CRM_CP_Mobility__c='Wheelchair', CRM_CP_Status__c='Current', CRM_CP_Account_Name_Legal_Name__c=lstAccount[2].Id);
                 listHealthBasics.add(objHealthBasics1);
        insert listHealthBasics;
     }

     
     public static testMethod void TestStomaCountNot1TOY(){
            
            List <Account> listAccounts = new List <Account>();
            Test.startTest();
            List <account> accList = [select id from account WHERE Lastname = 'TestAccountLName_0' OR Lastname = 'TestAccountLName_3'];

                for(Account acc : accList){

                    acc.CRM_CP_ELQ_Colostomy__pc = 'Y';
                    acc.CRM_CP_ELQ_Continent_Urostomy__pc= 'Y';
                    acc.CRM_CP_ELQ_Ileostomy__pc= 'Y';
                    acc.CRM_CP_ELQ_Jejunostomy__pc= 'Y';
                    acc.CRM_CP_ELQ_Nephrostomy__pc= 'Y';
                    acc.CRM_CP_ELQ_Urostomy__pc= 'Y';
                    acc.CRM_CP_ELQ_Wheelchair__pc= 'Y';
                    acc.CRM_CP_ELQ_Surgery_Date__pc= System.today() - 10;
                    acc.CRM_CP_ELQ_Temporary_Stoma__pc= 'Y';

            }
            update accList;

            System.assertNotEquals(accList,null);
            Test.stopTest();    
     }
     
     public static testMethod void TestStomaCountNot1TON(){
            
            List <account> accList = [select id, Lastname, CRM_CP_ELQ_Colostomy__pc, CRM_CP_ELQ_Wheelchair__pc, CRM_CP_ELQ_Temporary_Stoma__pc, CRM_CP_ELQ_Surgery_Date__pc, CRM_CP_Update_Health_Basic__pc from account WHERE Lastname = 'TestAccountLName_0' OR Lastname = 'TestAccountLName_2'];
            System.debug(' QUeried account in the Neg method : ' + accList);
            Test.startTest();

        
            for(Account acc : accList){
                acc.CRM_CP_ELQ_Colostomy__pc = 'N';
                acc.CRM_CP_ELQ_Continent_Urostomy__pc= 'N';
                acc.CRM_CP_ELQ_Ileostomy__pc= 'N';
                acc.CRM_CP_ELQ_Jejunostomy__pc= 'N';
                acc.CRM_CP_ELQ_Nephrostomy__pc= 'N';
                acc.CRM_CP_ELQ_Urostomy__pc= 'N';
                acc.CRM_CP_ELQ_Wheelchair__pc= 'N';
                acc.CRM_CP_ELQ_Surgery_Date__pc= System.today() - 20;
                acc.CRM_CP_ELQ_Temporary_Stoma__pc= 'N';

            }
            update accList;

            System.assertNotEquals(accList,null);
            Test.stopTest();    
     }
     
      public static testMethod void TestStomaCount1(){
            
            List <Account> listAccounts = new List <Account>();
            Test.startTest();

            List <account> accList = [select id from account WHERE Lastname = 'TestAccountLName_1'];
                for(Account acc : accList){
              
                    acc.CRM_CP_ELQ_Surgery_Date__pc= System.today() - 10;
                    acc.CRM_CP_ELQ_Temporary_Stoma__pc= 'Y';
                 }
                update accList;
            
            System.assertNotEquals(accList,null);
            Test.stopTest();    
     }
     
     public static testMethod void testMethodToTempStomaToN() {
            List <Account> listAccounts = new List <Account>();
            Test.startTest();
            for(Account acc : [Select Id from Account])
            {
                acc.CRM_CP_ELQ_Surgery_Date__pc= System.today();
                acc.CRM_CP_ELQ_Temporary_Stoma__pc= 'N';
                listAccounts.add(acc);
            }
            update listAccounts;
            System.assertNotEquals(listAccounts,null);
            Test.stopTest();    
     }
     
     public static testMethod void testMethodToTempStomaToY() {
            List <Account> listAccounts = new List <Account>();
            Test.startTest();
                for(Account acc : [Select Id from Account])
                {
                    acc.CRM_CP_ELQ_Surgery_Date__pc= System.today();
                    acc.CRM_CP_ELQ_Temporary_Stoma__pc= 'Y';
                    listAccounts.add(acc);
                }
             update listAccounts;
             System.assertNotEquals(listAccounts,null);
             delete listAccounts;
             undelete listAccounts;
            Test.stopTest();    
     }
}