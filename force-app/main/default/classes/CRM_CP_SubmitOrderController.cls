/*
 * CRM_CP_SubmitOrderController : This class is	controller for the lightning Action
 * Created Date : 16 - March - 2018
 */

public class CRM_CP_SubmitOrderController 
{
	/*
	 * convertToOrder : This method gets called on Submit order button from front end.
	 * @param idCase : This is case Id which is required to be converted.
	 * @return CRM_CP_SubmitOrderResponse
	 * @throws DMLExceptions via CRM_CP_SubmitOrderResponse class
	 */	
	@AuraEnabled
    public static CRM_CP_SubmitOrderResponse convertToOrder(Id idRecord)
    {
    	string strobjAPI = '';
    	CRM_CP_SubmitOrderResponse objResponse = new CRM_CP_SubmitOrderResponse();
    	if(idRecord != null && !String.isblank(idRecord))
    		strobjAPI = idRecord.getSObjectType().getDescribe().getName();
    		
    	objResponse.strobjAPI = strobjAPI;
    	
    	if(strobjAPI == 'Case') 
    		objResponse = CRM_CP_Case_OrderConversionUtil.convertToOrder(idRecord);
    		
    	else if (strobjAPI == 'Order') 
			objResponse = CRM_CP_Case_OrderConversionUtil.createProdUsageForOrder(idRecord);
		
		return objResponse;
    }
    /*
	 * createProductUsageRecords : This method gets called on Clicking Next when We select choose Old Product Usage and save
	 * @param idCase : This is case Id which is required to be converted.
	 * @return CRM_CP_SubmitOrderResponse
	 * @throws DMLExceptions via CRM_CP_SubmitOrderResponse class
	 */	
	@AuraEnabled
    public static CRM_CP_SubmitOrderResponse createProductUsageRecords(String objResponseForSave, Id idRecord)
    {
    	string strobjAPI = '';
    	if(idRecord != null && !String.isblank(idRecord))
    		strobjAPI = idRecord.getSObjectType().getDescribe().getName();
    		
    	CRM_CP_SubmitOrderResponse objResponse = (CRM_CP_SubmitOrderResponse)JSON.deserialize(objResponseForSave, CRM_CP_SubmitOrderResponse.class);
    	
    	objResponse.strobjAPI = strobjAPI;  
    	
    	if(strobjAPI == 'Case') 
    		objResponse = CRM_CP_Case_OrderConversionUtil.convertToOrderSubmission(objResponse, idRecord);
    	else if (strobjAPI == 'Order') 
    		objResponse = CRM_CP_Case_OrderConversionUtil.saveProductUsageForOrder(objResponse);
    	    
        return objResponse;
    }
    /*
	 * getButtonVisibility : This method Checks whether buttons should be visible or not
	 * @param idCase : This is case Id which is required to be converted.
	 * @return CRM_CP_SubmitOrderResponse
	 * @throws DMLExceptions via CRM_CP_SubmitOrderResponse class
	 */	
    @AuraEnabled
    public static CRM_CP_SubmitOrderResponse getButtonVisibility(id idRecord)
    {
    	CRM_CP_SubmitOrderResponse objResponse = new CRM_CP_SubmitOrderResponse();
    	
    	string strobjAPI = '';
    	if(idRecord != null && !String.isblank(idRecord))
    		strobjAPI = idRecord.getSObjectType().getDescribe().getName();
    		
    	if(strobjAPI == 'Case') 
    		objResponse = getCaseButtonVisibility(idRecord);
    	else if (strobjAPI == 'Order') 
    		objResponse = getOrderButtonVisibility(idRecord);
    		
    	objResponse.strobjAPI = strobjAPI;    
    	
    	return objResponse;
    }
    /*
	 * getCaseButtonVisibility : This methods controls visisbility of Submit Order 
	 	button on screen.
	 * @param idCase : This is case Id which is required to be converted.
	 * @return CRM_CP_SubmitOrderResponse
	 * @throws DMLExceptions via CRM_CP_SubmitOrderResponse class
	 */	
    @AuraEnabled
    public static CRM_CP_SubmitOrderResponse getCaseButtonVisibility(id idCase)
    {
    	list<user> lstProfile = [Select id, profile.Name from User Where Id = : UserInfo.getUserId()];
    	list<case> lstCase = [Select id, status from Case Where Id = : idCase];
    	CRM_CP_SubmitOrderResponse objResponse = new CRM_CP_SubmitOrderResponse();
    			
    	if(lstProfile != null && !lstProfile.isEmpty())
    	{
    		if(lstProfile[0].profile.Name == system.Label.CRM_CP_SalesProfile 
    		|| lstProfile[0].profile.Name == system.Label.CRM_CP_MarketingProfile
    		|| lstCase[0].status == system.Label.CRM_CP_ClosedDone)
    		{
    			objResponse.boolShowButton = false;
    			return objResponse;
    		}
    	}
    	return objResponse; 
    }
    /*
	 * getOrderButtonVisibility : This methods controls visisbility of Submit Order 
	 	button on screen.
	 * @param idCase : This is case Id which is required to be converted.
	 * @return CRM_CP_SubmitOrderResponse
	 * @throws DMLExceptions via CRM_CP_SubmitOrderResponse class
	 */	
    @AuraEnabled
    public static CRM_CP_SubmitOrderResponse getOrderButtonVisibility(id idOrder)
    {
    	list<user> lstProfile = [Select id, profile.Name from User Where Id = : UserInfo.getUserId()];
    	list<Order> lstOrder = [Select id, 
    						status,
    						CRM_CP_Product_Usage_Created__c,
    						account.recordtype.developerName,
    						(select id, orderId, pricebookEntryId from orderItems) 
    				from Order 
    				Where Id = : idOrder];
    	CRM_CP_SubmitOrderResponse objResponse = new CRM_CP_SubmitOrderResponse();
    	if(lstProfile != null 
    		&& !lstProfile.isEmpty()
    		&& lstOrder != null
    		&& !lstOrder.isEmpty())
    		
    	{
    		if(lstProfile[0].profile.Name == system.Label.CRM_CP_SalesProfile 
    		|| lstProfile[0].profile.Name == system.Label.CRM_CP_MarketingProfile
    		|| lstOrder[0].CRM_CP_Product_Usage_Created__c != false
    		|| lstOrder[0].account.recordtype.developerName != system.Label.CRM_CP_ConsumerRecType
    		)
    		{
    			objResponse.boolShowButton = false;
    			return objResponse;
    		}  		
    	}
    	return objResponse; 
    }    
}