/**
* @author   	: Preeti Dubey
* @date 		: 06 Sep 2018
* @description 	: This class is Test class for Product Trigger Handler class functionality.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Preeti Dubey - 09/06/2018
*/
@isTest
private class CRM_CP_ProductTriggerHandler_Test {
    
	private static testMethod void testProductTriggerHandler() {
	     //Creating test Product Data from Utility
	     Product2 prodTest = CRM_CP_TestDataUtility.createProduct('TestProd','1');
	     insert prodTest;
	     
	     //Updating the record to test update 
	     Product2 updateProd = [Select Id, Name from Product2 where Name='TestProd' LIMIT 1];
	     updateProd.CRM_CP_Hierarchy_Level__c = '2';
	     update updateProd;
	}

}