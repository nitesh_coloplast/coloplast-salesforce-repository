/**
* @author       : Nitesh Kumar
* @date         : 04/17/2018
* @description  : This is the LeadTriggerHandler class which will have all trigger event methods implmented and it is recommnended that all functionality
*                 of trigger should reside in this code of any utility method can be called from here.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Nitesh Kumar - Creted all the method as part of US-0693 and US-0556
* Version 1.1  : Nitesh Kumar - Removed the supress validation check from the IsDisabled() method.
* Version 1.2  : Anisa Shaikh - US-1350 Calling methods to sync proxy field information to child records
*/

public with sharing class CRM_CP_LeadTriggerHandler implements CRM_CP_TriggerInterface{
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will check whether the lead trigger is disaled or not. The configuration is stored in Trigger Setting custom metadata.
    */
    public Boolean IsDisabled(){
        return CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserId()).CRM_CP_Triggers_Disabled__c;
    }

    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records before inserting the data to the database(NOT commit)..
    * @param        : newItems - this will accepts the new reocords of account.
    */
    public void BeforeInsert(List<SObject> newItems) {
    
        List<sObject> preferredSpokenlist = new List<sObject>();
        List<sObject> preferredwrittenList = new List<sObject>();
        
        //Poulate the default spoken and written lanaguage
        
        for(sObject obj : newItems){
            
             if(Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Patient').getRecordTypeId() == obj.get('RecordTypeId')){
    
                 if(obj.get('CRM_CP_Preferred_Written_Language__c') == null){
                    preferredwrittenList.add(obj);
                 }
                 
                 if(obj.get('CRM_CP_PreferredSpoken_Lang__c') == null){
                    preferredSpokenlist.add(obj);
                 }
            }
        }
        
        //Poulate the default spoken lanaguage
         CRM_CP_TriggerHandlerUtilities.populateDefaultSpokenLanguage(preferredSpokenlist);
         
         //Poulate the default written lanaguage
         CRM_CP_TriggerHandlerUtilities.populateDefaultWrittenLanguage(preferredwrittenList);
    }
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records before updating the data to the database(NOT commit)..
    * @param        : newItems - this will accepts the new reocords of account.
    * @param        : oldItems - this will accepts the old reocords of account.
    */
      public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        
        
        List<sObject> preferredSpokenlist = new List<sObject>();
        List<sObject> preferredwrittenList = new List<sObject>();
        EDQ.DataQualityService.SetValidationStatus(newItems.values(), oldItems.values(), Trigger.IsInsert, 2); //Line required to populate Validation timestamp for QAS
        for(sObject lead : newItems.values()){
            if(Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Patient').getRecordTypeId() == newItems.get((String)lead.get('Id')).get('RecordTypeId')){
                if(newItems.get((String)lead.get('Id')).get('IndividualId') != oldItems.get((String)lead.get('Id')).get('IndividualId') 
                    && newItems.get((String)lead.get('Id')).get('IsConverted') == false 
                    && newItems.get((String)lead.get('Id')).get('IndividualId') == null){
                    lead.addError(Label.CRM_CP_Data_Privacy_Deletion_Message);
                }
                
                if(newItems.get((String)lead.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c') == null){
                    preferredSpokenlist.add(lead);
                }
                if(newItems.get((String)lead.get('Id')).get('CRM_CP_Preferred_Written_Language__c') == null){
                    preferredwrittenList.add(lead);
                }
                
                
                if(newItems.get((String)lead.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c') != oldItems.get((String)lead.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c')
                    && newItems.get((String)lead.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c') == 'French'
                    && newItems.get((String)lead.get('Id')).get('CRM_CP_Owner_Company_Number__c') == '760'){
                    
                    lead.put('CRM_CP_Preferred_Written_Language__c','fr-CA');
                 }
                 else if(newItems.get((String)lead.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c') != oldItems.get((String)lead.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c')
                    && newItems.get((String)lead.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c') != 'French'
                    && newItems.get((String)lead.get('Id')).get('CRM_CP_Owner_Company_Number__c') == '760'){
                    
                    lead.put('CRM_CP_Preferred_Written_Language__c','en-CA');
                 }
            } 
        }
        
        //Poulate the default spoken lanaguage
        CRM_CP_TriggerHandlerUtilities.populateDefaultSpokenLanguage(preferredSpokenlist);
     
        //Poulate the default spoken lanaguage
        CRM_CP_TriggerHandlerUtilities.populateDefaultWrittenLanguage(preferredwrittenList);
          
    }
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records before deleting the data from the database(NOT commit)..
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records after inseting the data from the database(NOT commit).
    * @param        : newItems - this will accepts the new reocords of account.
    */
    public void AfterInsert(Map<Id, SObject> newItems) {
        
        //US-1350 Recursion check
        Map<Id, SObject> executeForTheseRecords = CRM_CP_TriggerRecursionHandler.checkRecursionAfterInsert(newItems);
        if(executeForTheseRecords != null && !executeForTheseRecords.isEmpty()){
            //US-1350 Handle syncing of proxy field data to child records
            CRM_CP_LeadTriggerHelper helper = new CRM_CP_LeadTriggerHelper();
            boolean isSuccessful= helper.syncProxyDataOnInsert(executeForTheseRecords);                 
        }   
        //Call the generic mapping utility to map the Contact with the corresponding individuals
        CRM_CP_TriggerHandlerUtilities.mapIndividuals(newItems,null);        
    }
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records after updating the data from the database(NOT commit).
    * @param        : oldItems - this will accepts the old reocords of account.
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
              
        //US-1350 Recursion check
        Map<Id, SObject> executeForTheseRecords = CRM_CP_TriggerRecursionHandler.checkRecursionAfterUpdate(newItems);
        if(executeForTheseRecords != null && !executeForTheseRecords.isEmpty()){
            //US-1350 Handle syncing of proxy field data to child records
            CRM_CP_LeadTriggerHelper helper = new CRM_CP_LeadTriggerHelper();
            boolean isSuccessful= helper.syncProxyDataOnUpdate(executeForTheseRecords, oldItems);               
        }   
        
        if(CRM_CP_TriggerRecursionHandler.Lead_runOnce()){
            
            Map<Id, SObject> leadIds = new Map<Id, SObject> ();
            
            for(sObject lead : newItems.values()){
                if(Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Patient').getRecordTypeId() == newItems.get((String)lead.get('Id')).get('RecordTypeId')
                   &&(newItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_Consent__c') != oldItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_Consent__c')
                    || newItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_Letter__c') != oldItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_Letter__c')
                    || newItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_Phone__c') != oldItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_Phone__c')
                    || newItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_SMS__c') != oldItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_SMS__c')
                    || newItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_Latest_Consent_Date__c') != oldItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_Latest_Consent_Date__c')
                    || newItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_Promotional_Email_Permission__c') != oldItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_Promotional_Email_Permission__c')
                    || newItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_Informational_Email_Perm__c') != oldItems.get((String)lead.get('Id')).get('CRM_CP_ELQ_Informational_Email_Perm__c'))){
                
                    leadIds.put((String)lead.get('Id'),lead);
                }
            }
            
            system.debug('leadIds -> ' + leadIds.size() + ' ' + leadIds);
            
            if(!leadIds.isEmpty() && CRM_CP_TriggerRecursionHandler.dataPrivacyUpdates == false){
                CRM_CP_TriggerHandlerUtilities.updateDataPrivaciesLeadOrContact(leadIds, oldItems);
            }
        }         
    }
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records after deleting the data from the database(NOT commit).
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records after undeleting the data from the database(NOT commit).
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}