/**
* @author       : Nitesh Kumar
* @date         : 08/20/2018
* @description  : This class will handle recursions for triggers on different objects
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Nitesh Kumar - Original version
* Version 1.1  : Anisa Shaikh - US-1350 Stop recursion for proxy field data sync
*/
public class CRM_CP_TriggerRecursionHandler {
    
    //Account Trigger variable to handle the recursion�
    private static boolean run_Account = true;
    
    //Escape GDPR Rules
    public static boolean gdprRules = false;
    
    //Escape GDPR Rules
    public static boolean dataPrivacyUpdates = false;
    
    //Lead Trigger variable to handle the recursion
    private static boolean run_Lead = true;
    
    //Data Privacy Trigger variable to handle the recursion
    private static boolean run_DataPrivacy = true;
    
    //Contact Trigger variable to handle the recursion
    private static boolean run_Contact = true;
    
    //Campaign Trigger variable to handle the recursion
    private static boolean run_Campaign = true;
    
    //Health Basics Trigger variable to handle the recursion
    private static boolean run_HB = true;
    
    //Product Usage Trigger variable to handle the recursion
    private static boolean run_PU = true;
    
    //Health Check Trigger variable to handle the recursion
    private static boolean run_HC = true;
    
    //Campaign Trigger variable to handle the recursion
    private static boolean escapeDPValidation = true;
    
    //ELQ proxy Trigger variable to handle the recursion
    public static integer hbRecursionCounter = 0;
    
    //US-1350 - Added to stop recursion for proxy field data sync
    public static Set<Id> setOfIDsAfterInsert = new Set<Id>();

    //US-1350 - Added to stop recursion for proxy field data sync
    public static Set<Id> setOfIDsAfterUpdate = new Set<Id>();    
    
   
    /** 
    * @author       : Anisa Shaikh
    * @date         : 08/28/2018
    * @description  : This method will check the IDs of records for which trigger is
    *                 invoked against the record IDs for which trigger logic is already
    *                 executed and return list of IDs for which trigger needs to be
    *                 invoked
    * @param        : Map<Id, Sobject> affectedRecords
    * @returns      : Map<Id,Sobject>   
    */
    public static Map<Id,Sobject> checkRecursionAfterInsert(Map<Id,Sobject> affectedRecords){
        Map<Id,Sobject> executeForTheseRecords = new Map<Id,Sobject>();
        
        for(ID affectedId: affectedRecords.keySet()){
            if(!setOfIDsAfterInsert.contains(affectedId)){
                executeForTheseRecords.put(affectedId, affectedRecords.get(affectedId));
                setOfIDsAfterInsert.add(affectedId);
            }
        }   
        System.debug('executeForTheseRecords '+executeForTheseRecords); 
        return executeForTheseRecords;
    }
    
    /** 
    * @author       : Anisa Shaikh
    * @date         : 08/28/2018
    * @description  : This method will check the IDs of records for which trigger is
    *                 invoked against the record IDs for which trigger logic is already
    *                 executed and return list of IDs for which trigger needs to be
    *                 invoked
    * @param        : Map<Id, Sobject> affectedRecords
    * @returns      : Map<Id,Sobject>   
    */
    public static Map<Id,Sobject> checkRecursionAfterUpdate(Map<Id,Sobject> affectedRecords){
        Map<Id,Sobject> executeForTheseRecords = new Map<Id,Sobject>();
        
        for(ID affectedId: affectedRecords.keySet()){
            if(!setOfIDsAfterUpdate.contains(affectedId)){
                executeForTheseRecords.put(affectedId, affectedRecords.get(affectedId));
                setOfIDsAfterUpdate.add(affectedId);
            }
        }   
        System.debug('executeForTheseRecords after update'+executeForTheseRecords); 
        return executeForTheseRecords;
    }    
    
        
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This methond will stop the validation rule related to data privacy trigger if that is happening from Eloqua
    */
    public static boolean escapeValidationOn(){
        if(escapeDPValidation){
            escapeDPValidation = false;
            return true;
        }else{
            return escapeDPValidation;
        }
    }
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This methond will make sure that Account trigger is running only one time.
    */
    public static boolean Account_runOnce(){
        if(run_Account){
            run_Account = false;
            return true;
        }else{
            return run_Account;
        }
    }
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This methond will make sure that Lead trigger is running only one time.
    */
    public static boolean Lead_runOnce(){
        if(run_Lead){
            run_Lead = false;
            return true;
        }else{
            return run_Lead;
        }
    }
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This methond will make sure that Data Privacy trigger is running only one time.
    */
    public static boolean DataPrivacy_runOnce(){
        if(run_DataPrivacy){
            run_DataPrivacy = false;
            return true;
        }else{
            return run_DataPrivacy;
        }
    }
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This methond will make sure that Contact trigger is running only one time.
    */
    public static boolean Contact_runOnce(){
        if(run_Contact){
            run_Contact = false;
            return true;
        }else{
            return run_Contact;
        }
    }
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This methond will make sure that Contact trigger is running only one time.
    */
    public static boolean Campaign_runOnce(){
        if(run_Campaign){
            run_Campaign = false;
            return true;
        }else{
            return run_Campaign;
        }
    }
    
    /** 
    * @author       : Mruga Shastri 
    * @date         : 28-08-2018
    * @description  : This methond will make sure that HB trigger is running only one time.
    */
    public static boolean HB_runOnce(){
        if(run_HB){
            run_HB = false;
            return true;
        }else{
            return run_HB;
        }
    }
    
    /** 
    * @author       : Mruga Shastri 
    * @date         : 06-09-2018
    * @description  : This methond will make sure that PU trigger is running only one time.
    */
    public static boolean PU_runOnce(){
        if(run_PU){
            run_PU = false;
            return true;
        }else{
            return run_PU;
        }
    }
    
    /** 
    * @author       : Mruga Shastri 
    * @date         : 06-09-2018
    * @description  : This methond will make sure that PU trigger is running only one time.
    */
    public static boolean HC_runOnce(){
        if(run_HC){
            run_HC = false;
            return true;
        }else{
            return run_HC;
        }
    }
    
}