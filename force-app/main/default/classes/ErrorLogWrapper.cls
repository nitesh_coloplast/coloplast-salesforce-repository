/*********************************************************************************
* @author         Namrata Bansode
* @description    wrapper class for Error Log Design
* @date	     	  2018-02-20
* @group          Common libraries for error logging
**********************************************************************************/

public class ErrorLogWrapper {
    public Exception exceptionName;
    public string className;
    public string methodName;
    
    public ErrorLogWrapper(Exception exceptionName,string className,string methodName){
        this.exceptionName=exceptionName;
        this.className=className;
        this.methodName=methodName;
    }
}