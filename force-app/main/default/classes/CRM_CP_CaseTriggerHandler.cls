/**
* @author       : Ninad Patil
* @date         : 06 AUG 2018
* @description  : This is the CaseTriggerHandler class which will have all trigger event methods implmented and it is recommnended that all functionality
*                 of trigger should reside in this code of any utility method can be called from here.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Ninad Patil - US1386 : Logic for allowing only one Contact Protocol per consumer Account
*/
public with sharing class CRM_CP_CaseTriggerHandler implements CRM_CP_TriggerInterface
{
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
	public static Boolean TriggerDisabled = false;
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will check whether the account trigger is disaled or not. The configuration is stored in Trigger Setting custom metadata.
	*/
	public Boolean IsDisabled()
	{
		return CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserId()).CRM_CP_Triggers_Disabled__c;
    }
    
    /** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records before inserting the data to the database(NOT commit)..
	* @param        : newItems - this will accepts the new reocords of Case.
	*/
	public void BeforeInsert(List<SObject> newItems) 
	{
		List<Case> lstEligibleCase = new List<Case>();
		for(sObject objCase : newItems)
		{
	         if(Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get('CRM_CP_Call_Protocol').getRecordTypeId() == objCase.get('RecordTypeId'))
	         {
	         	lstEligibleCase.add((Case)objCase);	             
	         }	
	     }
	     handleContactProtocolDuplicate(lstEligibleCase);
	}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records before updating the data to the database(NOT commit)..
	* @param        : newItems - this will accepts the new reocords of Case.
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
		List<Case> lstEligibleCase = new List<Case>();
		EDQ.DataQualityService.SetValidationStatus(newItems.values(), oldItems.values(), Trigger.IsInsert, 2); //Line required to populate Validation timestamp for QAS
		for(sObject objCase : newItems.values())
		{
	         if(Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get('CRM_CP_Call_Protocol').getRecordTypeId() == objCase.get('RecordTypeId')
	         	&& newItems.get(objCase.Id).get('status') != oldItems.get(objCase.Id).get('status')
	         	&& objCase.get('status') != system.Label.CRM_CP_CaseStatusClosedCancelled 
				&& objCase.get('status') != system.Label.CRM_CP_ClosedDone)
	         {
	         	lstEligibleCase.add((Case)objCase);	             
	         }	
	     }
	     handleContactProtocolDuplicate(lstEligibleCase);
	}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records before deleting the data from the database(NOT commit)..
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	private void handleContactProtocolDuplicate(list<Case> lstCase)
	{
		if(lstCase != null 
			&& !lstCase.isEmpty())
		{
			map<Id, List<Case>> mapAccIdVsContactProtocols = CRM_CP_Case_OrderConversionUtil.checkExistsingContactProtocolForCases(lstCase);
			for(Case objCase : lstCase)
			{
				if(mapAccIdVsContactProtocols != null 
					&& mapAccIdVsContactProtocols.containskey(objCase.accountId)
					&& mapAccIdVsContactProtocols.get(objCase.accountId) != null
					&& !mapAccIdVsContactProtocols.get(objCase.accountId).isEmpty())
				{
					objCase.addError(system.Label.CRM_CP_ExistingContactProtocol);
				}
			}
		}
	}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records before deleting the data from the database(NOT commit)..
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	public void BeforeDelete(Map<Id, SObject> oldItems) {}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records after deleting the data from the database(NOT commit).
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	public void AfterDelete(Map<Id, SObject> oldItems) {}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records after undeleting the data from the database(NOT commit).
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	public void AfterUndelete(Map<Id, SObject> oldItems) {}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records after undeleting the data from the database(NOT commit).
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	public void AfterInsert(Map<Id, SObject> oldItems) {}
	
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method will process all the records after undeleting the data from the database(NOT commit).
	* @param        : oldItems - this will accepts the old reocords of Case.
	*/
	public void AfterUpdate(Map<Id, SObject> newItems,Map<Id, SObject> oldItems) {}
	
	
}