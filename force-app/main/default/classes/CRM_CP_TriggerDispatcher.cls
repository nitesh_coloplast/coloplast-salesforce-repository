/**
* @author   	: Nitesh Kumar
* @date 		: 04/17/2018
* @description 	: This class will dispacth the tigger execution flow to the required method of the trigger handler.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0   : Nitesh Kumar - Creted all the method as part of US-0693 and US-0556
*/
public class CRM_CP_TriggerDispatcher {
    
    /** 
	* @author 		: Nitesh Kumar
	* @date 		: 04/17/2018
	* @description 	: Call this method from your trigger, passing in an instance of a trigger handler which implements CRM_CP_TriggerHandler.
    *				  This method will fire the appropriate methods on the handler depending on the trigger context.
	* @param 		: handler - This will accepts the instance of the trigger handler class.
	*/
    public static void Run(CRM_CP_TriggerInterface handler){
        
        // Check to see if the trigger has been disabled. If it has, return
        if (handler.IsDisabled()){
            return;
        }    

        //Detect the current trigger context and fire the relevant methods on the trigger handler:
        //Before trigger logic
        if (Trigger.IsBefore){
            
            if (Trigger.IsInsert){
            	//Call the BeforeInsert method of the handler if the trigger event is isInsert and IsBefore.
                handler.BeforeInsert(trigger.new);
            }
            
            if (Trigger.IsUpdate){
            	//Call the BeforeUpdate method of the handler if the trigger event is isUpdate and IsBefore.
                handler.BeforeUpdate(trigger.newMap, trigger.oldMap);
            }
            
            if (Trigger.IsDelete){
            	//Call the BeforeDelete method of the handler if the trigger event is IsDelete and IsBefore.
                handler.BeforeDelete(trigger.oldMap);
            }
        }
        
        // After trigger logic
        if (Trigger.IsAfter){
            
            if (Trigger.IsInsert){
            	//Call the AfterInsert method of the handler if the trigger event is IsAfter and IsInsert.
                handler.AfterInsert(Trigger.newMap);
            }
            
            if (Trigger.IsUpdate){
            	//Call the AfterInsert method of the handler if the trigger event is IsAfter and IsInsert.
                handler.AfterUpdate(trigger.newMap, trigger.oldMap);
            }
            
            if (trigger.IsDelete){
            	//Call the AfterDelete method of the handler if the trigger event is IsAfter and IsDelete.
                handler.AfterDelete(trigger.oldMap);
            }
            
            if (trigger.isUndelete){
            	//Call the AfterUndelete method of the handler if the trigger event is IsAfter and isUndelete.
                handler.AfterUndelete(trigger.oldMap);
            }
        }
    }
}