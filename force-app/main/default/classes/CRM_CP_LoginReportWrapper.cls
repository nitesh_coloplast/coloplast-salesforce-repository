/**
* @author   	 : Anisa Shaikh
* @date 		 : 09/20/2018
* @description 	 : This class is wrapper for the delegated login report US-1838
* ------------------------------------------ Version Control ----------------------------------------------------------
* Version 1.0    : Anisa Shaikh - Original (US-1838)
*/

public class CRM_CP_LoginReportWrapper {
    
    @AuraEnabled
    public String strDelegatedUserName{get;set;}
    @AuraEnabled
    public String strDelegatedUserOCN{get;set;}
    @AuraEnabled
    public String strLoggedInAsUserName{get;set;}
    @AuraEnabled
    public String strLoggedInAsUserOCN{get;set;}
    @AuraEnabled
    public DateTime dtTimeStamp{get;set;}
    
    
    public CRM_CP_LoginReportWrapper(SetupAuditTrail auditTrailRecord){
    	
    	this.strDelegatedUserName = auditTrailRecord.DelegateUser;
    	this.strDelegatedUserOCN = auditTrailRecord.DelegateUser;
    	this.strLoggedInAsUserName = auditTrailRecord.CreatedBy.UserName;
    	this.strLoggedInAsUserOCN = auditTrailRecord.CreatedBy.CRM_CP_Subsidiary_Number__C;
    	this.dtTimeStamp = auditTrailRecord.CreatedDate;
    }
    
}