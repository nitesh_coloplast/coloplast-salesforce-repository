public without sharing class CRM_CP_ChangeOwnerController {
    
    @AuraEnabled
    public static List<User> searchUsers(String searchText){
        string tempInput = '%' + searchText + '%';
        return [Select Id, Email, Name, Profile.Name from User WHERE Name LIKE :tempInput and isActive=true];
    }
    
    @AuraEnabled
    public static Case changeOwner(String caseId, String userId, String email, Boolean isSendEmailTrue){
        Case objCase = [Select Id, CaseNumber from Case where Id = :caseId];
            
        objCase.OwnerId = userId;
        update objCase;
        
        if(isSendEmailTrue == true){
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { email };
            message.subject = Label.CRM_CP_Owner_change;
            message.plainTextBody = Label.CRM_CP_This_is_to_notify_that_the_case + ' '+ objCase.CaseNumber +' '+ Label.CRM_CP_has_been_assigned_to_you;
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            if (results[0].success) {
                System.debug('The email was sent successfully.');
            } else {
                System.debug('The email failed to send: ' + results[0].errors[0].message);
            }
        }
        
        return objCase;
    }
}