/*
 * CRM_CP_SystemModeQueryHelper : This method Queries Contact Protocols if the account is Consumer.
 * Created Date : 09-August-2018
 Modification Log
 --------------------------------------------------------------------
 DeveloperName       Date			User Story     Description
 --------------------------------------------------------------------
 Ninad				09-August-2018	US1386		This method Queries Contact Protocols if the account is Consumer. 																		 
 */
public without sharing class CRM_CP_SystemModeQueryHelper 
{
	/*
     * checkDuplicateContactProtocol : This method Queries Contact Protocols if the account is Consumer.
     * @param map<Id, List<Case>> : This map holds Account Id vs list of contact protocol cases under it.  
     * @return map<Id, List<Case>> : This map holds Account Id vs list of contact protocol cases under it.
     * @throws : None
     */
	public static map<Id, List<Case>> checkDuplicateContactProtocol(set<Id> setCaseIds, map<Id, List<Case>> mapAccIdVsContactProtocols)
	{
		//for Accounts in the above Keyset, fetch all contactProtocol Cases existing in the system.
		if(mapAccIdVsContactProtocols != null
			&& !mapAccIdVsContactProtocols.keyset().isEmpty())
		{
			List<case> lstDupCase = new List<Case>();
			for(Account objAcc : [Select id, 
									name, 
									(select id, 
										recordtypeId 
										from cases 
										where recordtype.DeveloperName = :system.Label.CRM_CP_ContactProtocol
										AND status 
										NOT IN (:system.Label.CRM_CP_CaseStatusClosedCancelled, 
										:system.Label.CRM_CP_ClosedDone)
									) 
									FROM Account 
									WHERE recordtype.developername = :system.Label.CRM_CP_ConsumerRecType
									AND Id IN :mapAccIdVsContactProtocols.keyset()])
			{
				if(setCaseIds != null && !setCaseIds.isEmpty())
				{
					if(objAcc.cases != null && !objAcc.cases.isEmpty())
					{
						for(Case objCase: objAcc.cases)
						{
							if(!setCaseIds.contains(objCase.Id))
							{
								lstDupCase.add(objCase);
							}
						}
					}
					if(lstDupCase != null && !lstDupCase.isEmpty()) mapAccIdVsContactProtocols.put(objAcc.Id, lstDupCase);
				}
				else mapAccIdVsContactProtocols.put(objAcc.Id, objAcc.cases);
			}
		}
		return mapAccIdVsContactProtocols;
	}    
}