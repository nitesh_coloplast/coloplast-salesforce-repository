/**
* @author   	: Nitesh Kumar
* @date 		: 04/17/2018
* @description 	: This is the ContactTriggerHandler class which will have all trigger event methods implmented and it is recommnended that all functionality
*				  of trigger should reside in this code of any utility method can be called from here.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Nitesh Kumar - Creted all the method as part of US-0693 and US-0556
* Version 1.1  : Nitesh Kumar - Removed the supress validation check from the IsDisabled() method.
*/
public with sharing class CRM_CP_ContactTriggerHandler implements CRM_CP_TriggerInterface{
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /** 
	* @author 		: Nitesh Kumar
	* @date 		: 04/17/2018
	* @description 	: This method will check whether the Contact trigger is disaled or not. The configuration is stored in Trigger Setting custom metadata.
	*/
    public Boolean IsDisabled(){
		return CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserId()).CRM_CP_Triggers_Disabled__c;
    }
    /** 
	* @author 		: Nitesh Kumar
	* @date 		: 04/17/2018
	* @description 	: This method will process all the records before inserting the data to the database(NOT commit)..
	* @param 		: newItems - this will accepts the new reocords of account.
	*/
    public void BeforeInsert(List<SObject> newItems) {}
    
    /** 
	* @author 		: Nitesh Kumar
	* @date 		: 04/17/2018
	* @description 	: This method will process all the records before updating the data to the database(NOT commit)..
	* @param 		: newItems - this will accepts the new reocords of account.
	* @param 		: oldItems - this will accepts the old reocords of account.
	*/
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
		EDQ.DataQualityService.SetValidationStatus(newItems.values(), oldItems.values(), Trigger.IsInsert, 2); //Line required to populate Validation timestamp for QAS
    	for(sObject con : newItems.values()){ 
    		if(Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('CRM_CP_Professional_Contact') != null){
	    		if(newItems.get((String)con.get('Id')).get('IndividualId') != oldItems.get((String)con.get('Id')).get('IndividualId')
	    			&& newItems.get((String)con.get('Id')).get('IndividualId') == null
	    			&& Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('CRM_CP_Professional_Contact').getRecordTypeId() == newItems.get((String)con.get('Id')).get('RecordTypeId')){
	    			
	    			con.addError(Label.CRM_CP_Data_Privacy_Deletion_Message);
	    		}
			}
    	}
    }
    
    /** 
	* @author 		: Nitesh Kumar
	* @date 		: 04/17/2018
	* @description 	: This method will process all the records before deleting the data from the database(NOT commit)..
	* @param 		: oldItems - this will accepts the old reocords of account.
	*/
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    
    /** 
	* @author 		: Nitesh Kumar
	* @date 		: 04/17/2018
	* @description 	: This method will process all the records after inseting the data from the database(NOT commit).
	* @param 		: newItems - this will accepts the new reocords of account.
	*/
    public void AfterInsert(Map<Id, SObject> newItems) {        	
        //Call the generic mapping utility to map the Contact with the corresponding individuals
        
        Map<Id, SObject> finalNewItems = new Map<Id, SObject>();
        
        for(SObject objLoop : newItems.values()){
        	if(objLoop.get('CRM_CP_Is_Converted_from_Lead__c') == false){
        		finalNewItems.put((Id)objLoop.get('Id'),objLoop);
        	}	
        }
		if(!finalNewItems.isEmpty()) {      
        	CRM_CP_TriggerHandlerUtilities.mapIndividuals(newItems,null);
		}
    }
    
    /** 
	* @author 		: Nitesh Kumar
	* @date 		: 04/17/2018
	* @description 	: This method will process all the records after updating the data from the database(NOT commit).
	* @param 		: oldItems - this will accepts the old reocords of account.
	* @param 		: oldItems - this will accepts the old reocords of account.
	*/
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        if(CRM_CP_TriggerRecursionHandler.Contact_runOnce()){
            //Write your update logic here, make sure you nothing is outside this if condition
        }
        
    }
    
    /** 
	* @author 		: Nitesh Kumar
	* @date 		: 04/17/2018
	* @description 	: This method will process all the records after deleting the data from the database(NOT commit).
	* @param 		: oldItems - this will accepts the old reocords of account.
	*/
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    
    /** 
	* @author 		: Nitesh Kumar
	* @date 		: 04/17/2018
	* @description 	: This method will process all the records after undeleting the data from the database(NOT commit).
	* @param 		: oldItems - this will accepts the old reocords of account.
	*/
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}