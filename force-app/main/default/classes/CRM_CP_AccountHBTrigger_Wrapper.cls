/**
* @author       : Mruga Shastri
* @date         : 01-06-2018
* @description  : This is the class is Wrapper for Account Proxy fields to Child Health Basic Sync. 
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Mruga Shastri - Creted all the methods as part of US 0726, 0826. 
* Version 1.1  : Mruga Shastri - Added one more constructor and introduced two Health Basics lists for US 1349
*/
public class CRM_CP_AccountHBTrigger_Wrapper{
    
    
        Public Account personAccount;
        Public Lead patient;
        Public Boolean isColostomyChanged;
        Public Boolean isContinentUrostomyChanged;
        Public Boolean isIleostomyChanged;
        Public Boolean isJejunostomyChanged;
        Public Boolean isNephrostomyChanged;
        Public Boolean isUrostomyChanged; 
        Public Boolean isSurgeryDateChanged;
        Public Boolean isStomaStatusChanged;
        Public Boolean isWheelChairChanged;
        
        Public Boolean hasColostomy;
        Public Boolean hasContinentUrostomy;
        Public Boolean hasIleostomy;
        Public Boolean hasJejunostomy;
        Public Boolean hasNephrostomy;
        Public Boolean hasUrostomy;
        Public Boolean hasSurgeryDate;
        Public Boolean hasStomaStatus;
        Public Boolean hasWheelChair;
        Public Boolean runHealthBasicsCreateLogic; 
        Public Boolean runHealthBasicsUpdateLogic; 
        Public Integer stomaCount; 
        
        Public List<CRM_CP_Health_Basics__c> lstRelatedOutcomes; 
        Public List<CRM_CP_Health_Basics__c> lstOpenOutcomes;
        Public List<CRM_CP_Health_Basics__c> lstRelatedPatientInfo;
        
        Public CRM_CP_AccountHBTrigger_Wrapper(Account personAccount, Boolean isColostomyChanged, Boolean isContinentUrostomyChanged, Boolean isIleostomyChanged, Boolean isJejunostomyChanged, Boolean isNephrostomyChanged, Boolean isUrostomyChanged, Boolean isSurgeryDateChanged, Boolean isStomaStatusChanged, Boolean isWheelChairChanged){
            this.personAccount = personAccount;
            this.isColostomyChanged = isColostomyChanged;
            this.isContinentUrostomyChanged = isContinentUrostomyChanged; 
            this.isIleostomyChanged = isIleostomyChanged;
            this.isJejunostomyChanged = isJejunostomyChanged;
            this.isNephrostomyChanged = isNephrostomyChanged;
            this.isUrostomyChanged = isUrostomyChanged;
            this.isSurgeryDateChanged = isSurgeryDateChanged;
            this.isStomaStatusChanged = isStomaStatusChanged;
            this.isWheelChairChanged = isWheelChairChanged;
            this.hasColostomy = false; 
            this.hasContinentUrostomy = false;
            this.hasIleostomy = false;
            this.hasJejunostomy = false;
            this.hasNephrostomy = false;
            this.hasUrostomy = false;
            this.hasWheelChair = false; 
            this.hasSurgeryDate = false; 
            this.hasStomaStatus = false; 
            this.stomaCount = 0;
            this.lstRelatedOutcomes = new List<CRM_CP_Health_Basics__c>();
            this.lstRelatedPatientInfo = new List<CRM_CP_Health_Basics__c>();
            this.lstOpenOutcomes = new List<CRM_CP_Health_Basics__c>(); 
        }
        
        Public CRM_CP_AccountHBTrigger_Wrapper(Account personAccount){
            this.personAccount = personAccount;
            this.isColostomyChanged = false;
            this.isContinentUrostomyChanged = false; 
            this.isIleostomyChanged = false;
            this.isJejunostomyChanged = false;
            this.isNephrostomyChanged = false;
            this.isUrostomyChanged = false;
            this.isSurgeryDateChanged = false;
            this.isStomaStatusChanged = false;
            this.isWheelChairChanged = false;
            this.hasColostomy = false; 
            this.hasContinentUrostomy = false;
            this.hasIleostomy = false;
            this.hasJejunostomy = false;
            this.hasNephrostomy = false;
            this.hasUrostomy = false;
            this.hasWheelChair = false; 
            this.hasSurgeryDate = false; 
            this.hasStomaStatus = false; 
            this.stomaCount = 0;
            this.lstRelatedOutcomes = new List<CRM_CP_Health_Basics__c>();
            this.lstRelatedPatientInfo = new List<CRM_CP_Health_Basics__c>();
            this.lstOpenOutcomes = new List<CRM_CP_Health_Basics__c>(); 
        }
        Public CRM_CP_AccountHBTrigger_Wrapper(){
            
        }
        Public static Boolean returnHasCheck(CRM_CP_AccountHBTrigger_Wrapper accountHBwrapper, string fieldAPI){
            Boolean hasCheckFlag = False; 
            if(fieldAPI == 'Colostomy' && accountHBwrapper.hasColostomy){
                hasCheckFlag = TRUE;
            }else if(fieldAPI == 'Continent Urostomy (Mitrofanoff)' && accountHBwrapper.hasContinentUrostomy){
                hasCheckFlag = TRUE;
            }else if(fieldAPI == 'Ileostomy' && accountHBwrapper.hasIleostomy){
                hasCheckFlag = TRUE;
            }else if(fieldAPI == 'Jejunostomy' && accountHBwrapper.hasJejunostomy){
                    hasCheckFlag = TRUE;
            }else if(fieldAPI == 'Nephrostomy' && accountHBwrapper.hasNephrostomy){
                    hasCheckFlag = TRUE;
            }else if(fieldAPI == 'Urostomy' && accountHBwrapper.hasUrostomy){
                    hasCheckFlag = TRUE;
            } else if(fieldAPI == 'CRM_CP_ELQ_Wheelchair__pc' && accountHBwrapper.hasWheelChair){
                    hasCheckFlag = TRUE;
            }

            return hasCheckFlag; 
        }
}