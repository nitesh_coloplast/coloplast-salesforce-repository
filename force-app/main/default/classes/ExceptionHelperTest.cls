/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
 /*********************************************************************************
* @author         Anisa Shaikh
* @description    Test class for ExceptionHelper class
* @date	     	  2018-02-26
* @group          Test Classes
**********************************************************************************/

@isTest
private class ExceptionHelperTest {

/*****************************************************************************
* @author		Anisa Shaikh
* @date			2018-02-27
* @description  Null check on exception object in the logErrors method
*******************************************************************************/   

    static testMethod void nullCheckOnExceptionObj() {
        
        List < CRM_CP_Error_Log__c > lstErrors = new List < CRM_CP_Error_Log__c >();
        //lstErrors = ExceptionHelper.logErrors(null, 'testClass', 'testMethod');
        										 
        //System.assertEquals(lstErrors.size(),0);
        
    }
    
    
/*****************************************************************************
* @author		Anisa Shaikh
* @date			2018-02-27
* @description  Creates error records
*******************************************************************************/   

    static testMethod void createsError() {
        
        List < CRM_CP_Error_Log__c > lstErrors = new List < CRM_CP_Error_Log__c >();
        //Below code will cause Nullpointer exception
        try{
        	String a;
        	integer b = a.length();
        }
        catch(Exception e){
        	lstErrors = ExceptionHelper.logErrors(e, 'testClass', 'testMethod');
        }                										
        System.assertEquals(lstErrors.size(),1);
        
    }    
}