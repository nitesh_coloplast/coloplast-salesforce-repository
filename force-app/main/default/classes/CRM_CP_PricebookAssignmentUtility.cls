/*
 * CRM_CP_PricebookAssignmentUtility : This class contains logic to read Order/Opportunity types
 	and populate PricebookId onit.
 *
 * Created Date : 03-Sep-2018 
 * 
 Modification Log
 --------------------------------------------------------------------
 DeveloperName       Date           User Story		Description
 --------------------------------------------------------------------
 Ninad              03-Sep-2018     US1667         This class contains logic to 
												 	read Order/Opportunity types
												 	and populate PricebookId onit.                                               
 */
public without sharing class CRM_CP_PricebookAssignmentUtility 
{
	/*
     * validateUserCreds : This method accepts User objects 
     * 					   checks for edit rights else Error
     * @param List<sObject> newItems :  New instance of User Objct
	 * @param map<id, sObject> : Old map in trigger context
	 * @param map<Id, sObject> mapOldValues : New map in trigger context
     * @return Void
     * @throws Error : Using AddError() method of sObjects.
     */ 
	public static void assignPricebook(List<sObject> newItems)
	{
		if(newItems != null 
			&& !newItems.isEmpty())
		{
			map<string, Pricebook2> mapPricebookMeta = new map<String, Pricebook2>();
			map<String, List<sObject>> mapPricebookSobject = new map<String,List<sObject>>();
			
			for(sObject objSobj : newItems)
			{
				String strKey = getMapKey(objSobj);
				
				mapPricebookMeta.put(strKey , NUll);
				list<sObject> lstsObj;
				
				if(mapPricebookSobject.containsKey(strKey)
					&& mapPricebookSobject.get(strKey) != Null) lstsObj = mapPricebookSobject.get(strKey);
				else lstsObj = new List<sObject>();
				
				lstsObj.add(objSobj);
				mapPricebookSobject.put(strKey, lstsObj);
			}
			if(mapPricebookMeta != null && !mapPricebookMeta.keyset().isEmpty())
			{
				set<string> setPricebookType = new set<string>();
				set<string> setPricebookOCN = new set<string>();
				for(String strKey : mapPricebookMeta.keyset())
				{
					if(strKey.contains(':'))
					{
						if(!String.isBlank(strKey.split(':')[0])) setPricebookType.add(strKey.split(':')[0]);
						if(!String.isBlank(strKey.split(':')[1])) setPricebookOCN.add(strKey.split(':')[1]);
					}					
				}
				List<Pricebook2> lstPricebook = [Select 
												id, 
												name,
												CRM_CP_Pricebook_Type__c,
												CRM_CP_Owner_Company_Number__c 
												from Pricebook2
												Where CRM_CP_Owner_Company_Number__C IN : setPricebookOCN
												AND CRM_CP_Pricebook_Type__c IN : setPricebookType
												AND isActive = true];
				if(lstPricebook != null && !lstPricebook.isEmpty())
				{
					for(Pricebook2  objPB : lstPricebook)
					{
						string strKey = getMapKey(objPB);
						
						mapPricebookMeta.put(strKey, objPB);
					}
				}
				if(mapPricebookMeta != null && !mapPricebookMeta.keyset().isEmpty())
				{
					for(String strKey : mapPricebookMeta.keyset())
					{
						if(mapPricebookSobject.containskey(strKey)  
							&& mapPricebookSobject.get(strKey) != null
							&& !mapPricebookSobject.get(strKey).isEmpty())
						{
							for(sObject sObj : mapPricebookSobject.get(strKey))
							{
								if(mapPricebookMeta != null
									&& mapPricebookMeta.containskey(strKey) 
									&& mapPricebookMeta.get(strKey) != null) 
								sObj.put('pricebook2Id', mapPricebookMeta.get(strKey).Id);
							}
						}
					}
				}				
			}			
		}
	}  
	
	/*
     * getMapKey : This method checks permission set assignment
     * @param : None
     * @return : Boolean
     * @throws : Nonad
     */ 
	private static String getMapKey(sObject objSobj)
	{
		if(String.valueOf(objSobj.getSObjectType()) == 'Opportunity')
			return 'Opportunity' + ':' + objSobj.get('CRM_CP_Owner_Company_Number__c');
		
		if(String.valueOf(objSobj.getSObjectType()) == 'Order')
		{
			if(objSobj.get('Type') == system.Label.CRM_CP_OrderTypeSalesOrder 
				|| objSobj.get('Type') == system.Label.CRM_CP_OrderTypeReturnOrder 
				|| objSobj.get('Type') == system.Label.CRM_CP_OrderTypeReplacementOrder) 
				return system.Label.CRM_CP_PBType_ConsumerSalesOrder + ':' + objSobj.get('CRM_CP_Subsidiary_Number__c');
			
			if(objSobj.get('Type') == system.Label.CRM_CP_OrderTypeSampleOrder) 
				return system.Label.CRM_CP_PBType_SampleOrder + ':' + objSobj.get('CRM_CP_Subsidiary_Number__c');
		}
		if(String.valueOf(objSobj.getSObjectType()) == 'Pricebook2')
		{
			return objSobj.get('CRM_CP_Pricebook_Type__c') + ':' + objSobj.get('CRM_CP_Owner_Company_Number__c');
		}
		return null;  								
	}     
}