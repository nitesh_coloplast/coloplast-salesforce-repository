/**
* @author       : Nitesh Kumar
* @date         : 04/17/2018
* @description  : This is the AccountTriggerHandler class which will have all trigger event methods implmented and it is recommnended that all functionality
*                 of trigger should reside in this code of any utility method can be called from here.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Nitesh Kumar - Creted all the method as part of US-0693 and US-0556
* Version 1.1  : Nitesh Kumar - Removed the supress validation check from the IsDisabled() method.
*/
public class CRM_CP_DataPrivacyTriggerHandler implements CRM_CP_TriggerInterface{
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will check whether the Data Privacy trigger is disaled or not. The configuration is stored in Trigger Setting custom metadata.
    */
    public Boolean IsDisabled(){
        return CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserId()).CRM_CP_Triggers_Disabled__c;
    }
    
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records before inserting the data to the database(NOT commit)..
    * @param        : newItems - this will accepts the new reocords of account.
    */
    public void BeforeInsert(List<SObject> newItems) {}
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records before updating the data to the database(NOT commit)..
    * @param        : newItems - this will accepts the new reocords of account.
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    
        
        for(Individual ind : (List<Individual>) newItems.values()){
            //Update the Email permissions only if DOI Trigger is off
            if(!CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserId()).CRM_CP_Disable_DOI_Trigger__c && ind.CRM_CP_Owner_Company_Number__c != null && CRM_CP_Global_Settings__c.getOrgDefaults().CRM_CP_Double_Opt_In_Countries__c !=  null && CRM_CP_Global_Settings__c.getOrgDefaults().CRM_CP_Double_Opt_In_Countries__c.contains(ind.CRM_CP_Owner_Company_Number__c)){
                if(ind.CRM_CP_Promotional_Email_Permission__c == 'No' && newItems.get(ind.Id).get('CRM_CP_Promotional_Email_Permission__c') != oldItems.get(ind.Id).get('CRM_CP_Promotional_Email_Permission__c')){
                    ind.CRM_CP_Informational_Email_permission__c = 'No';
                }
                else if(ind.CRM_CP_Informational_Email_permission__c == 'No' && newItems.get(ind.Id).get('CRM_CP_Informational_Email_permission__c') != oldItems.get(ind.Id).get('CRM_CP_Informational_Email_permission__c')){
                    ind.CRM_CP_Promotional_Email_Permission__c = 'No';
                }
            }
        }
    
    }
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records before deleting the data from the database(NOT commit)..
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records after inseting the data from the database(NOT commit).
    * @param        : newItems - this will accepts the new reocords of account.
    */
    public void AfterInsert(Map<Id, SObject> newItems) {}
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records after updating the data from the database(NOT commit).
    * @param        : oldItems - this will accepts the old reocords of account.
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        
        Set<Id> newItemsToUpdateIds = new Set<Id>();
        Set<Id> newItemsToUpdateIds1 = new Set<Id>();
        
        if(CRM_CP_TriggerRecursionHandler.DataPrivacy_runOnce()){
            for(sObject obj : newItems.values()){
                if(CRM_CP_TriggerRecursionHandler.escapeValidationOn()){
                    if(newItems.get(obj.Id).get('CRM_CP_Consent_Type__c') == oldItems.get(obj.Id).get('CRM_CP_Consent_Type__c')
                       && newItems.get(obj.Id).get('CRM_CP_Latest_Consent_Date__c') == oldItems.get(obj.Id).get('CRM_CP_Latest_Consent_Date__c')
                       && newItems.get(obj.Id).get('CRM_CP_Latest_Consent_Channel__c') == oldItems.get(obj.Id).get('CRM_CP_Latest_Consent_Channel__c')
                       && (((newItems.get(obj.Id).get('CRM_CP_Promotional_Email_Permission__c') == 'Yes - to be confirmed' 
                       || newItems.get(obj.Id).get('CRM_CP_Promotional_Email_Permission__c') == Label.CRM_CP_Yes) && oldItems.get(obj.Id).get('CRM_CP_Promotional_Email_Permission__c') == Label.CRM_CP_No)
                           || (newItems.get(obj.Id).get('CRM_CP_Phone__c') == Label.CRM_CP_Yes && oldItems.get(obj.Id).get('CRM_CP_Phone__c') == Label.CRM_CP_No)
                           || (newItems.get(obj.Id).get('CRM_CP_SMS__c') == Label.CRM_CP_Yes && oldItems.get(obj.Id).get('CRM_CP_SMS__c') == Label.CRM_CP_No)
                           || (newItems.get(obj.Id).get('CRM_CP_Letter__c') == Label.CRM_CP_Yes && oldItems.get(obj.Id).get('CRM_CP_Letter__c') == Label.CRM_CP_No)
                           || ((newItems.get(obj.Id).get('CRM_CP_Informational_Email_Permission__c') == 'Yes - to be confirmed' || newItems.get(obj.Id).get('CRM_CP_Informational_Email_Permission__c') == Label.CRM_CP_Yes) && oldItems.get(obj.Id).get('CRM_CP_Informational_Email_Permission__c') == Label.CRM_CP_No)
                           || (newItems.get(obj.Id).get('CRM_CP_Fax__c') == Label.CRM_CP_Yes && oldItems.get(obj.Id).get('CRM_CP_Fax__c') == Label.CRM_CP_No))){
                         
                           obj.addError(Label.CRM_CP_Marketing_Permission_Validation_Message);
                     }
                }
                
                if(newItems.get(obj.Id).get('CRM_CP_Promotional_Email_Permission__c') != oldItems.get(obj.Id).get('CRM_CP_Promotional_Email_Permission__c')
                    || newItems.get(obj.Id).get('CRM_CP_Phone__c') != oldItems.get(obj.Id).get('CRM_CP_Phone__c')
                    || newItems.get(obj.Id).get('CRM_CP_SMS__c') != oldItems.get(obj.Id).get('CRM_CP_SMS__c')
                    || newItems.get(obj.Id).get('CRM_CP_Letter__c') != oldItems.get(obj.Id).get('CRM_CP_Letter__c')
                    || newItems.get(obj.Id).get('CRM_CP_Informational_Email_Permission__c') != oldItems.get(obj.Id).get('CRM_CP_Informational_Email_Permission__c')){
                    
                    newItemsToUpdateIds1.add(obj.Id); 
                }
                
                if((newItems.get(obj.Id).get('CRM_CP_Consent_Type__c') != oldItems.get(obj.Id).get('CRM_CP_Consent_Type__c'))
                   || (newItems.get(obj.Id).get('CRM_CP_Latest_Consent_Date__c') != oldItems.get(obj.Id).get('CRM_CP_Latest_Consent_Date__c'))
                   || (newItems.get(obj.Id).get('CRM_CP_Latest_Consent_Channel__c') != oldItems.get(obj.Id).get('CRM_CP_Latest_Consent_Channel__c'))){

                       newItemsToUpdateIds.add(obj.Id);
                 }                
            }
           
            if(!newItemsToUpdateIds.isEmpty()){
                CRM_CP_TriggerHandlerUtilities.populateConsentInformation(newItemsToUpdateIds,oldItems,[SELECT DeveloperName, CRM_CP_Country__c, 
                                                                                                        MasterLabel, CRM_CP_Consent_Type__c, 
                                                                                                        CRM_CP_Consent_Expiry_Time__c, 
                                                                                                        CRM_CP_Information_Email_Permission__c,
                                                                                                        CRM_CP_Latest_Consent_Channel__c, 
                                                                                                        CRM_CP_Letter_Setting__c, 
                                                                                                        CRM_CP_Phone_Setting__c, 
                                                                                                        CRM_CP_Promotional_Email_Permission__c, 
                                                                                                        CRM_CP_SMS_Setting__c, CRM_CP_Fax_Setting__c, 
                                                                                                        CRM_CP_Contact_Type__c 
                                                                                                        FROM CRM_CP_Marketing_Permission_Manager__mdt]);
            }
            if(!newItemsToUpdateIds1.isEmpty()){
                CRM_CP_TriggerHandlerUtilities.populateAccountConsentInformation(newItemsToUpdateIds1);
            }
        }
    }
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records after deleting the data from the database(NOT commit).
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    
     /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records after undeleting the data from the database(NOT commit).
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}