global class CRM_CP_SuppressProcess{
    /**
    * This method is used in process builders as a dummy action to Suppress Process 
    * @param : none
    */
    @InvocableMethod(label='Suppress Process' description='This method is used in process builders as a dummy action to Suppress Process')
    public static void dummyUpdate() {
        // do nothing
    }
}