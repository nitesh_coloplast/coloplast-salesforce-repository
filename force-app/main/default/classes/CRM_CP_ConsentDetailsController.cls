public with sharing class CRM_CP_ConsentDetailsController {
    
    @AuraEnabled
    public static Individual getIndividualRecordId(Id recordId){
        
        String objectName = recordId.getSObjectType().getDescribe().getName();
        
        if(objectName == Label.CRM_CP_AccountLabel){
            List<Account> lstData = [SELECT PersonIndividualId from Account where Id = :recordId];
            if(!lstData.isEmpty() && lstData[0].PersonIndividualId != null){
                return [SELECT Id,toLabel(CRM_CP_Promotional_Email_Permission__c),
                        toLabel(CRM_CP_Informational_Email_permission__c),
                        toLabel(CRM_CP_Phone__c),
                        toLabel(CRM_CP_Letter__c),
                        toLabel(CRM_CP_SMS__c),
                        toLabel(CRM_CP_Consent_Type__c),
                        toLabel(CRM_CP_Latest_Consent_Channel__c),
                        CRM_CP_Consent_Expiry_Date__c,
                        CRM_CP_Consent_Version__c,
                        toLabel(CRM_CP_Fax__c),
                        CRM_CP_Latest_Consent_Date__c from Individual where Id = :lstData[0].PersonIndividualId];
            }
            else{
                return null;
            }
        }
        else if(objectName == Label.CRM_CP_Contact){
            List<Contact> lstData = [SELECT IndividualId from Contact where Id = :recordId];
            if(!lstData.isEmpty() && lstData[0].IndividualId != null){
                return [SELECT Id,toLabel(CRM_CP_Promotional_Email_Permission__c),
                        toLabel(CRM_CP_Informational_Email_permission__c),
                        toLabel(CRM_CP_Phone__c),
                        toLabel(CRM_CP_Letter__c),
                        toLabel(CRM_CP_SMS__c),
                        toLabel(CRM_CP_Consent_Type__c),
                        toLabel(CRM_CP_Latest_Consent_Channel__c),
                        CRM_CP_Consent_Expiry_Date__c,
                        CRM_CP_Consent_Version__c,
                        toLabel(CRM_CP_Fax__c),
                        CRM_CP_Latest_Consent_Date__c from Individual where Id = :lstData[0].IndividualId];
            }
            else{
                return null;
            }
        }
        else if(objectName == Label.CRM_CP_Lead){
            List<Lead> lstData = [SELECT IndividualId from Lead where Id = :recordId];
            if(!lstData.isEmpty()  && lstData[0].IndividualId != null){
                return [SELECT Id,toLabel(CRM_CP_Promotional_Email_Permission__c),
                        toLabel(CRM_CP_Informational_Email_permission__c),
                        toLabel(CRM_CP_Phone__c),
                        toLabel(CRM_CP_Letter__c),
                        toLabel(CRM_CP_SMS__c),
                        toLabel(CRM_CP_Consent_Type__c),
                        toLabel(CRM_CP_Latest_Consent_Channel__c),
                        CRM_CP_Consent_Expiry_Date__c,
                        CRM_CP_Consent_Version__c,
                        toLabel(CRM_CP_Fax__c),
                        CRM_CP_Latest_Consent_Date__c from Individual where Id = :lstData[0].IndividualId];
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }
    
}