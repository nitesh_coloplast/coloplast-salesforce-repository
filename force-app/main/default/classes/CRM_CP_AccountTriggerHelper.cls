/**
* @author   	 : Anisa Shaikh
* @date 		 : 09/20/2018
* @description 	 : This class contains all the helper methods for Account object related processing that will be invoked
*                  from its trigger handler class.
* ------------------------------------------ Version Control ----------------------------------------------------------
* Version 1.0    : Anisa Shaikh - Original
* Version 1.1  : Anisa Shaikh - Added null check for TKT-002955
*/


public class CRM_CP_AccountTriggerHelper {


	/**
	* @author 		: Anisa Shaikh
	* @date 		: 09/20/2018
	* @description 	: This method will evaluate if the DOI trigger field needs to be set for the newly created records
	* @param 		: List<Sobject> lstNewAccounts
	* @returns      : boolean
	*/
	public static boolean updateDOITriggerOnInsert( List<Sobject> lstNewAccounts){

		//If Eloqua user  exit
		if(CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserID()).CRM_CP_Disable_DOI_Trigger__c){
			return false;
		}

		//Check if Email Permission is 'Yes - to be confirmed' or consent is 'Yes - to be confirmed'
		for(Sobject acc: lstNewAccounts){

			//Added below for lead conversion, ensure both c and pc fields for OCN are populated
			if(acc.get('CRM_CP_Owner_Company_Number__pc') != null){
				acc.put('CRM_CP_Owner_Company_Number__c', acc.get('CRM_CP_Owner_Company_Number__pc'));
			}
			else if(acc.get('CRM_CP_Owner_Company_Number__c') != null){
				acc.put('CRM_CP_Owner_Company_Number__pc', acc.get('CRM_CP_Owner_Company_Number__c'));
			}

			//OCN belongs to countries where DOI in enabled
			if(acc.get('CRM_CP_Owner_Company_Number__pc') != null &&
			   !System.Label.CRM_CP_DOI_enables_countries.contains((String)acc.get('CRM_CP_Owner_Company_Number__c'))){
				continue;
			}

			/**Check if any Email Permissions are 'Yes - to be confirmed' OR
			   consent is 'Yes - to be confirmed' and Email Permissions are not 'No'
			**/
			System.debug('CRM_CP_ELQ_Informational_Email_Perm__pc '+acc.get('CRM_CP_ELQ_Informational_Email_Perm__pc'));
			System.debug('CRM_CP_ELQ_Promotional_Email_Permission__pc '+acc.get('CRM_CP_ELQ_Promotional_Email_Permission__pc'));
			System.debug('CRM_CP_ELQ_Consent__pc '+acc.get('CRM_CP_ELQ_Consent__pc'));

			if(acc.get('CRM_CP_ELQ_Informational_Email_Perm__pc') == 'Yes - to be confirmed' ||
			   acc.get('CRM_CP_ELQ_Promotional_Email_Permission__pc') == 'Yes - to be confirmed' ||
			   (acc.get('CRM_CP_ELQ_Promotional_Email_Permission__pc') != 'No' &&
			    acc.get('CRM_CP_ELQ_Informational_Email_Perm__pc') != 'No' &&
			    acc.get('CRM_CP_ELQ_Consent__pc') == 'Yes - to be confirmed')){

			    //If a record found then set to 'Yes'
				acc.put('CRM_CP_ELQ_DOI_Trigger__pc', 'Yes');
			}
		}
		return true;
	}


	/**
	* @author 		: Anisa Shaikh
	* @date 		: 09/20/2018
	* @description 	: This method will evaluate if the DOI trigger field needs to be set for the updated records
	* @param 		: Map<Id, Sobject> mapNewAccounts
	*				  Map<Id, Sobject> mapOldAccounts
	* @returns      : boolean
	*/

	public static boolean updateDOITriggerOnUpdate( Map<Id, Sobject> mapNewAccounts, Map<Id, Sobject> mapOldAccounts){

		Map<Id, Sobject> mapQualifiedAccounts = new Map<Id, Sobject>();
		//If Eloqua user or if eloqua user and email change status is not set to '0'  exit
		if(CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserID()).CRM_CP_Disable_DOI_Trigger__c){
			for(Id accId:mapNewAccounts.keySet()){
				if((mapNewAccounts.get(accId).get('CRM_CP_ELQ_Email_Change_Status__pc') !=
				   mapOldAccounts.get(accId).get('CRM_CP_ELQ_Email_Change_Status__pc')) &&
				   mapNewAccounts.get(accId).get('CRM_CP_ELQ_Email_Change_Status__pc') =='0' ){
					mapQualifiedAccounts.put(accId,mapNewAccounts.get(accId));
				}
			}
			if(mapQualifiedAccounts.isEmpty()){
				return false;
			}
		}


		//Check if Email Permission is 'Yes - to be confirmed' or consent is 'Yes - to be confirmed'
		for(Id accId: !mapQualifiedAccounts.keySet().isEmpty() ? mapQualifiedAccounts.keySet(): mapNewAccounts.keySet()){

			//OCN belongs to countries where DOI in enabled
			//Added null check for TKT-002955
			if(mapNewAccounts.get(accId).get('CRM_CP_Owner_Company_Number__c') != null &&
			!Label.CRM_CP_DOI_enables_countries.contains((String)mapNewAccounts.get(accId).
			get('CRM_CP_Owner_Company_Number__c'))){
				continue;
			}

			/**Check if Consent is changed to 'Yes - to be confirmed' AND
			   and Email Permissions are not 'No'
			**/
			if((((mapNewAccounts.get(accId).get('CRM_CP_ELQ_Consent__pc') !=
			     mapOldAccounts.get(accId).get('CRM_CP_ELQ_Consent__pc'))) ||

			    ((mapNewAccounts.get(accId).get('CRM_CP_ELQ_Promotional_Email_Permission__pc') !=
			      mapOldAccounts.get(accId).get('CRM_CP_ELQ_Promotional_Email_Permission__pc')))  ||

			    ((mapNewAccounts.get(accId).get('CRM_CP_ELQ_Informational_Email_Perm__pc') !=
			      mapOldAccounts.get(accId).get('CRM_CP_ELQ_Informational_Email_Perm__pc')))) &&

				((mapNewAccounts.get(accId).get('CRM_CP_ELQ_Consent__pc') == 'Yes - to be confirmed') &&
				(mapNewAccounts.get(accId).get('CRM_CP_ELQ_Promotional_Email_Permission__pc') != 'No') &&
				(mapNewAccounts.get(accId).get('CRM_CP_ELQ_Informational_Email_Perm__pc') != 'No'))){

			    //If a record found then set to 'Yes'
				mapNewAccounts.get(accId).put('CRM_CP_ELQ_DOI_Trigger__pc', 'Yes');
			}
			/**Check if any Email Permission is 'Yes - to be confirmed'
			**/
			else if(((mapNewAccounts.get(accId).get('CRM_CP_ELQ_Promotional_Email_Permission__pc') !=
			          mapOldAccounts.get(accId).get('CRM_CP_ELQ_Promotional_Email_Permission__pc')) &&
			        (mapNewAccounts.get(accId).get('CRM_CP_ELQ_Promotional_Email_Permission__pc') ==
			         'Yes - to be confirmed'))
			         ||
			        ((mapNewAccounts.get(accId).get('CRM_CP_ELQ_Informational_Email_Perm__pc') !=
			          mapOldAccounts.get(accId).get('CRM_CP_ELQ_Informational_Email_Perm__pc')) &&
			        (mapNewAccounts.get(accId).get('CRM_CP_ELQ_Informational_Email_Perm__pc') ==
			         'Yes - to be confirmed'))
			        ){
			    //If a record found then set to 'Yes'
				mapNewAccounts.get(accId).put('CRM_CP_ELQ_DOI_Trigger__pc', 'Yes');
			}
		}
		return true;
	}
}