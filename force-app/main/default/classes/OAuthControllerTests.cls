@isTest
public class OAuthControllerTests {
    
    public static PageReference pageRef = Page.CRM_CP_ActivityDashboard;
    public static OAuthApp_pbi__c app;
    
    @testSetup public static void setUp()
    {   
        app = new OAuthApp_pbi__c();
        app.Name = 'PowerBI';
        app.Token_Expires_On__c = '0';
        app.Client_Id__c = 'clientId';
        app.Client_Secret__c = 'clientSecret';
        app.Authorization_URL__c = 'https://login.windows.net/common/oauth2/authorize';
        app.Access_Token_URL__c = 'https://login.microsoftonline.com/common/oauth2/token';
        app.Resource_URI__c = 'https://analysis.windows.net/powerbi/api';
        insert app;
        OAuthApp_pbi__c pbi = new OAuthApp_pbi__c();
        pbi.Access_Token_URL__c = 'https://login.microsoftonline.com/common/oauth2/token';
        pbi.Authorization_URL__c = 'https://login.windows.net/common/oauth2/authorize';
        pbi.Client_Id__c = 'clientId';
        pbi.Client_Secret__c = 'clientSecret';
        pbi.Resource_URI__c = 'https://analysis.windows.net/powerbi/api';
        pbi.Token_Expires_On__c = '0';
        pbi.Name = 'PowerBI';
        insert pbi;
       // OAuthController controller = new OAuthController();
        //controller.application_name = 'PowerBI';
    }
    
    public static testMethod void createController()
    {  
        OAuthController controller = new OAuthController();     
        System.assertNotEquals(controller, null);       
    }   
    
    public static testMethod void checkAccessTokenNotNull()
    {
        OAuthController controller = new OAuthController();
        System.assertNotEquals(controller.PBIAccess_token, null);   
    }
    
    public static testMethod void checkRefreshTokenNotNull()
    {
        OAuthController controller = new OAuthController();
        System.assertNotEquals(controller.PBIRefresh_token, null);  
    }
    
    public static testMethod void checkExpiresOnNotNull()
    {
        OAuthController controller = new OAuthController();
        System.assertNotEquals(controller.PBIExpires_on, null); 
    }
    
    public static testMethod void checkHasTokenReturnsFalse()
    {
        OAuthController controller = new OAuthController();
        System.assertEquals(controller.getHasToken(), false);   
    }
    
    public static testMethod void checkHasTokenReturnsTrue()
    {
        OAuthController controller = new OAuthController();
        controller.PBIAccess_token =  'testToken';
        System.assertEquals(controller.getHasToken(), false);   
    }
    
    public static testMethod void getAuthURLReturnSuccess()
    {
        Test.setCurrentPage(pageRef);
        OAuthController controller = new OAuthController();
        String authUrl = controller.getAuthUrl();
        
        System.assertEquals(authUrl.contains('https://login.windows.net/common/oauth2/authorize?'), true);
    }
    
    public static testMethod void redirectOnCallbackCreatesCookies()
    {   
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        OAuthController controller = new OAuthController();
        controller.application_name = 'PowerBI';
        controller.isCallback = true;

        OAuthControllerTests.getAuthURLReturnSuccess();
        pageRef = new PageReference('https://c.eu11.visual.force.com/apex/Report?code=testCode');
        Test.setCurrentPage(pageRef);
        
        PageReference ref = controller.redirectOnCallback(pageRef);
        
        String accessCookie = controller.PBIAccess_token;
        String refreshCookie =  controller.PBIRefresh_token;
        
        System.assertEquals('accessCookieToken',accessCookie);
        System.assertEquals('refreshCookieToken',refreshCookie);
        Test.stopTest();
    }

    public static testMethod void refreshToken()
    {   
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        OAuthControllerTests.getAuthURLReturnSuccess();
        pageRef = new PageReference('https://c.eu11.visual.force.com/apex/Report?code=testCode');
        Test.setCurrentPage(pageRef);
        OAuthController controller = new OAuthController();
        controller.isCallback = true;

        controller.refreshAccessToken(pageRef);
        
        String accessCookie = controller.PBIAccess_token;
        String refreshCookie =  controller.PBIRefresh_token;
        
        System.assertEquals('accessCookieToken',accessCookie);
        System.assertEquals('refreshCookieToken',refreshCookie);
        Test.stopTest();
    }
}