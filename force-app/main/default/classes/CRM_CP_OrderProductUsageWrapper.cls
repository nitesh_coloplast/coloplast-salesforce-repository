/*
 * CRM_CP_SubmitOrderResponse : This class is wrapper class to 
 	convey information between Lightning component to Apex code.
 *
 * Created Date : 16 - March - 2018
 * 
 */
public class CRM_CP_OrderProductUsageWrapper
{
	@AuraEnabled
	public OrderItem objOrderItem;
	@AuraEnabled
	public Id idProdRequest;
	@AuraEnabled
	public Product2 objProduct;
	@AuraEnabled
	public string idProductUsageId ='';
	@AuraEnabled
	public CRM_CP_Product_Usage__c objProdUsage;
	@AuraEnabled
	public String strResonForStopUsing;	
	@AuraEnabled
	public boolean boolIsErrorInOldProduct = false;
	@AuraEnabled
	public boolean boolIsErrorInReasonForStopUsing = false;
	@AuraEnabled
	public string strErrorInOldProduct = '';
	@AuraEnabled
	public string strErrorInReasonForStopUsing = '';
	@AuraEnabled
	public boolean boolIsDisabled = true;
	@AuraEnabled
	public boolean boolIsEligibleForProductUsage = true;
}