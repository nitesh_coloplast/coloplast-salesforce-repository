public with sharing class CRM_CP_CampaignTriggerHandler implements CRM_CP_TriggerInterface{
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
  */
  
    public Boolean IsDisabled(){
        return CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserId()).CRM_CP_Triggers_Disabled__c;
    }

    
    public void BeforeInsert(List<SObject> newItems) {
        CRM_CP_CampaignTriggerUtilities.insertOverallTargetAudience(newItems);
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        CRM_CP_CampaignTriggerUtilities.updateOverallTargetAudience(oldItems,newItems);
    }
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterInsert(Map<Id, SObject> newItems) {          
        //Call the generic mapping utility to map the Contact with the corresponding individuals
        CRM_CP_CampaignTriggerUtilities.insertCampaignMemberStatus(newItems);
        
    }
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        if(CRM_CP_TriggerRecursionHandler.Campaign_runOnce()){
            //Write your update logics here, make you nothing is outside this if condition
        }
        
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}