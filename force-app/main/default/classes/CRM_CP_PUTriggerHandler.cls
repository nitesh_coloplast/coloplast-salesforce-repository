/**
* @author       : Mruga Shastri
* @date         : 06-09-2018
* @description  : This is the Product Usage Trigger Handler class which will have all trigger event methods implmented 
*                  and it is recommnended that all functionality of trigger should reside in this code of any utility 
*                  method can be called from here.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0 : Mruga Shastri - Created initial version for US 1367. 
*/

public with sharing class CRM_CP_PUTriggerHandler implements CRM_CP_TriggerInterface{

    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;

/** 
* @author       : Mruga Shastri
* @date         : 06-09-2018018
* @description  : This method will check whether the account trigger is disaled or not. 
*                 The configuration is stored in Trigger Setting custom metadata.
*/
        public Boolean IsDisabled(){
            return CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserId()).CRM_CP_Triggers_Disabled__c;
        }

/** 
* @author       : Mruga Shastri
* @date         : 06-09-2018
* @description  : This method will process all the records before inserting the data to the database(NOT commit)..
* @param        : newItems - this will accepts the new reocords of account.
*/
    public void BeforeInsert(List<SObject> newItems) {

    }

/** 
* @author       : Mruga Shastri
* @date         : 06-09-2018
* @description  : This method will process all the records before updating the data to the database(NOT commit)..
* @param        : newItems - this will accepts the new reocords of account.
* @param        : oldItems - this will accepts the old reocords of account.
*/
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
         
    }

/** 
* @author       : Mruga Shastri
* @date         : 06-09-2018
* @description  : This method will process all the records before deleting the data from the database(NOT commit)..
* @param        : oldItems - this will accepts the old reocords of account.
*/
    public void BeforeDelete(Map<Id, SObject> oldItems) {}

/** 
* @author       : Mruga Shastri
* @date         : 06-09-2018
* @description  : This method will process all the records after inseting the data from the database(NOT commit).
* @param        : newItems - this will accepts the new reocords of account.
*/
    public void AfterInsert(Map<Id, SObject> newItems){            
        //Check the trigger recursion for Health Basics
        If(CRM_CP_TriggerRecursionHandler.PU_runOnce()){
           Map<Id, sObject> oldItems = new Map<Id, sObject>();
           CRM_CP_TriggerHandlerUtilities.productUsageToParentSync( newItems, oldItems);
        }


    }

/** 
* @author       : Mruga Shastri
* @date         : 06-09-2018
* @description  : This method will process all the records after updating the data from the database(NOT commit).
* @param        : oldItems - this will accepts the old reocords of account.
* @param        : oldItems - this will accepts the old reocords of account.
*/
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    //Check the trigger recursion for Health Basics
        If(CRM_CP_TriggerRecursionHandler.PU_runOnce()){
           CRM_CP_TriggerHandlerUtilities.productUsageToParentSync( newItems, oldItems); 
        }
        


    }
/** 
* @author       : Mruga Shastri
* @date         : 06-09-2018
* @description  : This method will process all the records after deleting the data from the database(NOT commit).
* @param        : oldItems - this will accepts the old reocords of account.
*/
    public void AfterDelete(Map<Id, SObject> oldItems) {}

/** 
* @author       : Mruga Shastri
* @date         : 06-09-2018
* @description  : This method will process all the records after undeleting the data from the database(NOT commit).
* @param        : oldItems - this will accepts the old reocords of account.
*/
    public void AfterUndelete(Map<Id, SObject> oldItems) {}

}