/**
* @author       : Nitesh Kumar
* @date         : 04/17/2018
* @description  : Defines the interface for trigger handlers. All the Trigger handler will implement this and have to implement all the methods in the
*                 handler class.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0   : Nitesh Kumar - Creted all the method as part of US-0693 and US-0556
*/
public Interface CRM_CP_TriggerInterface {
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration of Beore Insert events.
    */
    void BeforeInsert(List<SObject> newItems);
 
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration of before update events.
    */
    void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);
 
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration of Beore delete events.
    */
    void BeforeDelete(Map<Id, SObject> oldItems);
 
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration of after insert events.
    */
    void AfterInsert(Map<Id, SObject> newItems);
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration of after update events.
    */
    void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration of after delete events.
    */
    void AfterDelete(Map<Id, SObject> oldItems);
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration of after Undelete events.
    */
    void AfterUndelete(Map<Id, SObject> oldItems);
 
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration for the trigger switch check
    */
    Boolean IsDisabled();
}