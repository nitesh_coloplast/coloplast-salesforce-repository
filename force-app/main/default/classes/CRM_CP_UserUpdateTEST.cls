/**
* @author       : Ninad Patil
* @date         : 06 Aug 2018
* @description  : This class is Test class for Sample Request to Case Order conversion utilities.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Ninad Patil - 
*/
@isTest
private class CRM_CP_UserUpdateTEST {
    
    /** 
    * @author       : Ninad Patil
    * @date         : 06 AUG 2018
    * @description  : This method creates data in the system before test methods are executed
    */
    @testSetup static void setup() 
    {
        //Add allowed language setting records
        CRM_CP_AllowedLanguages__c objCust = new CRM_CP_AllowedLanguages__c(name = 'en_US');
        insert objCust;
        
        List<CRM_CP_CPBC_Editable_User_Fields__c>lstCustSet = CRM_CP_TestDataUtility.getEditableUserFieldsForCPBCUser();        
        if(lstCustSet != null && !lstCustSet.isEmpty()) insert lstCustSet;
    }
    /** 
    * @author       : Ninad Patil
    * @date         : 06 AUG 2018
    * @description  : This method test conversion of case under Consumer Account.
    */
    @isTest static void testCPBCValidator() 
    {
    	User objUser = CRM_CP_TestDataUtility.createTestUser('Call Center Agent');
        insert objUser;    
        
        List<PermissionSet> lstPermissionSet = [Select id, Name from PermissionSet Where name = 'CRM_CP_CPBC_User_Update_Permission' 
                                                                                      OR name =: Label.CRM_CP_QAS_AddressValidation_PermissionSet];
        
        if(lstPermissionSet != null 
            && !lstPermissionSet.isEmpty())
        {
            //Assign Permission set
            PermissionSetAssignment objPermissionset = new PermissionSetAssignment();
            objPermissionset.AssigneeId = objUser.Id;
            objPermissionset.PermissionSetId = lstPermissionSet[0].Id;
            
            insert objPermissionset;
        } 
        
        List<user> lstUser = [Select id, name ,firstName from User Where Name like '%TestUser%'];
        if(lstUser != null && !lstUser.isEmpty())
        {
            system.runAs(lstUser[0])
            {        
                Test.startTest();
                
                lstUser[0].firstName = lstUser[0].firstName + '_1';
                try
                {
                    update lstUser[0];
                }
                catch(exception e)
                {
                    system.assert(e.getMessage().contains(system.Label.CPBC_User_Update_Error));
                }
                Test.stopTest();
            }
        }
       
    }
    /** 
    * @author       : Ninad Patil
    * @date         : 06 AUG 2018
    * @description  : This method test conversion of case under Consumer Account.
    */
    @isTest static void testWithoutCPBC() 
    {
    	User objUser = CRM_CP_TestDataUtility.createTestUser('Call Center Agent');
        insert objUser;    
              
        
        List<user> lstUser = [Select id, name ,firstName from User Where Name like '%TestUser%'];
        if(lstUser != null && !lstUser.isEmpty())
        {
            system.runAs(lstUser[0])
            {        
                Test.startTest();
                
                lstUser[0].firstName = lstUser[0].firstName + '_1';
                try
                {
                    update lstUser[0];
                }
                catch(exception e)
                {
                    system.assert(e.getMessage().contains(system.Label.CPBC_User_Update_Error));
                }
                Test.stopTest();
            }
        }
       
    }
    /** 
    * @author       : Animesh Chauhan
    * @date         : 06 SEP 2018
    * @description  : This method test conversion of class CRM_CP_QAS_AddressVisibilityExtention.
    */
    @isTest static void testQASExtentionWithoutUser() 
    {
    	User objUser = CRM_CP_TestDataUtility.createTestUser('Call Center Agent');
        insert objUser;    
        
        List<PermissionSet> lstPermissionSet = [Select id, Name from PermissionSet Where name = 'CRM_CP_CPBC_User_Update_Permission' 
                                                                                      OR name =: Label.CRM_CP_QAS_AddressValidation_PermissionSet];
        
        if(lstPermissionSet != null 
            && !lstPermissionSet.isEmpty())
        {
            //Assign Permission set
            PermissionSetAssignment objPermissionset = new PermissionSetAssignment();
            objPermissionset.AssigneeId = objUser.Id;
            objPermissionset.PermissionSetId = lstPermissionSet[0].Id;
            
            insert objPermissionset;
        } 
        List<user> lstUser = [Select id, name ,firstName from User Where Name like '%TestUser1%'];
        if(lstUser != null && !lstUser.isEmpty())
        {
            ApexPages.StandardController sc = new ApexPages.StandardController(lstUser[0]);    
            CRM_CP_QAS_AddressVisibilityExtention cls = new CRM_CP_QAS_AddressVisibilityExtention(sc);
            system.runAs(lstUser[0])
            {        
                Test.startTest();
                cls.getIsQASVisibleForLoggedInUser();
                Test.stopTest();
            }
        }   
    }
    
     /** 
    * @author       : Animesh Chauhan
    * @date         : 06 SEP 2018
    * @description  : This method test conversion of class CRM_CP_QAS_AddressVisibilityExtention .
    */
    @isTest static void testQASExtention() 
    {
    	User objUser = CRM_CP_TestDataUtility.createTestUser('Call Center Agent');
        insert objUser;    
        
        List<PermissionSet> lstPermissionSet = [Select id, Name from PermissionSet Where name = 'CRM_CP_CPBC_User_Update_Permission' 
                                                                                      OR name =: Label.CRM_CP_QAS_AddressValidation_PermissionSet];
        
        if(lstPermissionSet != null 
            && !lstPermissionSet.isEmpty())
        {
            //Assign Permission set
            PermissionSetAssignment objPermissionset = new PermissionSetAssignment();
            objPermissionset.AssigneeId = objUser.Id;
            objPermissionset.PermissionSetId = lstPermissionSet[0].Id;
            
            insert objPermissionset;
        } 
        List<user> lstUser = [Select id, name ,firstName from User Where Name like '%TestUser%'];
        if(lstUser != null && !lstUser.isEmpty())
        {
            ApexPages.StandardController sc = new ApexPages.StandardController(lstUser[0]);    
            CRM_CP_QAS_AddressVisibilityExtention cls = new CRM_CP_QAS_AddressVisibilityExtention(sc);
            Test.startTest();
            cls.getIsQASVisibleForLoggedInUser();
            Test.stopTest();
        }    
    }
   
    
}