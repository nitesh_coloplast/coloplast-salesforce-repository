/*
 * CRM_CP_TestDataUtility : This is class created test data records
 * Created Date : 16 - March - 2018
 * 
 */
@isTest 
public class CRM_CP_TestDataUtility 
{
     /*
     * createTestUser : This method tests User and return it to the calling test class.
     * @param profileName : it accepts profileName as strig
     * @return User
     * @throws None
     */
    public static User createTestUser(String profileName)
    {
        Profile p = [select name, id from profile where name= :profileName]; 
        system.assert(p.id != null);
        String testUserName = String.valueOf(System.now().getTime()) + '@coloplast.com';
        User usr = new User( alias = 'TestUser', email='testUser@coloplast.com',
                                emailencodingkey='UTF-8', lastname='TestUser1', languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Denver', username=testUserName, isActive=true);
        return usr;
    }
   
    /*
     * createLead :It creates instances of Lead.
     * @param strFirstName : firstName of the Lead
     * @param strLastName : lastName of the Lead
     * @param strRecordTypeName : record type of the Lead
     * @param strPhone : Phone of the Lead
     * @param strEmail : Email of the Lead
     * @return User
     * @throws None
     */
    public static Lead createLead(String strFirstName, string strLastName, string strRecordTypeName, 
        string strPhone, string strEmail)
    {
        ID rtId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(strRecordTypeName).getRecordTypeId();
        Lead objLead = new Lead();
        objLead.firstName = strFirstName;
        objLead.lastName = strLastName;
        objLead.phone = strPhone;
        objLead.Email = strEmail;
        objLead.company = 'Coloplast';
        return objLead;
    }
    /*
     * createCaseSampleRequest :It creates instances of Case.
     * @param strLeadId : Lead Id
     * @param strconId : Contact Id
     * @return User
     * @throws None
     */
    public static Case createCaseSampleRequest(string strLeadId, string strconId)
    {
        ID rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Sample Request').getRecordTypeId();
        Case objCase = new Case();
        objCase.recordTypeId = rtId;
        objCase.AccountId = strLeadId;
        objCase.contactId = strconId;
        objCase.CRM_CP_Due_Date__c = system.today();
        objCase.CRM_CP_City__c = 'TEST_CITY';
        objCase.CRM_CP_Country__c = 'Denmark';
        objCase.CRM_CP_Street__c = 'TEst_STreet';
        return objCase;
    }
    /*
     * createCase :It creates instances of Case.
     * @param strLeadId : Lead Id
     * @param strconId : Contact Id
     * @return User
     * @throws None
     */
    public static Case createContactProtocol(Account objAcc,
                                            Contact objContact )
    {
        ID rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(system.Label.CRM_CP_ContactProtocolLabel).getRecordTypeId();
        
        Case objCase = new Case();
        objCase.recordTypeId = rtId;
        objCase.AccountId = objAcc.Id;
        objCase.contactId = objContact.Id;                
        return objCase;
    }
    /*
     * createAccount :It Creates instance of Account
     * @param strName : Name of the account
     * @return Account
     * @throws None
     */
    public static Account createAccount(string strName)
    {
        ID rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
        Account objAccount = new Account();
        objAccount.lastName = strName;
        objACcount.recordtypeId = rtId;
        return objAccount;
    }
    /*
     * createAccount :It Creates instance of Account
     * @param strName : Name of the account
     * @return Account
     * @throws None
     */
    public static Account createAccountRecTypeAcc(string strName)
    {
        ID rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        Account objAccount = new Account();
        objAccount.Name = strName;
        objAccount.shippingStreet = 'test';
        objACcount.recordtypeId = rtId;
        return objAccount;
    }
    /*
     * createContact :It Creates instance of Contact
     * @param strName : Name of the Contacts
     * @param Account : objAcc
     * @return Contact
     * @throws None
     */
    public static Contact createContact(string strName, Account objAcc)
    {
        ID rtId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Professional Contact').getRecordTypeId();
        Contact objCon = new Contact();
        objCon.accountId = objAcc.Id;
        objCon.lastName = strName;
        objCon.CRM_CP_Owner_Company_Number__c = '190';
        return objCon;
    }
    /*
     * createPriceBookSampleOrder :It Creates instance of Pricebook
     * @param : None
     * @return Pricebook2
     * @throws None
     */
    public static Pricebook2 createPriceBookSampleOrder()
    {
        Pricebook2 customPB = new Pricebook2(Name='Sample Order', isActive=true, CRM_CP_Owner_Company_Number__c = '190', 
            CRM_CP_Pricebook_Type__c = System.Label.CRM_CP_SampleOrderPB,
            CRM_CP_External_ID__c = '1234');
        return customPB;
    }
    /*
     * createPriceBook :It Creates instance of Pricebook
     * @param : None
     * @return Pricebook2
     * @throws None
     */
    public static Pricebook2 createPriceBook(string strType, 
                                                        string strOCN )
    {
        Pricebook2 customPB = new Pricebook2(Name=strType + '-' + strOCN, 
                                isActive=true, CRM_CP_Owner_Company_Number__c = strOCN,         
                                CRM_CP_Pricebook_Type__c = strType,
                                CRM_CP_External_ID__c = strType + '-' + strOCN);
        return customPB;
    }
    /*
     * createProduct :It Creates instance of product
     * @param : StrName : Name of the Product
     * @param : strHierarchy : Hierarchy level of the Product
     * @return product2
     * @throws None
     */
    Public static product2 createProduct(string strName, string strHierarchy)
    {
        Product2 objProd = new Product2();
        objProd.Name = strName;
        objProd.CRM_CP_Hierarchy_Level__c = strHierarchy;
        objProd.CRM_CP_Active_for_Product_Usage__c = true;
        objProd.CRM_CP_Active_for_Sampling__c = true;
        objProd.CRM_CP_Active_for_Requests_and_Marketing__c = true;
        objProd.CRM_CP_External_ID__c = strName;
        objProd.CRM_CP_Manufacturer__c = system.Label.CRM_CP_Coloplast;
        objProd.CRM_CP_Owner_Company_Number__c = '190';
        return objProd;
    }
    /*
     * PricebookEntry :It Creates instance of pricebookENtry
     * @param : objProd : Product Instance
     * @param : objPB : pricebook instance
     * @return PricebookEntry
     * @throws None
     */
    Public static PricebookEntry createPriceBook(Product2 objProd, Pricebook2 objPB)
    {
        PricebookEntry objPribookEntry = new PricebookEntry(
            Pricebook2Id = objPB.Id, Product2Id = objProd.Id,
            UnitPrice = 12000, IsActive = true,
            CRM_CP_External_ID__c = objProd.name);
        return objPribookEntry;
    }
    /*
     * createProdrequest :It Creates instance of Product request
     * @param : objProd : Product Instance
     * @param : objCase : case instance
     * @param : boolToBeConverted : to be converted flag
     * @return CRM_CP_Product_Request__c
     * @throws None
     */
    public static CRM_CP_Product_Request__c createProdrequest(Product2 objProd, Case objCase, boolean boolToBeConverted)
    {
        CRM_CP_Product_Request__c objProdreq = new CRM_CP_Product_Request__c();
        objProdreq.CRM_CP_Product__c = objProd.Id;
        objProdreq.CRM_CP_Quantity__c = 2;
        objProdreq.CRM_CP_To_Be_Converted__c = boolToBeConverted;
        objProdreq.CRM_CP_Case__c = objCase.Id;
        return objProdreq;
    }
    
    /*
     * createOrder :It Creates instance of Order
     * @param : objAccount : Account Instance
     * @param : objContact : Contact Instance
     * @return Order : Order object 
     * @throws None
     */
    public static Order createOrder(Account objAccount)
    {
        Order objOrder = new Order();
        objOrder.accountID = objAccount.Id;
        objOrder.EffectiveDate = system.today() +1;
        objOrder.Status = 'Draft';
        
        objOrder.ShippingStreet = 'Test_Street';
        objOrder.ShippingCountry = 'Denmark';
        objOrder.ShippingCity = 'TEST_City';
        
        return objOrder;
    }
    
    /*
     * createAccount :It Creates instance of Account
     * @param strName : Name of the account
     * @return Account
     * @throws None
     */
    public static Account createAccountRecTypeConsumer(string strName)
    {
        ID rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
        Account objAccount = new Account();
        objAccount.firstName = strName;
        objAccount.lastName = strName;
        objAccount.shippingStreet = 'test';
        objACcount.recordtypeId = rtId;
        return objAccount;
    }
    
    /*
     * createAccount :It Creates instance of Account
     * @param strName : Name of the account
     * @return Account
     * @throws None
     */
    public static CRM_CP_Product_Usage__c createProdUsage(Account objAcc, Product2 objProduct )
    {
        CRM_CP_Product_Usage__c objProdUsage = new CRM_CP_Product_Usage__c();
                
        objProdUsage.CRM_CP_Account_Name_Legal_Name__c =  objAcc.Id;
        objProdUsage.CRM_CP_Product__c = objProduct.Id;
            
        objProdUsage.CRM_CP_Product_Sampled_Date__c = system.today();
        objProdUsage.CRM_CP_Funnel_Stage__c = system.Label.CRM_CP_FunnelStage_Ordered;
        objProdUsage.CRM_CP_Product_Usage_Status__c = system.Label.CRM_CP_ProdUsageStatus_Current;
        objProdUsage.CRM_CP_Manufacturer__c = objProduct.CRM_CP_Manufacturer__c;
        return objProdUsage;
    }
    
    public static List<Contact> insertBulkContacts(){
        // Load the test accounts from the static resource
        //List<sObject> ls = Test.loadData(Contact.sObjectType, 'testAccounts');
        //String acctName = a1.Name;
        //System.debug(acctName);
        return null;
    }
    
    public static OrderItem createOrderItem (Order objOrder, PricebookEntry objPBEntry, Product2 objProduct)
    {
        OrderItem objOrderItem = new OrderItem();
        objOrderItem.OrderId = objOrder.id;
        objOrderItem.PricebookEntryId = objPBEntry.Id;
        objOrderItem.Quantity = 1;
        objOrderItem.UnitPrice = 100;
        
        return objOrderItem;
    }
     /*
     * createOpportunity :It Creates instance of OPportunity
     * @param strName : Name of the account
     * @return Account
     * @throws None
     */
    public static Opportunity createOpportunity(Account objAcc, 
                                        String strName,
                                        date ClosedDate,
                                        String strStageName )
    {
        Opportunity objOpp = new Opportunity();
        objOpp.accountId = objAcc.Id;
        objOpp.Name = strName;
        objOpp.Stagename = strStageName;
        objOpp.closeDate = system.today() +1;
        objOpp.CRM_CP_Annual_Value_Units__c = 2;
        return  objOpp;
    }
    /*
     * createConsumerCampaign :It Creates instance of Consumer Campaign
     * @param strName : Name of the createConsumerCampaign
     * @return createConsumerCampaign
     * @throws None
     */
    public static Campaign createConsumerCampaign(String strName)
    {
        ID rtId = Schema.SObjectType.campaign.getRecordTypeInfosByName().get('Campaign').getRecordTypeId();
        Campaign objCamp = new Campaign();
        objCamp.Name = strName;
        objCamp.CRM_CP_Global_Campaign__c = false;
        objCamp.recordtypeId = rtId;
        objCamp.CRM_CP_Start_Date__c = System.today();
        objCamp.StartDate = System.today();
        objCamp.Status = 'Active';
        objCamp.CRM_CP_Aligned_with_Consumer_Manager__c = true;
        objCamp.CRM_CP_Overall_Target_Audience__c = 'Consumer';
        return  objCamp;
    }
    /*
     * getEditableUserFieldsForCPBCUser :It Creates custom setting records in CPBC user. 
     * @param Nonad
     * @return 
     * @throws None
     */
    public static List<CRM_CP_CPBC_Editable_User_Fields__c> getEditableUserFieldsForCPBCUser()
    {
    
        List<String> lstString = new List<String>{'CRM_CP_Subsidiary_Number__c',
                    'CRM_CP_Subsidiary_Number_Text__c',
                    'CRM_CP_Territory_Visit_Plan_Count__c',
                    'CurrencyIsoCode',
                    'DefaultCurrencyIsoCode',
                    'LastModifiedDate',
                    'LocaleSidKey',
                    'SystemModstamp',
                    'TimeZoneSidKey'};
        List<CRM_CP_CPBC_Editable_User_Fields__c> lstCustSet = new List<CRM_CP_CPBC_Editable_User_Fields__c>();
        
        for(String strName : lstString)
        {
            CRM_CP_CPBC_Editable_User_Fields__c objCustSet = new CRM_CP_CPBC_Editable_User_Fields__c();
            objCustSet.Name = strName;
            
            lstCustSet.add(objCustSet);
        }
    
        return lstCustSet;
    }
    
    /*
     * getSpokenAndwrittenLanguageCustomConfig :It Creates custom setting records in CPBC user. 
     * @param Nonad
     * @return 
     * @throws None
     */
    public static List<CRM_CP_Spoken_Written_Language_Config__c> getSpokenAndwrittenLanguageCustomConfig()
    {
	    List<CRM_CP_Spoken_Written_Language_Config__c> lstCustSet = new List<CRM_CP_Spoken_Written_Language_Config__c>();
	
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name = '190', CRM_CP_County__c = 'Denmark',	CRM_CP_Preferred_Spoken_Language__c = 'Danish',CRM_CP_Preferred_Written_Language__c= 'da-DK'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '210', CRM_CP_County__c = 'Austria',	CRM_CP_Preferred_Spoken_Language__c = 'German',CRM_CP_Preferred_Written_Language__c= 'de-AT'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '220', CRM_CP_County__c = 'Switzerland',CRM_CP_Preferred_Spoken_Language__c = 'German',CRM_CP_Preferred_Written_Language__c= 'de-CH'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '230', CRM_CP_County__c = 'Germany',	CRM_CP_Preferred_Spoken_Language__c = 'German',CRM_CP_Preferred_Written_Language__c= 'de-DE'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '340', CRM_CP_County__c = 'Belgium',	CRM_CP_Preferred_Spoken_Language__c = 'Dutch'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '350', CRM_CP_County__c = 'France',CRM_CP_Preferred_Spoken_Language__c = 'French'	 ));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '380', CRM_CP_County__c = 'UK',CRM_CP_Preferred_Spoken_Language__c = 'English',CRM_CP_Preferred_Written_Language__c= 'en-GB'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '410', CRM_CP_County__c = 'Finland',CRM_CP_Preferred_Spoken_Language__c = 'Finnish',CRM_CP_Preferred_Written_Language__c= 'fi-FI'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '430', CRM_CP_County__c = 'Sweden',CRM_CP_Preferred_Spoken_Language__c = 'Swedish',CRM_CP_Preferred_Written_Language__c= 'sv-SE'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '440', CRM_CP_County__c = 'Norway',CRM_CP_Preferred_Spoken_Language__c = 'Norwegian',CRM_CP_Preferred_Written_Language__c= 'nb-NO'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '450', CRM_CP_County__c = 'Italy',CRM_CP_Preferred_Spoken_Language__c = 'Italian',CRM_CP_Preferred_Written_Language__c= 'it-IT'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '460', CRM_CP_County__c = 'Spain',CRM_CP_Preferred_Spoken_Language__c = 'Spanish',CRM_CP_Preferred_Written_Language__c= 'es-ES'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '470', CRM_CP_County__c = 'Poland',CRM_CP_Preferred_Spoken_Language__c = 'Polish'));	 
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '480', CRM_CP_County__c = 'Netherlands',CRM_CP_Preferred_Spoken_Language__c = 'Dutch'	,CRM_CP_Preferred_Written_Language__c= 'nl-NL'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '490', CRM_CP_County__c = 'Hungary',	CRM_CP_Preferred_Spoken_Language__c = 'Hungarian',CRM_CP_Preferred_Written_Language__c= 'hu-HU'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '520', CRM_CP_County__c = 'Israel',CRM_CP_Preferred_Spoken_Language__c = 'Hebrew')); 
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '530', CRM_CP_County__c = 'Turkey',CRM_CP_Preferred_Spoken_Language__c = 'Turkish',CRM_CP_Preferred_Written_Language__c= 'tr-TR'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '555', CRM_CP_County__c = 'Portugal',	CRM_CP_Preferred_Spoken_Language__c = 'Portuguese'));	 
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '610', CRM_CP_County__c = 'Japan',CRM_CP_Preferred_Spoken_Language__c = 'Japanese',CRM_CP_Preferred_Written_Language__c= 'ja-JP'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '621', CRM_CP_County__c = 'China',CRM_CP_Preferred_Spoken_Language__c = 'Chinese'));	 
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '630', CRM_CP_County__c = 'Australia/ New Zealand',CRM_CP_Preferred_Spoken_Language__c = 'English',CRM_CP_Preferred_Written_Language__c= 'en-AU'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '645', CRM_CP_County__c = 'Korea',	CRM_CP_Preferred_Spoken_Language__c = 'Korean',CRM_CP_Preferred_Written_Language__c= 'ko-KR'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '650', CRM_CP_County__c = 'India',CRM_CP_Preferred_Spoken_Language__c = 'English'));	 
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '710', CRM_CP_County__c = 'USA',CRM_CP_Preferred_Spoken_Language__c = 'English',CRM_CP_Preferred_Written_Language__c= 'en-US'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '760', CRM_CP_County__c = 'Canada',CRM_CP_Preferred_Spoken_Language__c = 'English',CRM_CP_Preferred_Written_Language__c= 'en-CA'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '850', CRM_CP_County__c = 'Argentina',CRM_CP_Preferred_Spoken_Language__c = 'Spanish'	)); 
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '860', CRM_CP_County__c = 'Brazil',CRM_CP_Preferred_Spoken_Language__c = 'Portuguese'	,CRM_CP_Preferred_Written_Language__c= 'pt-BR'));
		lstCustSet.add(new CRM_CP_Spoken_Written_Language_Config__c(name= '900', CRM_CP_County__c = 'South Africa',CRM_CP_Preferred_Spoken_Language__c = 'English'));	
	
		return lstCustSet;
    }
}