/**
* @author   	: Ninad Patil
* @date 		: 06 Aug 2018
* @description 	: This class is Test class for Sample Request to Case Order conversion utilities.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Ninad Patil - 
*/
@isTest
private class CRM_CP_CaseTrigger_Test {
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method fetches logged in User's OCN. ELse returns 190 as default
	*/
	static private string getOCN()
    {
        string strOCN = '';
        
        List<user> lstUser = [Select id, 
                                     name,
                                     CRM_CP_Subsidiary_Number__C
                                     From User 
                                     Where Id = :UserInfo.getuserId()];
        if(lstUser != null && !lstuser.isEmpty())
        {
            if(lstUser[0].CRM_CP_Subsidiary_Number__C != null) strOCN = lstUser[0].CRM_CP_Subsidiary_Number__C;
            else strOCN = '190';
        }
        
        return strOCN;
    }
	/** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method creates data in the system before test methods are executed
	*/
    @testSetup static void setup() 
    {
        string strOCN = getOCN();
        
        //Create Person Account
        Account objAcc = CRM_CP_TestDataUtility.createAccount('ConsumerLastName');
        objAcc.CRM_CP_Owner_Company_Number__c = strOCN;
        insert objAcc;
        
        Contact objCon = new Contact();
        List<Contact> lstCon = [Select id, name from Contact Where accountID = : objAcc.Id];
        if(lstCon != null && !lstCon.isEMpty()) objCon = lstCon[0];
        
        //Create Case sample request
        Case objCase  = CRM_CP_TestDataUtility.createContactProtocol(objAcc, objCon);
        objCase.CRM_CP_Owner_Company_Number__c = strOCN;
        insert objCase;
    }
    /** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method tests Creation of contact Protocol case under Consumer if already contact protocol exists.
	*/
    @isTest static void testCreateContactProtocol() 
    {
    	string strOCN = getOCN();
                
        Test.startTest();
        try
        {
        	List<Account> lstAcc = [Select id, 
        								name, 
        								(select id from Cases), 
        								(Select id, name from contacts) 
        								FROM Account 
        								WHERE name like '%ConsumerLastName' ];        								
			if(lstAcc != null 
				&& !lstAcc.isEmpty())
			{
				//Create Case sample request
			    Case objCase  = CRM_CP_TestDataUtility.createContactProtocol(lstAcc[0], lstAcc[0].Contacts[0]);
			    objCase.CRM_CP_Owner_Company_Number__c = strOCN;
			    insert objCase;	
			}	
        }
        catch(Exception objException)
        {
	        system.assert(objException.getMessage().contains(system.Label.CRM_CP_ExistingContactProtocol));        	
        }
        Test.stopTest();
    }
    /** 
	* @author       : Ninad Patil
	* @date         : 06 AUG 2018
	* @description  : This method simulates update of case.
	*/
    @isTest static void testUpdateContactProtocol() 
    {
    	string strOCN = getOCN();
                
        Test.startTest();
        try
        {
        	List<Account> lstAcc = [Select id, 
        								name, 
        								(select id from Cases), 
        								(Select id, name from contacts) 
        								FROM Account 
        								WHERE name like '%ConsumerLastName' ];        								
			if(lstAcc != null 
				&& !lstAcc.isEmpty())
			{
				if(lstAcc[0].Cases != null
					&& !lstAcc[0].Cases.isEmpty())
				{
					lstAcc[0].Cases[0].status = system.Label.CRM_CP_ClosedDone;
					update lstAcc[0].Cases[0];
				}
				
				//Create Case sample request
			    Case objCase  = CRM_CP_TestDataUtility.createContactProtocol(lstAcc[0], lstAcc[0].Contacts[0]);
			    objCase.CRM_CP_Owner_Company_Number__c = strOCN;
			    insert objCase;	
			    
			    if(objCase.Id != null)
			    {
			    	lstAcc[0].Cases[0].status = 'Opened';
			    	update lstAcc[0].Cases[0];
			    }
			}	
        }
        catch(Exception objException)
        {
	        system.assert(objException.getMessage().contains(system.Label.CRM_CP_ExistingContactProtocol));        	
        }
        Test.stopTest();
    } 	   
}