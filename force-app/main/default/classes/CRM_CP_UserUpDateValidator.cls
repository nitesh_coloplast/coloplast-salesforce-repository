/*
 * CRM_CP_UserUpDateValidator : This class accepts User object and checks 
 	for data modifications based on CPBC user permissions
 *
 * Created Date : 30 - August - 2018
 * 
 Modification Log
 --------------------------------------------------------------------
 DeveloperName       Date           User Story		Description
 --------------------------------------------------------------------
 Ninad              30-Aug-2018     US1274          This class accepts 
 													User object and checks 
 													for data modifications 
 													based on CPBC user permissions
 Ninad				15-Sep-2018		US1864			Modified methdos to include 
 													lanaguage specific validation. 													                                                  
 */
public without sharing class CRM_CP_UserUpDateValidator 
{
	/*
     * validateUserCreds : This method accepts User objects 
     * 					   checks for edit rights else Error
     * @param List<sObject> newItems :  New instance of User Objct
	 * @param map<id, sObject> : Old map in trigger context
	 * @param map<Id, sObject> mapOldValues : New map in trigger context
     * @return Void
     * @throws Error : Using AddError() method of sObjects.
     */ 
	public static void validateUserCreds(List<sObject> newItems,
		map<id, sObject> mapNewValues, map<Id, sObject> mapOldValues)
	{
		//Get User Fields using MetaDataAPi
		map<String, Schema.SObjectField> mapFieldResult = Schema.SObjectType.User.fields.getMap();
		
		//Get API names of the fields which are allowed updates.
		map<String, CRM_CP_CPBC_Editable_User_Fields__c> mapCPBCEditableFields = CRM_CP_CPBC_Editable_User_Fields__c.getAll();
		
		//map to store Field API Name vs Label.
		map<String, string> mapFields = new map<String, String>();
		
		//List of checking langauge setting
		list<sObject> lstUser = new List<sObject>();
		
		//Check for Permission set
		if(getEligibilityForCPBC())
		{
			//Store the number of languages configured
			for(Schema.SObjectField field : mapFieldResult.Values())
			{
				//Store only Custom fields
				if(mapCPBCEditableFields != null 
					&& !mapCPBCEditableFields.keyset().isEmpty()
					&& !mapCPBCEditableFields.containsKey(field.getDescribe().getName()))
				{
					mapFields.put(field.getDescribe().getName(), field.getDescribe().getLabel());
				}     
			}
			
			if(mapFields != null && !mapFields.keyset().isEmpty())
			{  
				//Looping over User Object to check if fields modified Error.
				
 				for(sObject objSUser : newItems)
				{
					if(objSUser.Id != UserInfo.getUserId())
					objSUser.addError(system.Label.CPBC_User_Update_Error);
					
					boolean isEditableFieldsChange = false;
					boolean isNonEditableFieldsChange = false;
						
					for(string strFieldAPIName : mapFields.keyset())
					{ 
						if(objSUser.get(strFieldAPIName) <> mapOldValues.get(objSUser.Id).get(strFieldAPIName))
						{
                            system.debug('strFieldAPIName -> ' + mapOldValues.get(objSUser.Id).get(strFieldAPIName));
							isNonEditableFieldsChange = true;
							break;
						}
					}
					
					for(string strFieldAPIName : mapCPBCEditableFields.keyset())
					{ 
						if(objSUser.get(strFieldAPIName) <> mapOldValues.get(objSUser.Id).get(strFieldAPIName))
						{
							isEditableFieldsChange = true;
							break;
						}
					}
					if(!(isEditableFieldsChange && !isNonEditableFieldsChange)) 
						objSUser.addError(system.Label.CPBC_User_Update_Error);
				}
			}
		}
		else
		{
			List<Profile> lstProfile = [Select id, name
					from Profile Where Id = :UserInfo.getProfileId()];
			boolean boolIsAdminUser = false;
			if(lstProfile != null 
				&& !lstProfile.isEmpty()
				&& (lstProfile[0].Name == system.Label.CRM_CP_System_Administrator_Custom_Profile
					|| lstProfile[0].Name == system.Label.CRM_CP_System_Admin_Profile)
			) 
			{
				boolIsAdminUser = true;		
			}
			for(sObject objSUser : newItems)
			{
				for(Schema.SObjectField field : mapFieldResult.Values())
				{
					//Store only Custom fields
					if(!boolIsAdminUser 
						&& field.getDescribe().getName() != 'LanguageLocaleKey' 
					 	&& field.getDescribe().getName() != 'CRM_CP_Territory_Visit_Plan_Count__c')
					{
						string strAPIName = field.getDescribe().getName();
						
						if(objSUser.get(strAPIName) <> mapOldValues.get(objSUser.Id).get(strAPIName))
						objSUser.addError(system.Label.CPBC_User_Update_Error);
					}     
				}
				if(objSUser.get('LanguageLocaleKey') <> mapOldValues.get(objSUser.Id).get('LanguageLocaleKey'))
				lstUser.add(objSUser);			
			}
			if(lstUser != null && !lstUser.isEmpty()) checkForLanguageUpdate(lstUser);
		}
	}   
	/*
     * getEligibilityForCPBC : This method checks permission set assignment
     * @param : None
     * @return : Boolean
     * @throws : Nonad
     */ 
	private static boolean getEligibilityForCPBC()
	{
		List<PermissionSetAssignment> lstpermissionset = [SELECT PermissionSetId
                								FROM PermissionSetAssignment
                								WHERE AssigneeId = :UserInfo.getUserId()
                								AND permissionset.Name = :system.Label.CRM_CP_CPBC_User_Update_Permission];
        if(lstpermissionset != null
        	&& !lstpermissionset.isEmpty())
    	{
    		return true;
    	}              
    	return false;  								
	}
	
	/*
     * checkForLanguageUpdate : This method checks for langauges, if they are from allowed set of languages.
     * @param : None
     * @return : Void
     * @throws : Error 
     */ 
	public static void checkForLanguageUpdate(List<User> lstUser)
	{
		map<string, CRM_CP_AllowedLanguages__c> mapLanguage = CRM_CP_AllowedLanguages__c.getAll();
		for(Sobject objUser : lstUser)
		{
			if(!mapLanguage.containsKey(String.valueOf(objUser.get('LanguageLocaleKey'))))
			{	
				objUser.addError(system.Label.CPBC_User_Update_Error);
			}
		}  								
	}  
}