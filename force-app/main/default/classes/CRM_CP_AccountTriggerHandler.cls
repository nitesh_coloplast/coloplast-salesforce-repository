/**
* @author       : Nitesh Kumar
* @date         : 04/17/2018
* @description  : This is the AccountTriggerHandler class which will have all trigger event methods implmented and it is recommnended that all functionality
*                 of trigger should reside in this code of any utility method can be called from here.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Nitesh Kumar - Creted all the method as part of US-0693 and US-0556
* Version 1.1  : Nitesh Kumar - Removed the supress validation check from the IsDisabled() method.
* Version 1.2  : Mruga Shastri - Removed the Account -> Health Basics Sync logic from AfterUpdate() method into the Utility Class for US - 01349
* Version 1.3  : Anisa Shaikh - Added function calls in before insert and update for US-1728
* Version 1.4  : Anisa Shaikh - Added null check for TKT-002955
*/

public with sharing class CRM_CP_AccountTriggerHandler implements CRM_CP_TriggerInterface{

    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;

    /**
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will check whether the account trigger is disaled or not. The configuration is stored in Trigger Setting custom metadata.
    */
    public Boolean IsDisabled(){
        return CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserId()).CRM_CP_Triggers_Disabled__c;
    }

    /**
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records before inserting the data to the database(NOT commit)..
    * @param        : newItems - this will accepts the new reocords of account.
    */
    public void BeforeInsert(List<SObject> newItems) {


        List<sObject> preferredSpokenlist = new List<sObject>();
        List<sObject> preferredwrittenList = new List<sObject>();

        //Poulate the default spoken and written lanaguage

        for(sObject obj : newItems){

             if(Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CRM_CP_Consumer').getRecordTypeId() == obj.get('RecordTypeId')){

                 if(obj.get('CRM_CP_Preferred_Written_Language__pc') == null){
                    preferredwrittenList.add(obj);
                 }

                 if(obj.get('CRM_CP_PreferredSpoken_Lang__c') == null){
                    preferredSpokenlist.add(obj);
                 }
            }
        }

        //Poulate the default spoken lanaguage
         CRM_CP_TriggerHandlerUtilities.populateDefaultSpokenLanguage(preferredSpokenlist);

         //Poulate the default written lanaguage
         CRM_CP_TriggerHandlerUtilities.populateDefaultWrittenLanguage(preferredwrittenList);

            //Check custom setting to decide if the running user has permission to set the DOI trigger field (US-1728)
            boolean isSuccessful = CRM_CP_AccountTriggerHelper.updateDOITriggerOnInsert(newItems);
    }

    /**
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records before updating the data to the database(NOT commit)..
    * @param        : newItems - this will accepts the new reocords of account.
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {

         List<sObject> preferredSpokenlist = new List<sObject>();
         List<sObject> preferredwrittenList = new List<sObject>();

         EDQ.DataQualityService.SetValidationStatus(newItems.values(), oldItems.values(), Trigger.IsInsert, 2); //Line required to populate Validation timestamp for QAS
         for(sObject acc : newItems.values()){
             if(Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CRM_CP_Consumer').getRecordTypeId() == newItems.get((String)acc.get('Id')).get('RecordTypeId')){

                 if(newItems.get((String)acc.get('Id')).get('PersonIndividualId') != oldItems.get((String)acc.get('Id')).get('PersonIndividualId')
                    && newItems.get((String)acc.get('Id')).get('PersonIndividualId') == null){
                    acc.addError(Label.CRM_CP_Data_Privacy_Deletion_Message);
                 }

                 if(newItems.get((String)acc.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c') == null){
                    preferredSpokenlist.add(acc);
                 }
                 if(newItems.get((String)acc.get('Id')).get('CRM_CP_Preferred_Written_Language__pc') == null){
                    preferredwrittenList.add(acc);
                 }

                 if(newItems.get((String)acc.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c') != oldItems.get((String)acc.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c')
                    && newItems.get((String)acc.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c') == 'French'
                    && newItems.get((String)acc.get('Id')).get('CRM_CP_Owner_Company_Number__c') == '760'){

                    acc.put('CRM_CP_Preferred_Written_Language__pc','fr-CA');
                 }
                 else if(newItems.get((String)acc.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c') != oldItems.get((String)acc.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c')
                    && newItems.get((String)acc.get('Id')).get('CRM_CP_PreferredSpoken_Lang__c') != 'French'
                    && newItems.get((String)acc.get('Id')).get('CRM_CP_Owner_Company_Number__c') == '760'){

                    acc.put('CRM_CP_Preferred_Written_Language__pc','en-CA');
                 }
                 //Added OCN null check for TKT-002955
                 if(CRM_CP_Global_Settings__c.getOrgDefaults().CRM_CP_Double_Opt_In_Countries__c != null
                    && newItems.get((String)acc.get('Id')).get('CRM_CP_Owner_Company_Number__c') != null
                    && CRM_CP_Global_Settings__c.getOrgDefaults().CRM_CP_Double_Opt_In_Countries__c.contains((String)newItems.get((String)acc.get('Id')).get('CRM_CP_Owner_Company_Number__c'))){

                       if(newItems.get((String)acc.get('Id')).get('CRM_CP_ELQ_Email_Change_Status__pc') == '0'
                           && oldItems.get((String)acc.get('Id')).get('CRM_CP_ELQ_Email_Change_Status__pc') != '0'
                           && oldItems.get((String)acc.get('Id')).get('CRM_CP_ELQ_Email_Change_Status__pc') != null){
                               acc.put('CRM_CP_ELQ_Informational_Email_Perm__pc','Yes - to be confirmed');
                               acc.put('CRM_CP_ELQ_Promotional_Email_Permission__pc','Yes - to be confirmed');
                       }
                 }
             }

         }

         //Poulate the default spoken lanaguage
         CRM_CP_TriggerHandlerUtilities.populateDefaultSpokenLanguage(preferredSpokenlist);

         //Poulate the default spoken lanaguage
         CRM_CP_TriggerHandlerUtilities.populateDefaultWrittenLanguage(preferredwrittenList);

        //Check custom setting to decide if the running user has permission to set the DOI trigger field (US-1728)
        boolean isSuccessful = CRM_CP_AccountTriggerHelper.updateDOITriggerOnUpdate(newItems, oldItems);
    }

    /**
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records before deleting the data from the database(NOT commit)..
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    /**
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records after inseting the data from the database(NOT commit).
    * @param        : newItems - this will accepts the new reocords of account.
    */
    public void AfterInsert(Map<Id, SObject> newItems) {
        //Call the generic mapping utility to map the Contact with the corresponding individuals

        Map<Id, SObject> finalNewItems = new Map<Id, SObject>();
        List<Id> convertedDP = new List<Id>();

        for(sObject acc : newItems.values()){
            if(acc.get('CRM_CP_Is_Converted_from_Lead__pc') == false &&
                Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CRM_CP_Consumer').getRecordTypeId() == acc.get('RecordTypeId')){
                finalNewItems.put((Id)acc.get('Id'),acc);
            }
            else if(acc.get('CRM_CP_Is_Converted_from_Lead__pc') == true &&
                Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CRM_CP_Consumer').getRecordTypeId() == acc.get('RecordTypeId')){
                convertedDP.add((string)acc.get('PersonIndividualId'));
            }
        }
        if(!finalNewItems.isEmpty()) {
            CRM_CP_TriggerHandlerUtilities.mapIndividuals(finalNewItems,null);
        }

        if(!convertedDP.isEmpty()){
            List<Individual> lstInd = [SELECT CRM_CP_Data_Privacy_Origin__c from Individual where Id IN :convertedDP];
            for(Individual ind : lstInd){
                ind.CRM_CP_Data_Privacy_Origin__c = 'Consumer';
            }
            if(!lstInd.isEmpty()){
                Database.SaveResult[] dpList = Database.update(lstInd, false);
                ExceptionHelper.processSaveResults(dpList,'CRM_CP_AccountTriggerHandler','AfterUpdate');
            }
        }
    }

    /**
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records after updating the data from the database(NOT commit).
    * @param        : oldItems - this will accepts the old reocords of account.
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        //Check the trigger recursion for account
        if(CRM_CP_TriggerRecursionHandler.Account_runOnce()){

        Map<Id, SObject> accIds = new Map<Id, SObject> ();

        //Create Update Health Basics based on Account Proxies

        List<CRM_CP_Eloqua_Child_Sync__mdt> lstAccountHBFieldMapping = new List<CRM_CP_Eloqua_Child_Sync__mdt>();
        Boolean runHealthBasicsLogic = FALSE;
        Set<String> setOutcomeValues = new Set<String>();
        Set<Id> setConsumerIds = new Set<Id>();
        Set<Account> setAccountOutcomeUpdate = new Set<Account>();
        Map<Id, CRM_CP_AccountHBTrigger_Wrapper> mapAccountIdRelatedOutcomeWrapper = new Map<Id, CRM_CP_AccountHBTrigger_Wrapper>();
        CRM_CP_AccountHBTrigger_Wrapper accountHBwrapper;

        //Get all the Field Mapping metadata records for Account -> HB Sync

        lstAccountHBFieldMapping = [Select Id, Label, CRM_CP_Object_API__c, CRM_CP_ELQ_Proxy_API__c,
                                        CRM_CP_Health_Basic_API__c, CRM_CP_is_Field_Mapping__c, CRM_CP_is_Stoma_Mapping__c,
                                        CRM_CP_Health_Basics_Outcome__c
                                    From CRM_CP_Eloqua_Child_Sync__mdt
                                    Where CRM_CP_Object_API__c = 'Account'
                                    AND CRM_CP_Record_Type__c = 'Consumer'
                                    AND CRM_CP_is_Field_Mapping__c = TRUE
                                    AND CRM_CP_Health_Basic_API__c != ''];

        //Loop through new records to find records where Eloqua HB proxies are updated.
        for(sObject consumer : newItems.values()){
            if(Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CRM_CP_Consumer').getRecordTypeId() == newItems.get((String)consumer.get('Id')).get('RecordTypeId') &&
                newItems.get((String)consumer.get('Id')).get('CRM_CP_Contact_Status__pc') == 'Active' &&
                newItems.get((String)consumer.get('Id')).get('CRM_CP_Update_Health_Basic__pc') == false){

                if(lstAccountHBFieldMapping.size() > 0 && !lstAccountHBFieldMapping.isEmpty()){
                    for(CRM_CP_Eloqua_Child_Sync__mdt accHBMapping : lstAccountHBFieldMapping){
                        String consumerFieldAPI = accHBMapping.CRM_CP_ELQ_Proxy_API__c;
                        if(newItems.get((String)consumer.get('Id')).get(consumerFieldAPI) != oldItems.get((String)consumer.get('Id')).get(consumerFieldAPI)){
                            runHealthBasicsLogic = TRUE;
                            if(!mapAccountIdRelatedOutcomeWrapper.isEmpty() && mapAccountIdRelatedOutcomeWrapper.containsKey((Id)consumer.get('Id'))){
                                accountHBwrapper = mapAccountIdRelatedOutcomeWrapper.get((Id)consumer.get('Id'));
                            }else{
                                accountHBwrapper = new CRM_CP_AccountHBTrigger_Wrapper((Account)consumer);
                            }

                             if(accHBMapping.CRM_CP_is_Stoma_Mapping__c){
                                    if(accHBMapping.CRM_CP_Health_Basics_Outcome__c == 'Colostomy'){
                                        accountHBwrapper.isColostomyChanged = TRUE;
                                    }else if(accHBMapping.CRM_CP_Health_Basics_Outcome__c == 'Continent Urostomy (Mitrofanoff)'){
                                        accountHBwrapper.isContinentUrostomyChanged = TRUE;
                                    }else if(accHBMapping.CRM_CP_Health_Basics_Outcome__c == 'Ileostomy'){
                                        accountHBwrapper.isIleostomyChanged = TRUE;
                                    }else if(accHBMapping.CRM_CP_Health_Basics_Outcome__c == 'Jejunostomy'){
                                        accountHBwrapper.isJejunostomyChanged = TRUE;
                                    }else if(accHBMapping.CRM_CP_Health_Basics_Outcome__c == 'Nephrostomy'){
                                        accountHBwrapper.isNephrostomyChanged = TRUE;
                                    }else if(accHBMapping.CRM_CP_Health_Basics_Outcome__c == 'Urostomy'){
                                        accountHBwrapper.isUrostomyChanged = TRUE;
                                    }
                                }
                                if(accHBMapping.CRM_CP_ELQ_Proxy_API__c == 'CRM_CP_ELQ_Temporary_Stoma__pc'){
                                   accountHBwrapper.isStomaStatusChanged = TRUE;
                                }else if(accHBMapping.CRM_CP_ELQ_Proxy_API__c =='CRM_CP_ELQ_Surgery_Date__pc'){
                                   accountHBwrapper.isSurgeryDateChanged = TRUE;
                                }else if(accHBMapping.CRM_CP_ELQ_Proxy_API__c == 'CRM_CP_ELQ_Wheelchair__pc'){
                                    accountHBwrapper.isWheelChairChanged = TRUE;
                                }
                            mapAccountIdRelatedOutcomeWrapper.put((Id)newItems.get((String)consumer.get('Id')).get('Id'), accountHBwrapper );
                            setConsumerIds.add((Id)newItems.get((String)consumer.get('Id')).get('Id'));
                            setAccountOutcomeUpdate.add((Account)consumer);
                        }
                        if(accHBMapping.CRM_CP_Health_Basics_Outcome__c != '' && accHBMapping.CRM_CP_Health_Basics_Outcome__c != null){
                            setOutcomeValues.add(accHBMapping.CRM_CP_Health_Basics_Outcome__c);
                        }
                    }
                }
            }

            if(newItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Consent__pc') != oldItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Consent__pc')
                || newItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Email_Change_Status__pc') != oldItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Email_Change_Status__pc')
                || newItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Letter__pc') != oldItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Letter__pc')
                || newItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Phone__pc') != oldItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Phone__pc')
                || newItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_SMS__pc') != oldItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_SMS__pc')
                || newItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Latest_Consent_Date__pc') != oldItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Latest_Consent_Date__pc')
                || newItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Promotional_Email_Permission__pc') != oldItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Promotional_Email_Permission__pc')
                || newItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Informational_Email_Perm__pc') != oldItems.get((String)consumer.get('Id')).get('CRM_CP_ELQ_Informational_Email_Perm__pc')){

            accIds.put((String)consumer.get('Id'),consumer);
        }

        }
        if(runHealthBasicsLogic){
           CRM_CP_TriggerHandlerUtilities.ConsumerHealthBasicSync(lstAccountHBFieldMapping, newItems, oldItems,mapAccountIdRelatedOutcomeWrapper, setOutcomeValues,
                                                                  setConsumerIds,
                                                                  setAccountOutcomeUpdate);
        }



        if(!accIds.isEmpty() && CRM_CP_TriggerRecursionHandler.dataPrivacyUpdates == false){
            CRM_CP_TriggerHandlerUtilities.updateDataPrivaciesFromAccount(accIds, oldItems);
        }

      }
    }
    /**
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records after deleting the data from the database(NOT commit).
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void AfterDelete(Map<Id, SObject> oldItems) {}

    /**
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : This method will process all the records after undeleting the data from the database(NOT commit).
    * @param        : oldItems - this will accepts the old reocords of account.
    */
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}