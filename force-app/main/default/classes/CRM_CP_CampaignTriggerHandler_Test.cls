/*
* CRM_CP_CampaignTriggerHandler_Test: This class is test class for CRM_CP_CampaignTriggerHandler
* SeeAllData true is kept as True because the class query for default statuses 'Sent' and 'Responded' which
* appears when the triggers run but comes blank when run it test without SeeAllData kept as True.
* Also SeeAllData true will not affect as set of Ids are passed to process and only takes data what is passed from 
* test class.
* Created Date : 10-April-2018
* 
*/

@isTest(SeeAllData=True)
private class CRM_CP_CampaignTriggerHandler_Test{
    
    static testMethod void insertCampaignForRecordTypeCampaign1stData()
    {
        RecordType recType = [SELECT Id, Name FROM RecordType WHERE Name = 'Event'];
        List <Campaign> listOfCampaign = new List <Campaign>();
        for(Integer i=0;i<300;i++){
            Campaign camp = new Campaign(Name = 'Test Camp1'+i,CRM_CP_Campaign_Initiated_By__c ='Global',CRM_CP_Campaign_Purpose__c='Educational',
                        CRM_CP_Business_Area__c='WSC',CRM_CP_Overall_Target_Audience__c='Consumer',RecordTypeId=recType.Id);
            listOfCampaign.add(camp);                
        }                
        INSERT listOfCampaign;
        
        List <CampaignMemberStatus> listCampaignMemberStatus = [Select Id,SortOrder, Label FROM CampaignMemberStatus WHERE CampaignId =: listOfCampaign[0].Id];
        List<CRM_CP_CampaignMemberStatus__mdt> listCampaignMemberStatusMetadata = [Select Id from CRM_CP_CampaignMemberStatus__mdt WHERE CRM_CP_Record_Type__c ='Event'];
        
        System.assertEquals(listCampaignMemberStatus.size(),listCampaignMemberStatusMetadata.size());
        
        List <CampaignMemberStatus> listStatuses = new List <CampaignMemberStatus>();
        CampaignMemberStatus cmpStatus1 = new CampaignMemberStatus(CampaignId = listOfCampaign[0].Id,HasResponded = False,Label = 'Sent1',
                                                                                           SortOrder=31,IsDefault=True);
        CampaignMemberStatus cmpStatus2 = new CampaignMemberStatus(CampaignId = listOfCampaign[0].Id,HasResponded = True,Label = 'Responded1',
                                                                                           SortOrder=32,IsDefault=False);
        CampaignMemberStatus cmpStatus3 = new CampaignMemberStatus(CampaignId = listOfCampaign[0].Id,HasResponded = False,Label = 'Responded1',
                                                                                           SortOrder=33,IsDefault=True);                                                                                   
        listStatuses.add(cmpStatus1);
        listStatuses.add(cmpStatus2);
        listStatuses.add(cmpStatus3);
        insert listStatuses; 
        
        listOfCampaign[0].Name='Test Camp2';
        update listOfCampaign;
        delete listOfCampaign[0];
        undelete listOfCampaign[0];
        
     }
     
    static testMethod void insertCampaignForRecordTypeCampaign1stData_Eloqua()
    {
        RecordType recType = [SELECT Id, Name FROM RecordType WHERE Name = 'Campaign'];
        
        List <Campaign> listOfCampaign = new List <Campaign>();
        for(Integer i=0;i<300;i++){
            Campaign camp = new Campaign(Name = 'Test Camp2'+i,CRM_CP_Campaign_Initiated_By__c ='Global',CRM_CP_Campaign_Purpose__c='Educational',
                        CRM_CP_Business_Area__c='WSC',CRM_CP_Overall_Target_Audience__c='Consumer',RecordTypeId=recType.Id,
                        CRM_CP_Eloqua_Integration__c = True,CRM_CP_Coloplast_Programme__c = 'Coloplast Care');
            listOfCampaign.add(camp);
        }
        
        INSERT listOfCampaign;
        
        List <CampaignMemberStatus> listCampaignMemberStatus = [Select Id,SortOrder, Label FROM CampaignMemberStatus WHERE CampaignId =: listOfCampaign[0].Id];
        
        List<CRM_CP_CampaignMemberStatus__mdt> listCampaignMemberStatusMetadata = [Select Id from CRM_CP_CampaignMemberStatus__mdt WHERE CRM_CP_Eloqua_Integration__c = True AND CRM_CP_Record_Type__c ='Campaign'];
        
        System.assertEquals(listCampaignMemberStatus.size(),listCampaignMemberStatusMetadata.size());
        
        List <CampaignMemberStatus> listStatuses = new List <CampaignMemberStatus>();
        CampaignMemberStatus cmpStatus1 = new CampaignMemberStatus(CampaignId = listOfCampaign[0].Id,HasResponded = False,Label = 'Sent1',
                                                                                           SortOrder=31,IsDefault=True);
        CampaignMemberStatus cmpStatus2 = new CampaignMemberStatus(CampaignId = listOfCampaign[0].Id,HasResponded = True,Label = 'Responded1',
                                                                                           SortOrder=32,IsDefault=False);
        CampaignMemberStatus cmpStatus3 = new CampaignMemberStatus(CampaignId = listOfCampaign[0].Id,HasResponded = False,Label = 'Responded1',
                                                                                           SortOrder=33,IsDefault=True);                                                                                   
        listStatuses.add(cmpStatus1);
        listStatuses.add(cmpStatus2);
        listStatuses.add(cmpStatus3);
        insert listStatuses;

        
     }
    
}