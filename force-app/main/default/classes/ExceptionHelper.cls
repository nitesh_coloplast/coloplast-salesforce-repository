/*********************************************************************************
* @author         Anisa Shaikh
* @description    Utility class error logging
* @date         2018-02-23
* @group          Common Libraries for error logging
* @test class     ExceptionHelperTest
**********************************************************************************/

public class ExceptionHelper {
  
     
    /*****************************************************************************
  * @author   Anisa Shaikh
  * @date     2018-02-23
  * @description  Creates the error object
  * @param    exceptionToLog (System.Exception): exception object 
  * @param    sClassName (String): name of the class where error/exception occurred
  * @param    sMethodName (String): name of the method where error/exception occurred
  * @return   List < CRM_CP_Error_Log__c >: list of error records created
    *******************************************************************************/
     
    public static List < CRM_CP_Error_Log__c > logErrors(System.Exception exceptionToLog, 
                                 String sClassName, 
                                 String sMethodName){
                              
        List < CRM_CP_Error_Log__c > listErrorLogs = new List < CRM_CP_Error_Log__c > ();
        String sExceptionType;
        //Null check on exception object
        if (exceptionToLog != null){
            CRM_CP_Error_Log__c errorLog = new CRM_CP_Error_Log__c();
            errorLog.CRM_CP_Error_Message__c = exceptionToLog.getTypeName();
            errorLog.CRM_CP_Method_Name__c = sMethodName;
            errorLog.CRM_CP_Class_Name__c = sClassName;
            errorLog.CRM_CP_Error_Location__c = exceptionToLog.getStackTraceString();
            listErrorlogs.add(errorLog);
        }
        //Insert all the logs
        if(listErrorLogs != null && !listErrorLogs.isEmpty()){
          saveLog(listErrorLogs);
        }
        return listErrorLogs;
    }
    
    /*****************************************************************************
    * @author   Anisa Shaikh
    * @date     2018-02-23
    * @description  Commits the list of error records to database
    * @param    logsToSave (List < CRM_CP_Error_Log__c >): list of error records
    *******************************************************************************/   

    public static void saveLog(List < CRM_CP_Error_Log__c > logsToSave){
        
        if (logsToSave != null && !logsToSave.isEmpty()) {
            List < Database.SaveResult > lstSaveResults = Database.insert(logsToSave, false);
            List < ID > lstErrorCodeIDs = new List < ID > ();
            for (Database.SaveResult saveResult: lstSaveResults){
                lstErrorCodeIDs.add(saveResult.getID());
            }
        }
    }

   /** 
   * @author       : Nitesh Kumar
   * @date         : 04/18/2018
   * @description  : This method inserts the error records log.
   * @param        : dataList - this will accept the save result
   * @param        : sClassName - this will accept the class name
   * @param        : sMethodName - this will accept the method name.
   */
    public static void processSaveResults(Database.SaveResult[] dataList, String sMethodName, String sClassName){
           
           List < CRM_CP_Error_Log__c > listErrorLogs = new List < CRM_CP_Error_Log__c > ();
           String errorMessage = '';
           for (Database.SaveResult sr : dataList) {
                  if (sr.isSuccess()) {
                    continue;
                  }
                  // Operation failed, so get all errors                
                  for(Database.Error err : sr.getErrors()) {
                      CRM_CP_Error_Log__c errorLog = new CRM_CP_Error_Log__c();
                      errorLog.CRM_CP_Error_Message__c = err.getMessage() + ',  Fields : ' + err.getFields();
                      errorLog.CRM_CP_Method_Name__c = sMethodName;
                      errorLog.CRM_CP_Class_Name__c = sClassName;
                      listErrorlogs.add(errorLog);
                      errorMessage += err.getMessage();
                  }
            }  
            
            if(!listErrorLogs.isEmpty()) {
                Database.insert(listErrorLogs, false);
                if(!CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserId()).CRM_CP_Disable_DOI_Trigger__c){
                    Trigger.new[0].addError(errorMessage);
                }
            }
       } 
   /** 
   * @author       : Mruga Shastri
   * @date         : 14-09-2018
   * @description  : This method inserts the error records log.
   * @param        : upsertList- this will accept the upsert result List
   * @param        : sClassName - this will accept the class name
   * @param        : sMethodName - this will accept the method name.
   */
    public static void processUpsertResults(Database.UpsertResult[] upsertList, String sMethodName, String sClassName){
           
           List < CRM_CP_Error_Log__c > listErrorLogs = new List < CRM_CP_Error_Log__c > ();
           CRM_CP_Error_Log__c errorLog;
           String errorMessage = '';
           for (Database.UpsertResult sr: upsertList) {
                  if (sr.isSuccess()) {
                    continue;
                  }
                  // Operation failed, so get all errors                
                  for(Database.Error err : sr.getErrors()) {
                      errorLog = new CRM_CP_Error_Log__c();
                      errorLog.CRM_CP_Error_Message__c = err.getMessage() + ',  Fields : ' + err.getFields();
                      errorLog.CRM_CP_Method_Name__c = sMethodName;
                      errorLog.CRM_CP_Class_Name__c = sClassName;
                      listErrorlogs.add(errorLog);
                      errorMessage += err.getMessage();
                  }
            }  
            
            if(!listErrorLogs.isEmpty()) {
                Database.insert(listErrorLogs, false);
                if(!CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserId()).CRM_CP_Disable_DOI_Trigger__c){
                    Trigger.new[0].addError(errorMessage);
                }
            }
       }   
}