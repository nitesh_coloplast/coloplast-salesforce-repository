/**
* @author   	 : Anisa Shaikh
* @date 		 : 08/20/2018
* @description 	 : This class is controller to the custom delegated logins report
* ------------------------------------------ Version Control ---------------------------------------------------
* Version 1.0    : Anisa Shaikh - Original (US-1838)
*/

public class CRM_CP_DelegatedLoginReportController {
	
	private static Map<String, String> mapUsernameToOCN;
	private static Map<String, CRM_CP_LoginReportWrapper> mapUsernameToRecord;
	
    @AuraEnabled
    public static List<SetupAuditTrail> getReportDetails() {
        return [SELECT Action, DelegateUser, Display, Section, CreatedDate, CreatedBy.Name, 
        			   CreatedBy.CRM_CP_Subsidiary_Number__C 
        	    FROM SetupAuditTrail where DelegateUser != ''];
    }	
	

    @AuraEnabled
    public static List<CRM_CP_LoginReportWrapper> getReport() {
    	List<CRM_CP_LoginReportWrapper> wrapperList = new List<CRM_CP_LoginReportWrapper>();
    	mapUsernameToOCN =  new Map<String, String>();
    	mapUsernameToRecord = new Map<String, CRM_CP_LoginReportWrapper>();
    	List<SetupAuditTrail> lstReportRecords = [SELECT Action, DelegateUser, Display, Section, 
    													 CreatedDate, CreatedBy.UserName, 
        			   									 CreatedBy.CRM_CP_Subsidiary_Number__C 
        	    								  FROM SetupAuditTrail 
        	    								  WHERE DelegateUser != ''
        	    								  ORDER BY CreatedDate];
        CRM_CP_LoginReportWrapper wrapper;	    								  
        if(lstReportRecords != null && !lstReportRecords.isEmpty()){
        	for(SetupAuditTrail auditTrailRecord: lstReportRecords){
        		wrapper= new CRM_CP_LoginReportWrapper(auditTrailRecord);
        		mapUsernameToOCN.put(wrapper.strDelegatedUserName,'');
        		mapUsernameToRecord.put(wrapper.strDelegatedUserName,wrapper);
        		System.debug('wrapper '+wrapper);
        		wrapperList.add(wrapper);
        	}
        }
        
        return populateDelegatedUserOCN(wrapperList);
    }
    
	private static List<CRM_CP_LoginReportWrapper> populateDelegatedUserOCN(List<CRM_CP_LoginReportWrapper> lstReport){
		
		List<User> lstUsers	= [Select Id, CRM_CP_Subsidiary_Number__C,Username
		                       from User 
		                       where Username IN :mapUsernameToOCN.keySet()];	
	   if(lstUsers == null || lstUsers.isEmpty()){
	   		return lstReport;
	   }
	   
	   for(User userObj: lstUsers){
	   		mapUsernameToOCN.put(userObj.Username,userObj.CRM_CP_Subsidiary_Number__C);
	   		
	   }
	   for(CRM_CP_LoginReportWrapper wrapper: lstReport){	
	   		wrapper.strDelegatedUserOCN = mapUsernameToOCN.get(wrapper.strDelegatedUserName);
	   }
	   return lstReport;
	}
	
}