/**
* @author   	: Ninad Patil
* @date 		: 03 Sep 2018
* @description 	: This class is Test class for Pricebook Allocation Functionality on Opportunity and order.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Ninad Patil - 
*/
@isTest
private class CRM_CP_PricebookAllocationFunTest {
	
	/** 
	* @author       : Ninad Patil
	* @date         : 03 Sep 2018
	* @description  : This method creates data in the system before test methods are executed
	*/
    @testSetup static void setup() 
    {
        List<String> lstOCNs= new List<String>();
		Schema.DescribeFieldResult lstSubsidiaryNumberPLE = User.CRM_CP_Subsidiary_Number__c.getDescribe();
		List<Schema.PicklistEntry> lstSubsidiaryNUmber = lstSubsidiaryNumberPLE.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : lstSubsidiaryNUmber)
		{
			lstOCNs.add(pickListVal.getValue());
		}     
        
        List<String> lstPricebookType= new List<String>();
		Schema.DescribeFieldResult lstPBTypePLE = Pricebook2.CRM_CP_pricebook_Type__c.getDescribe();
		List<Schema.PicklistEntry> lstPricebookTypeENtry = lstPBTypePLE.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : lstPricebookTypeENtry)
		{
			lstPricebookType.add(pickListVal.getValue());
		}  
        
        List<pricebook2> lstPricebook = new List<Pricebook2>();
        for(String strOCN : lstOCNs)
        {
        	//Create Pricebooks
        	for(String strPBType : lstPricebookType)
        	{
	        	Pricebook2 objPBSR = CRM_CP_TestDataUtility.createPriceBook
	        					(strPBType , 
    							strOCN);
    			lstPricebook.add(objPBSR);
        	}
        }
        if(lstPricebook != null
        	&& !lstPricebook.isEmpty()) insert lstPricebook;
        	
        List<Account> lstAcc = new List<Account>();
        for(String strOCN : lstOCNs)
        {     
        	if(strOCN != '110' && strOCN != '555')
        	{
        		//Create Person Account
        		Account objAcc = CRM_CP_TestDataUtility.createAccountRecTypeConsumer('ConsumerLastName');
        		objAcc.CRM_CP_Owner_Company_Number__c = strOCN;
        		objAcc.CRM_CP_Owner_Company_Number__pc = strOCN;
        		lstAcc.add(objAcc);
        	}
        }
        for(String strOCN : lstOCNs)
        {     
        	if(strOCN != '110' && strOCN != '555')
        	{
        		//Create Person Account
        		Account objAcc = CRM_CP_TestDataUtility.createAccountRecTypeAcc('AccountRecType');
        		objAcc.CRM_CP_Owner_Company_Number__c = strOCN;
        		lstAcc.add(objAcc);
        	}
        }
        
        insert lstAcc;
        
        Campaign objCamp = CRM_CP_TestDataUtility.createConsumerCampaign('Test_Campaign');
        insert objCamp;
        
    }
    /** 
	* @author       : Ninad Patil
	* @date         : 03 Sep 2018
	* @description  : This method test pricebook allocation on Order object.
	*/
    @isTest static void testOrderCreation() 
    {
    	List<String> lstOCNs= new List<String>();
		Schema.DescribeFieldResult lstSubsidiaryNumberPLE = User.CRM_CP_Subsidiary_Number__c.getDescribe();
		List<Schema.PicklistEntry> lstSubsidiaryNUmber = lstSubsidiaryNumberPLE.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : lstSubsidiaryNUmber)
		{
			lstOCNs.add(pickListVal.getValue());
		}     
        
        List<String> lstPricebookType= new List<String>();
		Schema.DescribeFieldResult lstPBTypePLE = Pricebook2.CRM_CP_pricebook_Type__c.getDescribe();
		List<Schema.PicklistEntry> lstPricebookTypeENtry = lstPBTypePLE.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : lstPricebookTypeENtry)
		{
			lstPricebookType.add(pickListVal.getValue());
		} 
		
		Map<String, Account> mapAccOCN = new Map<String, Account>();
		for(Account objAcc : [Select id, name,CRM_CP_Owner_Company_Number__C from 
			Account WHere CRM_CP_Owner_Company_Number__C IN :lstOCNs
			And name like '%ConsumerLastName%'])
		{
			mapAccOCN.put(objAcc.CRM_CP_Owner_Company_Number__C , objAcc);
		}
		List<Order> lstOrder = new List<Order>();
		
		List<Campaign> lstCampaign = [Select id, name from campaign Where name = 'Test_Campaign'];
    	Test.startTest();
        
		for(String strOCN : lstOCNs)
		{
			for(string strPricebook : lstPricebookType)
			{
				if(mapAccOCN.containsKey(strOCN))
				{
					Order objOrder = CRM_CP_TestDataUtility.createOrder(mapAccOCN.get(strOCN));
			        objOrder.CRM_CP_Subsidiary_Number__c = strOCN;
			        objOrder.CRM_CP_Create_Contact_Protocol__c = true;
			        if(lstCampaign != null && !lstCampaign.isEmpty()) objOrder.CRM_CP_Campaign_Referen__c = lstCampaign[0].Id;
			        lstOrder.add(objOrder);
				}				
			}
		}
		if(lstOrder != null 
			&& !lstOrder.isEMpty()) insert lstOrder;
		
		list<Id> lstOrderIds = new List<Id>();
		for(order objOrder : lstOrder)
		{
			lstOrderIds.add(objOrder.Id);	
		}
        
        for(order objOrder : [Select id, name, Pricebook2Id from Order where Id in : lstOrderIds])
        {
        	system.assert(objOrder.Pricebook2Id != null);
        }       
        
        update lstOrder;
        Test.stopTest();            
    }
 	
 	/** 
	* @author       : Ninad Patil
	* @date         : 03 Sep 2018
	* @description  : This method test allocation of Pricebook on Opportunity.
	*/
    @isTest static void testOpportunityCreation() 
    {
    	List<String> lstOCNs= new List<String>();
		Schema.DescribeFieldResult lstSubsidiaryNumberPLE = User.CRM_CP_Subsidiary_Number__c.getDescribe();
		List<Schema.PicklistEntry> lstSubsidiaryNUmber = lstSubsidiaryNumberPLE.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : lstSubsidiaryNUmber)
		{
			lstOCNs.add(pickListVal.getValue());
		}     
        
        List<String> lstPricebookType= new List<String>();
		Schema.DescribeFieldResult lstPBTypePLE = Pricebook2.CRM_CP_pricebook_Type__c.getDescribe();
		List<Schema.PicklistEntry> lstPricebookTypeENtry = lstPBTypePLE.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : lstPricebookTypeENtry)
		{
			lstPricebookType.add(pickListVal.getValue());
		} 
		
		Map<String, Account> mapAccOCN = new Map<String, Account>();
		for(Account objAcc : [Select id, name,CRM_CP_Owner_Company_Number__C 
				from Account WHere CRM_CP_Owner_Company_Number__C IN :lstOCNs
				And name like '%AccountRecType%'])
		{
			mapAccOCN.put(objAcc.CRM_CP_Owner_Company_Number__C , objAcc);
		}
		List<Opportunity> lstOpportunity = new List<Opportunity>();
    	Test.startTest();
        
		for(String strOCN : lstOCNs)
		{
			for(string strPricebook : lstPricebookType)
			{
				if(mapAccOCN.containsKey(strOCN))
				{
					Opportunity objOpportunity = CRM_CP_TestDataUtility.createOpportunity(mapAccOCN.get(strOCN), 
    									'Opportunity' + '-' + strOCN,
    									system.today() + 1,
    									'Draft' );
			        
			        lstOpportunity.add(objOpportunity);
				}				
			}
		}
		if(lstOpportunity != null 
			&& !lstOpportunity.isEMpty()) insert lstOpportunity;
		
		list<Id> lstlstOpportunityIds = new List<Id>();
		for(Opportunity objOpportunity : lstOpportunity)
		{
			lstlstOpportunityIds.add(objOpportunity.Id);	
		}
        
        for(Opportunity objOpportunity : [Select id, name, Pricebook2Id from Opportunity where Id in : lstlstOpportunityIds])
        {
        	system.assert(objOpportunity.Pricebook2Id != null);
        }       
        
        Test.stopTest();            
    }
}