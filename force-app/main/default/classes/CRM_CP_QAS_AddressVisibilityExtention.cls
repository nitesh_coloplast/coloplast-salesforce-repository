/*
* CRM_CP_QASExtention : This class acts as an extension for <Object>EnforceUsage visualforce pages to render itself.
* Created Date : 04-SEP-2018
* Test Class: CRM_CP_UserUpdateTEST
*/
public with sharing class CRM_CP_QAS_AddressVisibilityExtention{
    public CRM_CP_QAS_AddressVisibilityExtention(ApexPages.StandardController controller) {
    }
    
    /*
    * getIsQASVisibleForLoggedInUser : This method renders QAS popup validation box on detail record page based on Permission set. 
    * @return Boolean
    * @throws None
    */
    public Boolean getIsQASVisibleForLoggedInUser(){
        try{
            List<PermissionSetAssignment> lstcurrentUserPerSet = [SELECT Id FROM PermissionSetAssignment WHERE AssigneeId = :Userinfo.getUserId() AND 
                                                                  PermissionSet.Name =: Label.CRM_CP_QAS_AddressValidation_PermissionSet];
            if(!lstcurrentUserPerSet.IsEmpty())
                return true;
            else
                return false;
        }
        catch(Exception ex){return false;}
        
    }
}