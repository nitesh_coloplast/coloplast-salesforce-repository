/*
 * CRM_CP_PicklistValue : This class is wrapper class to 
 	convey information between Lightning component to Apex code.
 *
 * Created Date : 16 - March - 2018
 * 
 */
public class CRM_CP_PicklistValue
{
	@AuraEnabled
	public string strLabel;
	@AuraEnabled
	public string strValue;
	@AuraEnabled
	public CRM_CP_Product_Usage__c objProductUsage;
	
}