/*
 * CRM_CP_SubmitOrderResponse : This class is wrapper class to 
 	convey information between Lightning component to Apex code.
 *
 * Created Date : 16 - March - 2018
 * 
 */
public class CRM_CP_SubmitOrderResponse
{
	@AuraEnabled
	public boolean boolIsConsumer = false;
	@AuraEnabled
	public string strobjAPI = '';
	@AuraEnabled
	public boolean boolShowButton = true;
	@AuraEnabled
	public boolean boolShowProdUsageModal = false;
	@AuraEnabled
	public boolean boolEligbleProdUsageExists = false;
	@AuraEnabled
	public Order objOrder;
	@AuraEnabled
	public boolean boolIsError = false;
	@AuraEnabled
	public string strErrMsg = '';
	@AuraEnabled
	public List<string> lstErrRec;
	@AuraEnabled
	public string strSucMsg = '';
	public Case objCase;
	
	@AuraEnabled
	public string strCaseId = '';
	@AuraEnabled
	public map<Id, orderitem> mapOrderItem;
	
	@AuraEnabled
	public List<CRM_CP_Product_Usage__c> lstProdUsage;
	@AuraEnabled
	public list<CRM_CP_OrderProductUsageWrapper> lstOrderProductWrapper;
	
	@AuraEnabled
	public list<CRM_CP_PicklistValue> lstPicklistValue;
	@AuraEnabled
	public list<CRM_CP_PicklistValue> lstProductUsagePicklist;
	
	@AuraEnabled
	public Case objCaseContactProtocol;
}