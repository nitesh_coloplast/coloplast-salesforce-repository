public with sharing class CRM_CP_ContentDocumentLinkTriggerHandler implements CRM_CP_TriggerInterface {

// Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /** 
    * @author       : Prity Kumari
    * @date         : 02/15/2019
    * @description  : This method will check whether the CRM_CP_ContentDocumentTrigger is disabled or not. The configuration is stored in Trigger Setting custom metadata.
    */
    public Boolean IsDisabled(){
        return CRM_CP_Trigger_Configurations__c.getInstance(UserInfo.getUserId()).CRM_CP_Triggers_Disabled__c;
    }

    /** 
    * @author       : Prity Kumari
    * @date         : 02/15/2019
    * @description  : This method will process all the records after updating the data to the database.
    * @param        : newItems - this will accepts the new reocords of account.
    */
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}    
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration of after delete events.
    */
    public void BeforeDelete(Map<Id,SObject> oldItems){}
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration of Beore Insert events.
    */
    public void BeforeInsert(List<SObject> newItems){}
 
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration of before update events.
    */
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){}
 
   
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration of after insert events.
    */
    public void AfterInsert(Map<Id, SObject> newItems){
        CRM_CP_TriggerHandlerUtilities.populateAttachmentLastModifiedDate(newItems.values());
    }
    
     
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration of after delete events.
    */
    public void AfterDelete(Map<Id, SObject> oldItems){}
    
    /** 
    * @author       : Nitesh Kumar
    * @date         : 04/17/2018
    * @description  : Method declaration of after Undelete events.
    */
    public void AfterUndelete(Map<Id, SObject> oldItems){}   
}