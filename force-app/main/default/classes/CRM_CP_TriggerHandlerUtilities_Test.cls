@isTest(seeAllData=false)
public class CRM_CP_TriggerHandlerUtilities_Test {
        
     @testSetup static void setup() {
        
        
        List<Account> lstAccount = new List<Account>();
        List<Contact> lstContact = new List<Contact>();
        List<Lead> lstLead = new List<Lead>();
        
        
        String AccountRecordTypeId, ContactRecordTypeId, LeadRecordTypeId;

        AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
        ContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Professional Contact').getRecordTypeId();
        LeadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        
        for(integer i = 1; i <= 5; i++){
            Account objAcc = new Account(RecordTypeId=AccountRecordTypeId, CRM_CP_Eloqua_Integration__pc = true, FirstName='TestAccountFname_'+i, LastName='TestAccountLName_'+i, personEmail = 'fnamelname_'+i+'@consumermail.com', CRM_CP_Owner_Company_Number__c ='480' );
            lstAccount.add(objAcc);
            
            objAcc = new Account(RecordTypeId=AccountRecordTypeId, CRM_CP_Eloqua_Integration__pc = true, FirstName='TestDOIAccountFname_'+i, LastName='TestDOIAccountLName_'+i, personEmail = 'fnamelnameDOI_'+i+'@consumermail.com', CRM_CP_Owner_Company_Number__c ='230' );
            lstAccount.add(objAcc);
            
            Account objAcc1 = new Account(RecordTypeId=AccountRecordTypeId, FirstName='TestAccountFname_'+i, LastName='TestAccountLName_'+i, personEmail = 'fnamelname_'+i+'@consumermail.com',  CRM_CP_Owner_Company_Number__c ='230');
            lstAccount.add(objAcc1);
            
            Contact objCon = new Contact(RecordTypeId=ContactRecordTypeId, FirstName='TestContactFname_'+i, LastName='TestContactLName_'+i,CRM_CP_Contact_Type__c='Educator',  CRM_CP_Owner_Company_Number__c ='430');
            lstContact.add(objCon);
            
            Lead objlead = new Lead(RecordTypeId=LeadRecordTypeId, CRM_CP_Eloqua_Integration__c = true, FirstName='TestLeadFname_'+i, LastName='TestLeadName_'+i,Status='New',  CRM_CP_Owner_Company_Number__c ='710');
            lstLead.add(objlead);
            
        }
        
        try{
            if(!lstAccount.isEmpty()){
                insert lstAccount;
            }
            if(!lstContact.isEmpty()){
                insert lstContact;
            }
            if(!lstLead.isEmpty()){
                insert lstLead;
            }
        }
                catch(Exception ex){
                    system.debug('Test Fail :' +ex.getMessage());
                }
     }
     
     
     public static testMethod void testConsentScenariosForAccount() {
            
            Test.startTest();
                Set<Id> individualIds = new Set<Id>();  
                
                for(Account acc : [Select PersonIndividualId from Account]){
                    individualIds.add(acc.PersonIndividualId);
                }
                
                List<Individual> lstDataPrivacyAccounts = [SELECT Id FROM Individual
                                                  WHERE Id IN : individualIds];
                
                for(Individual ind : lstDataPrivacyAccounts){
                    ind.CRM_CP_Consent_Type__c = 'Consent';
                    ind.CRM_CP_Latest_Consent_Channel__c = 'Online';
                }
                try{
                    update lstDataPrivacyAccounts;
                }
                catch(Exception ex){
                    system.debug('Test Fail :' +ex.getMessage());
                }
            Test.stopTest();    
     }
     
     public static testMethod void testConsentScenariosForContact() {
            
            Test.startTest();
                Set<Id> individualIds = new Set<Id>();  
                
                for(Contact con : [Select IndividualId from Contact]){
                    individualIds.add(con.IndividualId);
                }
                
                List<Individual> lstDataPrivacyContacts = [SELECT Id FROM Individual
                                                  WHERE Id IN : individualIds];
                
                for(Individual ind : lstDataPrivacyContacts){
                    ind.CRM_CP_Consent_Type__c = 'Consent';
                    ind.CRM_CP_Latest_Consent_Channel__c = 'Online';
                }
                try{
                    update lstDataPrivacyContacts;
                }
                catch(Exception ex){
                    system.debug('Test Fail :' +ex.getMessage());
                }
            Test.stopTest();    
        }
        
     public static testMethod void testConsentScenariosForContact_DeleteIndividual() {
            
            Test.startTest();
                Set<Id> individualIds = new Set<Id>();  
                
                for(Contact con : [Select IndividualId from Contact]){
                    individualIds.add(con.IndividualId);
                }
                
                List<Individual> lstDataPrivacyContacts = [SELECT Id FROM Individual
                                                  WHERE Id IN : individualIds];
                
                for(Individual ind : lstDataPrivacyContacts){
                    ind.CRM_CP_Consent_Type__c = 'Consent';
                    ind.CRM_CP_Latest_Consent_Channel__c = 'Online';
                }
                try{
                    update lstDataPrivacyContacts;
                    
                    
                    List <Contact> updateContacList = new List <Contact>();
                    List <Contact> contactList =  [Select Id,IndividualId from Contact];
                    for(Contact con: contactList){
                       con.IndividualId = null;
                       con.Individual= null;
                       updateContacList.add(con);
                   }
                    update updateContacList;
                    
                    delete [Select Id from Contact];
                    undelete [Select Id from Contact];
                }
                catch(Exception ex){
                    system.debug('Test Fail :' +ex.getMessage());
                }
            Test.stopTest();    
        }
        
     public static testMethod void testConsentScenariosForContact_Negative() {
            
            Test.startTest();
                Set<Id> individualIds = new Set<Id>();  
                
                for(Contact con : [Select IndividualId from Contact]){
                    individualIds.add(con.IndividualId);
                }
                
                List<Individual> lstDataPrivacyContacts = [SELECT Id FROM Individual
                                                  WHERE Id IN : individualIds];
                
                for(Individual ind : lstDataPrivacyContacts){
                    ind.CRM_CP_Consent_Type__c = 'Consent';
                    ind.CRM_CP_Latest_Consent_Channel__c = 'Verbal';
                }
                try{
                    update lstDataPrivacyContacts;
                    
                }
                catch(Exception ex){
                    system.debug('Test Fail :' +ex.getMessage());
                }
            Test.stopTest();    
        }
     
     public static testMethod void testConsentScenariosForLead() {
            
            Test.startTest();
                Set<Id> individualIds = new Set<Id>();  
                for(Lead objLead : [Select IndividualId from Lead]){
                    individualIds.add(objLead.IndividualId);
                }
                
                List<Individual> lstDataPrivacyLeads = [SELECT Id FROM Individual
                                                  WHERE Id IN : individualIds];
                
                for(Individual ind : lstDataPrivacyLeads){
                    ind.CRM_CP_Consent_Type__c = 'Consent';
                    ind.CRM_CP_Latest_Consent_Channel__c = 'Online';
                }
                
                try{
                    update lstDataPrivacyLeads;
                    
                    delete [Select Id from Lead];
                    undelete [Select Id from Lead ALL ROWS];
                    
                    delete [Select Id from Account];
                    undelete [Select Id from Account ALL ROWS];
                    
                    delete [Select Id from Contact];
                    undelete [Select Id from Contact ALL ROWS];
                }
                catch(Exception ex){
                    system.debug('Test Fail :' +ex.getMessage());
                }
            Test.stopTest();
     }
     
     public static testMethod void testConsentScenariosForLead_Negative() {
            
            Test.startTest();
                List <Lead> leadsList = [SELECT id,IndividualId FROM Lead];
                Set<Id> individualIds = new Set<Id>();  
                for(Lead objLead : [Select IndividualId from Lead]){
                    individualIds.add(objLead.IndividualId);
                }
                
                List<Individual> lstDataPrivacyLeads = [SELECT Id FROM Individual
                                                  WHERE Id IN : individualIds];
                
                for(Individual ind : lstDataPrivacyLeads){
                    ind.CRM_CP_Consent_Type__c = 'No';
                    ind.CRM_CP_Latest_Consent_Channel__c = 'Online';
                }
                
                try{
                    update lstDataPrivacyLeads;
                    leadsList[0].IndividualId = null;
                    update leadsList[0];
                }
                catch(Exception ex){
                    system.debug('Test Fail :' +ex.getMessage());
                }
            Test.stopTest();
     }
     
     public static testMethod void testConsentScenariosForEloqua_Scenario2() {
            
            Test.startTest();
            
                List<sObject> consumerandLeads = new List<sObject>();
                
                Lead objLead = [SELECT id,CRM_CP_ELQ_Consent__c,
                                        CRM_CP_ELQ_Informational_Email_Perm__c,
                                        CRM_CP_ELQ_Letter__c,
                                        CRM_CP_ELQ_Phone__c,
                                        CRM_CP_ELQ_Promotional_Email_Permission__c,
                                        CRM_CP_ELQ_SMS__c FROM Lead where CRM_CP_Eloqua_Integration__c = true LIMIT 1]; 
                                        
                try{
                    
                    objLead.CRM_CP_ELQ_Consent__c = 'Yes - to be confirmed';
                    update objLead;
                    
                    objLead.CRM_CP_ELQ_Consent__c = 'No';
                    update objLead;
                    
                    objLead.CRM_CP_ELQ_Consent__c = null;
                    update objLead;
                    
                    objLead.CRM_CP_ELQ_Consent__c = 'No';
                    update objLead;
                    
                }
                catch(Exception ex){
                    system.debug('Test Fail :' +ex.getMessage());
                }
            Test.stopTest();
     }
     
     public static testMethod void testConsentScenariosForEloqua_Scenario1() {
            
            Test.startTest();
            
                List<sObject> consumerandLeads = new List<sObject>();
                
                Account objAcc = [SELECT id,CRM_CP_ELQ_Consent__pc,
                                        CRM_CP_ELQ_Informational_Email_Perm__pc,
                                        CRM_CP_ELQ_Letter__pc,
                                        CRM_CP_ELQ_Phone__pc,
                                        CRM_CP_ELQ_Promotional_Email_Permission__pc,
                                        CRM_CP_ELQ_SMS__pc FROM Account where CRM_CP_Eloqua_Integration__pc = true LIMIT 1];    
                
                
                try{
                   
                    objAcc.CRM_CP_ELQ_Consent__pc = 'Yes - to be confirmed';
                    update objAcc;
                    
                    objAcc.CRM_CP_ELQ_Consent__pc = 'No';
                    update objAcc;
                    
                    objAcc.CRM_CP_ELQ_Consent__pc = null;
                    update objAcc;
                    
                    objAcc.CRM_CP_ELQ_Consent__pc = 'No';
                    update objAcc;
                    
                    
                }
                catch(Exception ex){
                    system.debug('Test Fail :' +ex.getMessage());
                }
            Test.stopTest();
     }
     
     public static testMethod void testDOIConsentScenariosForEloquaUpdate() {
            Id AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
            CRM_CP_Trigger_Configurations__c testTriggerConfig = new CRM_CP_Trigger_Configurations__c(); 
            testTriggerConfig.SetupOwnerId=UserInfo.getUserId(); 
            testTriggerConfig.CRM_CP_Disable_DOI_Trigger__c = true; 
            insert testTriggerConfig; 
            
            Test.startTest();

                List<Account> lstAccount = [SELECT id,CRM_CP_ELQ_Consent__pc,
                                        CRM_CP_ELQ_Informational_Email_Perm__pc,
                                        CRM_CP_ELQ_Letter__pc,
                                        CRM_CP_ELQ_Phone__pc,
                                        CRM_CP_ELQ_Promotional_Email_Permission__pc,
                                        CRM_CP_ELQ_SMS__pc FROM Account where CRM_CP_Eloqua_Integration__pc = true];    
                
                
                for(Account objAcc : lstAccount){
                    objAcc.CRM_CP_ELQ_Consent__pc = 'Yes - to be confirmed';
                    objAcc.CRM_CP_ELQ_Informational_Email_Perm__pc = 'Yes';
                    objAcc.CRM_CP_ELQ_Promotional_Email_Permission__pc = 'Yes - to be confirmed';
                }
                
                try{
                    update lstAccount;
                }
                catch(Exception ex){
                    system.debug('Test Fail :' +ex.getMessage());
                }
            Test.stopTest();
     }
     
          public static testMethod void testDOIConsentScenariosForEloquaInsert(){
            List<Account> lstAccount = new List<Account>(); 
            Id AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
            CRM_CP_Trigger_Configurations__c testTriggerConfig = new CRM_CP_Trigger_Configurations__c(); 
            testTriggerConfig.SetupOwnerId=UserInfo.getUserId(); 
            testTriggerConfig.CRM_CP_Disable_DOI_Trigger__c = true; 
            insert testTriggerConfig; 
            
            Test.startTest();
                
              Account objAcc = new Account(RecordTypeId=AccountRecordTypeId, CRM_CP_Eloqua_Integration__pc = true, FirstName='TestAccountFname_6', LastName='TestAccountLName_6', personEmail = 'fnamelname_6'+'@consumermail.com', CRM_CP_Owner_Company_Number__c ='230' );
              objAcc.CRM_CP_ELQ_Consent__pc = 'Yes - to be confirmed';
              objAcc.CRM_CP_ELQ_Informational_Email_Perm__pc = 'Yes';
              objAcc.CRM_CP_ELQ_Promotional_Email_Permission__pc = 'Yes - to be confirmed'; 
              lstAccount.add(objAcc);
              
              objAcc = new Account(RecordTypeId=AccountRecordTypeId, CRM_CP_Eloqua_Integration__pc = true, FirstName='TestAccountFname_6', LastName='TestAccountLName_6', personEmail = 'fnamelname_6'+'@consumermail.com');
              objAcc.CRM_CP_ELQ_Consent__pc = 'Yes - to be confirmed';
              objAcc.CRM_CP_ELQ_Informational_Email_Perm__pc = 'Yes';
              objAcc.CRM_CP_ELQ_Promotional_Email_Permission__pc = 'Yes - to be confirmed'; 
              lstAccount.add(objAcc);
                try{
                   
                    insert lstAccount;
                }
                catch(Exception ex){
                    system.debug('Test Fail :' +ex.getMessage());
                }
            Test.stopTest();
     }
     
     public static testMethod void testConsentScenariosForDataPrivacy() {
            
            Test.startTest();
            
             Individual objindividual = [SELECT id,CRM_CP_Consent_Type__c,
                                        CRM_CP_Informational_Email_Permission__c,
                                        CRM_CP_Letter__c,
                                        CRM_CP_Phone__c,
                                        CRM_CP_Promotional_Email_Permission__c,
                                        CRM_CP_SMS__c FROM Individual LIMIT 1]; 
            
                try{
                    objindividual.CRM_CP_Informational_Email_Permission__c = 'Yes';
                    update objindividual;
                    
                    objindividual.CRM_CP_Consent_Type__c = 'Consent';
                    objindividual.CRM_CP_Consent_Type__c = 'Third Party Relationship';
                    objindividual.CRM_CP_Promotional_Email_Permission__c = 'No';
                    update objindividual;
                    
                    objindividual.CRM_CP_Consent_Type__c = 'Withdrawn';
                    update objindividual;
                    
                    objindividual.CRM_CP_Consent_Type__c = 'No';
                    update objindividual;
                }
                catch(Exception ex){
                    system.debug('Test Fail :' +ex.getMessage());
                }
            Test.stopTest();
     }
     
          public static testMethod void TestAllStomaTOY(){
            
            List <Account> listAccounts = new List <Account>();
            List<CRM_CP_Health_Basics__c> lstHbs = new List<CRM_CP_Health_Basics__c>();
            
            Test.startTest();
            List <account> accList = [select id, PersonEmail, CRM_CP_ELQ_Colostomy__pc,CRM_CP_ELQ_Continent_Urostomy__pc, CRM_CP_ELQ_Ileostomy__pc,CRM_CP_ELQ_Jejunostomy__pc,
                                      CRM_CP_ELQ_Nephrostomy__pc, CRM_CP_ELQ_Urostomy__pc, CRM_CP_ELQ_Wheelchair__pc, CRM_CP_ELQ_Surgery_Date__pc,
                                      CRM_CP_ELQ_Temporary_Stoma__pc from account WHERE PersonEmail like '%@consumermail%'];

                for(Account acc : accList){

                    acc.CRM_CP_ELQ_Colostomy__pc = 'Y';
                    acc.CRM_CP_ELQ_Continent_Urostomy__pc= 'Y';
                    acc.CRM_CP_ELQ_Ileostomy__pc= 'Y';
                    acc.CRM_CP_ELQ_Jejunostomy__pc= 'Y';
                    acc.CRM_CP_ELQ_Nephrostomy__pc= 'Y';
                    acc.CRM_CP_ELQ_Urostomy__pc= 'Y';
                    acc.CRM_CP_ELQ_Wheelchair__pc= 'Y';
                    acc.CRM_CP_ELQ_Surgery_Date__pc= System.today() - 30;
                    acc.CRM_CP_ELQ_Temporary_Stoma__pc= 'Y';
                    
                    CRM_CP_Health_Basics__c newHB = new CRM_CP_Health_Basics__c(); 
                    newHB.CRM_CP_Outcome__c = 'Colostomy';
                    newHB.CRM_CP_Stoma_Status__c = 'Temporary';
                    newHB.CRM_CP_Owner_Company_Number__c = acc.CRM_CP_Owner_Company_Number__c; 
                    newHB.CRM_CP_Account_Name_Legal_Name__c = acc.Id; 
                    lstHbs.add(newHB);

            }
            update accList;
            insert lstHbs; 
            
            System.assertNotEquals(accList,null);
            
            for(Account acc : accList){

                    acc.CRM_CP_ELQ_Colostomy__pc = 'N';
                    acc.CRM_CP_ELQ_Wheelchair__pc= 'N';
                    acc.CRM_CP_ELQ_Surgery_Date__pc= System.today() - 40;
                    acc.CRM_CP_ELQ_Temporary_Stoma__pc= 'N';

            }
            update accList; 
            System.assertNotEquals(accList,null);
            
               for(Account acc : accList){
        
                    acc.CRM_CP_ELQ_Surgery_Date__pc= System.today() - 45;
                    acc.CRM_CP_ELQ_Temporary_Stoma__pc= 'Y';

            }
            update accList; 
            System.assertNotEquals(accList,null);
            Test.stopTest();    
     }
     
     
}