@isTest(seeAllData=false)
public class CRM_CP_ChangeOwnerController_Test {

    @testSetup static void setup() {
        
        Account objAcc = new Account(RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CRM_CP_Consumer').getRecordTypeId(), CRM_CP_Eloqua_Integration__pc = true, FirstName='TestAccountFname', LastName='TestAccountLName');
        insert objAcc;
        
        Case objCase = new Case();
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CRM_CP_Service_Interactions').getRecordTypeId();
        objCase.Status = 'Open';
        objCase.AccountId = objAcc.Id;
        insert objCase;
       
    }
    
    public static testMethod void testChangeCaseOwner() {
        /*CRM_CP_ChangeOwnerController.searchUsers('Nitesh');
        Case objCase = [Select Id, CaseNumber from Case where RecordTypeId = :Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CRM_CP_Service_Interactions').getRecordTypeId()];
        CRM_CP_ChangeOwnerController.changeOwner(objCase.Id, UserInfo.getUserId(), UserInfo.getUserEmail(), true);*/
    }
}