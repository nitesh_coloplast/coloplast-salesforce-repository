public class PowerBIController extends OAuthController {
    @TestVisible private static String APPLICATION_NAME = 'PowerBI';
    
    Public String selectedGroup{get;set;}
    Public String selectedReport{get;set;}
    Public String selectedPage{get;set;}
    Public String height{get;set;}
    Public String width{get;set;}
    
    
    public PowerBIController () {
        String urlPage = Apexpages.currentPage().getUrl();
        String PageName = '';
        List<CRM_CP_PowerBI_Page_settings__mdt> tabName = [Select id, DeveloperName , selectedGroup__c, selectedPage__c, selectedReport__c, Width__c, Height__c from CRM_CP_PowerBI_Page_settings__mdt];
        for (CRM_CP_PowerBI_Page_settings__mdt tab : tabName){
            if(urlPage.contains(tab.DeveloperName ) || Test.IsRunningTest()){
              PageName = tab.DeveloperName ;
              selectedGroup = tab.selectedGroup__c;
              selectedReport = tab.selectedReport__c;
              selectedPage = tab.selectedPage__c;
              height = tab.Height__c;
              width = tab.Width__c;
              break;
            }
            
        }
       
       
        this.application_name = APPLICATION_NAME;       

    }

    
    
    public String getValidateResult()
    {
        return validateResult;
    }

    /**
    * Validates the callback code and generates the access and refresh tokens
    *
    * @return null to refresh the page
    */
    public PageReference redirectOnCallback() {
        return super.redirectOnCallback(null);
    }
    
    public PageReference refreshAccessToken() {
        return super.refreshAccessToken(ApexPages.currentPage());
    }
}