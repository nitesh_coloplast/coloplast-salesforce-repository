/**
* @author   	: Ninad Patil
* @date 		: 30 Aug 2018
* @description 	: This is the Order trigger where no logics will be written, 
	this will call the trigger dispather and trigger dispather will handle the rest.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Ninad Patil - 
*/

trigger CRM_CP_OrderTrigger on Order (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  
	  
	/**
	* @author   	: Ninad Patil
	* @date 		: 30 Aug 2018
	* @description 	: This method will call the Trigger dispatcher and dispater will check the trigger event and will navigate the flow to the corresponding 
					  trigger handler method
	* @param		: CRM_CP_OrderTriggerHandler instance will be passed to the Trigger dispather
	*/
	CRM_CP_TriggerDispatcher.Run(new CRM_CP_OrderTriggerHandler()); 
    
}