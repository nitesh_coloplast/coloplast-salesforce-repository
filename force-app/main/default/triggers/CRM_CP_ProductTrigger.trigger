/**
* @author   	: Nitesh Kumar
* @date 		: 08/28/2018
* @description 	: This is the product trigger where no logics will be written, this will call the trigger dispather and trigger dispather will handle the rest.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Nitesh Kumar - Creted this trigger as part of US-1354
*/
trigger CRM_CP_ProductTrigger on Product2 (before update, before insert) {
    /**
	* @author   	: Nitesh Kumar
	* @date 		: 08/28/2018
	* @description 	: This method will call the Trigger dispatcher and dispater will check the trigger event and will navigate the flow to the corresponding 
					  trigger handler method
	* @param		: CRM_CP_ProductTriggerHandler instance will be passed to the Trigger dispather
	*/
	CRM_CP_TriggerDispatcher.Run(new CRM_CP_ProductTriggerHandler());
}