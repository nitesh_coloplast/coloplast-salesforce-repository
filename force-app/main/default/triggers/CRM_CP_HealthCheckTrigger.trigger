/**
* @author     : Mruga Shastri
* @date     : 06-09-2018
* @description   : This is the Health Check trigger where no logics will be written, this will call the trigger 
*                  dispather and trigger dispather will handle the rest.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Mruga Shastri- Creted all the method as part of US-1367 
*/
trigger CRM_CP_HealthCheckTrigger on CRM_CP_Health_Check__c(After Insert, After Update) {
   CRM_CP_TriggerDispatcher.Run(new CRM_CP_HCTriggerHandler ());
}