/**
* @author       : Prity Kumari
* @date         : 02/15/2019
* @description  : This is the ContentDocumentLink trigger where no logics will be written, this will call the trigger dispather and trigger dispather will handle the rest.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Prity Kumari - Creted all the methods as part of US-2018
*/

trigger CRM_CP_ContentDocumentLinkTrigger on ContentDocumentLink (before insert, before Update, before Delete) {
     /**
    * @author       : Prity Kumari
    * @date         : 02/15/2019
    * @description  : This method will call the Trigger dispatcher and dispater will check the trigger event and will navigate the flow to the corresponding trigger handler method
    * @param        : CRM_CP_ContentDocumentTriggerHandler instance will be passed to the Trigger dispather
    */
    CRM_CP_TriggerDispatcher.Run(new CRM_CP_ContentDocumentLinkTriggerHandler());     
}