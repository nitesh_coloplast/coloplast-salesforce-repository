trigger CRM_CP_CampaignTrigger on Campaign (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    /*This trigger calls helper class to create Campaign Member Status records*/
    CRM_CP_TriggerDispatcher.Run(new CRM_CP_CampaignTriggerHandler());
}