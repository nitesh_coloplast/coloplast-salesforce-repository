/**
* @author   	: Nitesh Kumar
* @date 		: 04/17/2018
* @description 	: This is the Data Privacy trigger where no logics will be written, this will call the trigger dispather and trigger dispather will handle the rest.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Nitesh Kumar - Creted all the method as part of US-0693 and US-0556
*/
trigger CRM_CP_DataPrivacyTrigger on Individual (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	
	/**
	* @author   	: Nitesh Kumar
	* @date 		: 04/17/2018
	* @description 	: This method will call the Trigger dispatcher and dispater will check the trigger event and will navigate the flow to the corresponding 
					  trigger handler method
	* @param		: CRM_CP_DataPrivacyTriggerHandler instance will be passed to the Trigger dispather
	*/
    CRM_CP_TriggerDispatcher.Run(new CRM_CP_DataPrivacyTriggerHandler());
    
}