/**
* @author     : Mruga Shastri
* @date     : 20-08-2018
* @description   : This is the Health Basics trigger where no logics will be written, this will call the trigger 
*                  dispather and trigger dispather will handle the rest.
* ------------------------------------------ Versions ------------------------------------
* Version 1.0  : Mruga Shastri- Creted all the method as part of US-1367 
*/
trigger CRM_CP_HealthBasicsTrigger on CRM_CP_Health_Basics__c (After Insert, After Update) {
   CRM_CP_TriggerDispatcher.Run(new CRM_CP_HBTriggerHandler());
}